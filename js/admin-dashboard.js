set_location_display();
function set_location_display() {
    var locname = $('#location-list-sidebar option:selected').text();
    $('.location-display').html(locname);
}

set_admin_header();
function set_admin_header() {
    var start = $.datepicker.formatDate('M dd, yy', new Date($('#start-date-sidebar').val()));
    var end = $.datepicker.formatDate('M dd, yy', new Date($('#end-date-sidebar').val()));
    $('.graph-header').html(start + ' - ' + end);
}

function set_clinician_names(data) {
    $('#clinician-list-admin').html(data);
    $('<input>').attr({
        type: 'hidden',
        id: 'location-admin',
        name: 'location',
        value: $('#location-list-sidebar').val()
    }).appendTo('#clinician-list-admin');
    $('<input>').attr({
        type: 'hidden',
        id: 'start-date-admin',
        name: 'start-date-admin',
        value: $('#start-date-sidebar').val()
    }).appendTo('#clinician-list-admin');
    $('<input>').attr({
        type: 'hidden',
        id: 'start-date-admin',
        name: 'end-date-admin',
        value: $('#end-date-sidebar').val()
    }).appendTo('#clinician-list-admin');
}


function initialize_table() {
    $.ajax({
        type: "POST",
        url: "admin_dashboard/get_data",
        data: 'location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
        dataType: 'json',
        success: function(data) {
            if (data.vol == '') {
                enable_button(false);
            } else {
                enable_button(true);
            }
            set_location_display();
            set_admin_header();
            set_clinician_names(data.clinician_list);
            //REDRAW TABLES WITH NEW DATA
            var vol = $('#vol').dataTable();
            vol.fnClearTable();
            $.each(data.vol, function(index, value) {
                vol.fnAddData(data.vol[index], false);
            });

            var bill = $('#bill').dataTable();
            bill.fnClearTable();
            $.each(data.bill, function(index, value) {
                bill.fnAddData(data.bill[index], false);
            });

            var unit = $('#unit').dataTable();
            unit.fnClearTable();
            $.each(data.unit, function(index, value) {
                unit.fnAddData(data.unit[index], false);
            });


            var visit = $('#visit').dataTable();
            visit.fnClearTable();
            $.each(data.visit, function(index, value) {
                visit.fnAddData(data.visit[index], false);
            });

            var cases = $('#cases').dataTable();
            cases.fnClearTable();
            $.each(data.case, function(index, value) {
                cases.fnAddData(data.case[index], false);
            });

            var fin = $('#fin').dataTable();
            fin.fnClearTable();
            $.each(data.finance, function(index, value) {
                fin.fnAddData(data.finance[index], false);
            });


            var staff = $('#staff').dataTable();
            staff.fnClearTable();
            $.each(data.staff, function(index, value) {
                staff.fnAddData(data.staff[index], false);
            });

            vol.fnDraw();
            bill.fnDraw();
            unit.fnDraw();
            visit.fnDraw();
            cases.fnDraw();
            fin.fnDraw();
            staff.fnDraw();
        }

    });
}

$('.toggleall').on('click', function() {//checkbox toggle
    $("#clinician-list-admin").find(':checkbox').prop('checked', this.checked);
});

$('button#generate-clinician-reports').on('click', function(e) {
    e.preventDefault();

    $("#clinician-list-admin").submit();
});


$('.update-button').on('click', function(e) {
    e.preventDefault();
    if (new Date($('#start-date-sidebar').val()).getTime() > new Date($('#end-date-sidebar').val()).getTime()) {
        $('#date-sidebar-error').html('Start Date should not be greater than End Date.');
        $('.startdate').addClass('has-error');
        $('.enddate').addClass('has-error');
    } else {
        $('#date-sidebar-error').html('');
        $('.startdate').removeClass('has-error');
        $('.enddate').removeClass('has-error');
        $.ajax({
            type: "POST",
            url: "admin_dashboard/get_data",
            data: 'location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
            dataType: 'json',
            beforeSend: function() {
                $('.update-button').button('loading');
            },
            success: function(data) {
                if (data.vol == '') {
                    enable_button(false);
                } else {
                    enable_button(true);
                }
                $('.update-button').button('reset');
                set_location_display();
                set_admin_header();
                console.log(data.clinician_list);
                set_clinician_names(data.clinician_list);
                //REDRAW TABLES WITH NEW DATA
                var vol = $('#vol').dataTable();
                vol.fnClearTable();
                $.each(data.vol, function(index, value) {
                    vol.fnAddData(data.vol[index], false);
                });

                var bill = $('#bill').dataTable();
                bill.fnClearTable();
                $.each(data.bill, function(index, value) {
                    bill.fnAddData(data.bill[index], false);
                });

                var unit = $('#unit').dataTable();
                unit.fnClearTable();
                $.each(data.unit, function(index, value) {
                    unit.fnAddData(data.unit[index], false);
                });


                var visit = $('#visit').dataTable();
                visit.fnClearTable();
                $.each(data.visit, function(index, value) {
                    visit.fnAddData(data.visit[index], false);
                });

                var cases = $('#cases').dataTable();
                cases.fnClearTable();
                $.each(data.case, function(index, value) {
                    cases.fnAddData(data.case[index], false);
                });

                var fin = $('#fin').dataTable();
                fin.fnClearTable();
                $.each(data.finance, function(index, value) {
                    fin.fnAddData(data.finance[index], false);
                });


                var staff = $('#staff').dataTable();
                staff.fnClearTable();
                $.each(data.staff, function(index, value) {
                    staff.fnAddData(data.staff[index], false);
                });

                vol.fnDraw();
                bill.fnDraw();
                unit.fnDraw();
                visit.fnDraw();
                cases.fnDraw();
                fin.fnDraw();
                staff.fnDraw();
            }

        });
    }
});
$('#vol').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "newcases"},
        {"mDataProp": "adis"},
        {"mDataProp": "pdis"},
        {"mDataProp": "activecases"},
        {"mDataProp": "vattended"},
        {"mDataProp": "vcanceled"},
        {"mDataProp": "vscheduled"},
        {"mDataProp": "ucharged"},
        {"mDataProp": "dworked"},
        {"mDataProp": "hworked"},
        {"mDataProp": "hscheduled"},
    ],
});

$('#bill').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "charges"},
        {"mDataProp": "collected"},
        {"mDataProp": "bdays"},
    ],
});

$('#unit').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "unitsvisit"},
        {"mDataProp": "unitsactive"},
        {"mDataProp": "unitshour"},
        {"mDataProp": "units8hour"},
        {"mDataProp": "chargeunit"},
        {"mDataProp": "collectedunit"},
    ],
});


$('#visit').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "visitscase"},
        {"mDataProp": "visitshour"},
        {"mDataProp": "visits8hour"},
        {"mDataProp": "chargesvisit"},
        {"mDataProp": "collectedvisit"},
        {"mDataProp": "noshowvisit"},
    ],
});


$('#cases').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "casesday"},
        {"mDataProp": "passiverate"},
        {"mDataProp": "attendrate"},
        {"mDataProp": "pdisadis"},
    ],
});


$('#fin').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "chargeshour"},
        {"mDataProp": "collectedhour"},
    ],
});


$('#staff').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "unitsbilling"},
        {"mDataProp": "visitsbilling"},
        {"mDataProp": "thourdays"},
        {"mDataProp": "scheddensity"},
    ],
});


initialize_table();


//creates a hidden form then submits to a new tab page --- USED to print clinician aggregates
jQuery(function($) {
    $.extend({
        form: function(url, data, method) {
            if (method == null)
                method = 'POST';
            if (data == null)
                data = {};

            var form = $('<form>').attr({
                method: method,
                action: url,
                target: '_blank'
            }).css({
                display: 'none'
            });

            var addData = function(name, data) {
                if ($.isArray(data)) {
                    for (var i = 0; i < data.length; i++) {
                        var value = data[i];
                        addData(name + '[]', value);
                    }
                } else if (typeof data === 'object') {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            addData(name + '[' + key + ']', data[key]);
                        }
                    }
                } else if (data != null) {
                    form.append($('<input>').attr({
                        type: 'hidden',
                        name: String(name),
                        value: String(data)
                    }));
                }
            };

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    addData(key, data[key]);
                }
            }

            return form.appendTo('body');
        }
    });
});



