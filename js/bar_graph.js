//load the default

//set_location_display();
//set_graph_header();
function set_location_display() {
    
    var locname = $('#location-list-sidebar option:selected').text();
    $('.location-display').html(locname);
}
function set_graph_header() {
    var start = $.datepicker.formatDate('M dd, yy', new Date($('#start-date-sidebar').val()));
    var end = $.datepicker.formatDate('M dd, yy', new Date($('#end-date-sidebar').val()));
    $('#graph-header').html(start + ' - ' + end);
}

$('.toggleall').on('click', function() {//checkbox toggle
    $("#clinician-list").find(':checkbox').prop('checked', this.checked);
});


$('#update-graph').click(function(e) {
    var text = selected;
    var subtext = subtitle;
    var show = $('#btn-showonly li.active a').attr('rel');
    $.ajax({
        type: 'POST',
        url: 'bar_graph/switch_graph',
        data: $('#clinician-list').serialize() + '&selected=' + text + '&show=' + show,
        dataType: 'json',
        success: function(data) {
            $('#user-list-modal').modal('hide');
            generateGraph(text, subtext, data.names, data.value);
        }
    });

});

$('.bar-graph-li li').click(function(e) {
    e.preventDefault();
    selected = $(this).text();
    subtitle = $(this).children('a').attr('title');
    var text = selected;
    var subtext = subtitle;
    var show = $('#btn-showonly li.active a').attr('rel');
    $.ajax({
        type: 'POST',
        url: 'bar_graph/switch_graph',
        data: $('#clinician-list').serialize() + '&selected=' + text + '&show=' + show,
        dataType: 'json',
        success: function(data) {
            generateGraph(text, subtext, data.names, data.value);
        }
    });

});

$('.showonly').click(function(e) {
    e.preventDefault();
    $('#btn-showonly li').each(function() {
        //remove the active class to all li
        $(this).removeClass('active');
    });
    $(this).parent().addClass('active');

    var text = selected;
    var subtext = subtitle;
    var show = $('#btn-showonly li.active a').attr('rel');
    $.ajax({
        type: 'POST',
        url: 'bar_graph/switch_graph',
        data: $('#clinician-list').serialize() + '&selected=' + text + '&show=' + show,
        dataType: 'json',
        success: function(data) {
            generateGraph(text, subtext, data.names, data.value);
        }
    });

});


$('#update-button-sidebar').click(function(e) {
    e.preventDefault();
    if (new Date($('#start-date-sidebar').val()).getTime() > new Date($('#end-date-sidebar').val()).getTime()) {
        $('#date-sidebar-error').html('Start Date should not be greater than End Date.');
        $('.startdate').addClass('has-error');
        $('.enddate').addClass('has-error');
    } else {
        $('#date-sidebar-error').html('');
        $('.startdate').removeClass('has-error');
        $('.enddate').removeClass('has-error');
        var text = 'New Cases';
        var subtext = 'Total number of new evaulations';
        var show = $('#btn-showonly li.active a').attr('rel');
        $.ajax({
            type: 'POST',
            url: 'bar_graph/get_data',
            data: 'selected=' + text + '&show=' + show + '&location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
            dataType: 'json',
            beforeSend: function() {
                $('#update-button-sidebar').button('loading');
            },
            success: function(data) {
                $('#update-button-sidebar').button('reset');
                set_location_display();
                set_graph_header();
                $('#clinician-list').html(data.clinicians);
                generateGraph(text, subtext, data.names, data.value);
            }
        });
    }
});

//graph generator function
function generateGraph(title, subtext, names, values) {

    var bar = new Highcharts.Chart({
        chart: {
            renderTo: 'bar-graph-container',
            type: 'bar',
            zoomType: 'xy',
        },
        title: {
            text: title
        },
        subtitle: {
            text: subtext
        },
        xAxis: {
            categories: names
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Values',
            },
            labels: {
                overflow: 'justify'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function() {
                if (title == 'Charges' || title == 'Estimated Collections' || title == 'Charge / Units' || title == 'Collected / Units' || title == 'Charges / Visits' || title == 'Collected / Visits' || title == 'Charges / Hours Worked' || title == 'Charges / Hours Worked' || title == 'Collected / Hours Worked') {
                    //money format
                    return this.x + '<br/>' + title + ': $ ' + this.y.formatMoney(2, '.', ',');
                } else if (title == 'Passive Discharge Rate' || title == 'Attendance Rate') {
                    //percentage format
                    return this.x + '<br/>' + title + ': ' + parseFloat(this.y).toFixed(2) + ' %';
                }
                if (title.indexOf("/") > -1 || title == 'Hours Worked' || title == 'Clinical Hours Scheduled') {
                    //float format
                    return this.x + '<br/>' + title + ': ' + this.y.toFixed(2);
                } else {
                    //default format
                    return this.x + '<br/>' + title + ': ' + this.y;
                }
            }
        },
        loading: {
            showDuration: 100,
            hideDuration: 100
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        if (title == 'Charges' || title == 'Estimated Collections' || title == 'Charge / Units' || title == 'Collected / Units' || title == 'Charges / Visits' || title == 'Collected / Visits' || title == 'Charges / Hours Worked' || title == 'Charges / Hours Worked' || title == 'Collected / Hours Worked') {
                            //money format
                            return '$ ' + this.y.formatMoney(2, '.', ',');
                        } else if (title == 'Passive Discharge Rate' || title == 'Attendance Rate') {
                            //percentage format
                            return parseFloat(this.y).toFixed(2) + ' %';
                        }
                        if (title.indexOf("/") > -1 || title == 'Hours Worked' || title == 'Clinical Hours Scheduled') {
                            //float format
                            return this.y.toFixed(2);
                        } else {
                            //default format
                            return this.y;
                        }
                    }
                }
            }
        },
    });

    var series = {
        name: names,
        data: []
    }
    $.each(values, function(index, value) {
        var odd = index % 2;
        var data = {};
        data.y = value;
        if (odd == 0) {
            data.color = '#DDD';
        } else {
            data.color = '#999';
        }

        series.data.push(data);

    });
    bar.addSeries(series);
}

Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


//initialize
var text = 'New Cases';
var subtext = 'Total number of new evaulations';
var show = $('#btn-showonly li.active a').attr('rel');
$.ajax({
    type: 'POST',
    url: 'bar_graph/get_data',
    data: 'selected=' + text + '&show=' + show + '&location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
    dataType: 'json',
    beforeSend: function() {
        $('#update-button-sidebar').button('loading');
    },
    success: function(data) {
        $('#update-button-sidebar').button('reset');
        set_location_display();
        set_graph_header();
        $('#clinician-list').html(data.clinicians);
        generateGraph(text, subtext, data.names, data.value);
    }
});