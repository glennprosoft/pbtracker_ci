
var location_id_var = $('select#location-list-sidebar :selected').val();
$("select#location-list-sidebar option:eq(0)").attr("selected", "selected");
$('select#location-list-sidebar').change(function(){
    location_id_var = $(this).val();
    $.ajax({
            async: false,
            type: "POST",
            url: 'clinician_dashboard/set_display_dashboard',
            data: {date_select: $('.calendar-box').datepicker({ dateFormat: 'yy-mm-dd' }).val(), location_id: location_id_var},
            dataType: 'json',
            success: function(data) {
                load_vars(data, location_id_var,true,$('.calendar-box').datepicker({ dateFormat: 'yy-mm-dd' }).val());
            }


        });
        
});

function set_post_date_values(selected_date) {
    if (selected_date !== 0) {
        $.ajax({
            async: false,
            type: "POST",
            url: 'clinician_dashboard/set_display_dashboard',
            data: {location_id: $('select#location-list-sidebar').val()},
            dataType: 'json',
            success: function(data) {
                load_vars(data, $('select#location-list-sidebar').val(),false, '');
            }
        });
    }else{
        $.ajax({
            async: false,
            type: "POST",
            url: 'clinician_dashboard/set_display_dashboard',
            data: {location_id: $('select#location-list-sidebar').val(),date_select: selected_date},
            dataType: 'json',
            success: function(data) {
                load_vars(data,$('select#location-list-sidebar').val(),false, '');
            }
        });
    }

}
set_post_date_values(0);

$('.calendar-box').datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(dateText, inst) {
        $.ajax({
            async: false,
            type: "POST",
            url: 'clinician_dashboard/set_display_dashboard',
            data: {date_select: dateText, location_id: location_id_var},
            dataType: 'json',
            success: function(data) {
                load_vars(data,location_id_var, true, dateText);

            }


        });
    }
});
function set_attr_by_date() {
    $('input[name=mon-1]').attr('date', '');
}

function load_vars(data, location_id,is_date_select, dateText) {
    //Sets the week dates on the table header
    $('.table-date').html(data.range['start'] + ' - ' + data.range['end']);
    for (y = 0, x = 1; x <= 7; y++, x++) {
        $('.week-dates:eq( ' + x + ' )').html(data.week_dates_header[x]);
    }
    //runs if the date is selected
    if (is_date_select) {
        $('#clinician-table-data-loader').load('clinician_dashboard/load_clinician_table', {'date_selected': dateText, location_id: location_id});
    } else {
        $('#clinician-table-data-loader').load('clinician_dashboard/load_clinician_table', {'date_selected': '0', location_id: location_id});
    }
    console.log(dateText);


}





