$('#submit-location').on('click', function(e) {

    e.preventDefault();
    $.ajax({
        async: false,
        url: 'submit_location',
        data: $('form#location-form').serialize(),
        dataType: 'json',
        type: 'post',
        beforeSend: function() {
            $('#submit-location').text('Loading...');
        },
        success: function(data) {
            if (data.status === 'error1') {
                new PNotify({
                    title: 'Error',
                    text: 'Please check the data you entered.',
                    type: 'error'
                });
                $('#submit-location').text('Submit');
                if (data.locationcode !== '') {
                    error('input[name=location-code]', data.locationcode);
                } else {
                    reset('input[name=location-code]');
                }

                if (data.locationname !== '') {
                    error('input[name=location-name]', data.locationname);
                } else {
                    reset('input[name=location-name]');
                }
                if (data.startdate !== '') {
                    error('input[name=start-date]', data.startdate);
                } else {
                    reset('input[name=start-date]');
                }
                if (data.collectionrate !== '') {
                    error('input[name=collection-rate]', data.collectionrate);
                } else {
                    reset('input[name=collection-rate]');
                }
                if (data.address1 !== '') {
                    error('input[name=address1]', data.address1);
                } else {
                    reset('input[name=address1]');
                }
                if (data.city !== '') {
                    error('input[name=city]', data.city);
                } else {
                    reset('input[name=city]');
                }
                if (data.state !== '') {
                    error('select[name=state]', data.state);
                } else {
                    reset('select[name=state]');
                }
                if (data.zipcode !== '') {
                    error('input[name=zip-code]', data.zipcode);
                } else {
                    reset('input[name=zip-code]');
                }
            } else if (data.status === 'success') {
                new PNotify({
                    title: 'Success',
                    text: 'The location has been created.',
                    type: 'success'
                });
                $('#submit-location').text('Submit');
                $('#location-form')[0].reset();

                refresh_all_fields();
            }
        }
    });
});




function error(element, html) {
    $(element).parent().parent().addClass('has-error');
    $(element).siblings('span').html(html);
}

function reset(element) {
    $(element).parent().parent().removeClass('has-error');
    $(element).siblings('span').html('');
}

function refresh_all_fields() {
    $('input').parent().parent().removeClass('has-error');
    $('input').siblings('span').html('');
    $('select').parent().parent().removeClass('has-error');
    $('select').siblings('span').html('');
}

function empty_fileds() {
    $('input').val('');

}


   