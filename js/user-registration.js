//$( "#start-date" ).datepicker();
$(document).ready(function() {

    function validate_page1() {
        var pagval = {
            status: 'error',
            password: '',
            cpassword: ''
        };
        $.ajax({
            async: false,
            url: 'user_registration/page1_val',
            data: $('form#form-owner').serialize(),
            dataType: 'json',
            type: 'post',
            success: function(data_page1) {
                if (data_page1.status === 'error1') {
                    pagval = {
                        status: 'error',
                        password: data_page1.password,
                        confirmpassword: data_page1.confirmpassword
                    };
                } else if (data_page1.status === 'ok') {
                    pagval = {
                        status: 'ok'
                    };
                }
            }
        });
        return pagval;
    }
    function validate_page2() {
        var pagval = {
            status: 'error',
            empid: '',
            regfname: '',
            reglname: '',
            regemail: ''
        };
        $.ajax({
            async: false,
            url: 'user_registration/page2_val',
            data: $('form#form-owner').serialize(),
            dataType: 'json',
            type: 'post',
            success: function(data_page2) {
                if (data_page2.status === 'error1') {
                    pagval = {
                        status: 'error',
                        empid: data_page2.empid,
                        regfname: data_page2.regfname,
                        reglname: data_page2.reglname,
                        regemail: data_page2.regemail
                    };
                } else if (data_page2.status === 'ok') {
                    pagval = {
                        status: 'ok'
                    };
                }
            }
        });
        return pagval;
    }
    


//initial states
    var current_page = 1;
    $('.pc1').parents('.registration-pane').addClass('active-pane');
    $('.pc1').children('.pane-number').show().siblings('.pane-check').hide();
    $('.pc2').children('.pane-number').show().siblings('.pane-check').hide();
    $('.pc3').children('.pane-number').show().siblings('.pane-check').hide();
    $('.pc4').children('.pane-number').show().siblings('.pane-check').hide();
    $('.page1').show();
    $('.page2').hide();
    $('.page3').hide();
    $('.page4').hide();
    $('button.prev-butt').hide();
    function onp1() {

        current_page = 1;
        $('.pc1').parents('.registration-pane').addClass('active-pane');
        $('.pc1').children('.pane-number').show().siblings('.pane-check').hide();
        $('.pc2').children('.pane-number').show().siblings('.pane-check').hide();
        $('.pc4').children('.pane-number').show().siblings('.pane-check').hide();
        $('.pc2').parents('.registration-pane').removeClass('active-pane');
        $('.pc3').parents('.registration-pane').removeClass('active-pane');
        $('.pc4').parents('.registration-pane').removeClass('active-pane');
        $('button.prev-butt').hide();
        $('button.next-butt').show();
        $('.page1').show();
        $('.page2').hide();
        $('.page3').hide();
        $('.page4').hide();
    }
    function onp2() {
        var val1 = validate_page1();
        if (val1.status === 'error') {
            if (val1.password !== '') {
                error('input[name=password]', val1.password);
            } else {
                reset('input[name=password]');
            }
            if (val1.confirmpassword !== '') {
                error('input[name=confirmpassword]', val1.confirmpassword);
            } else {
                reset('input[name=confirmpassword]');
            }
        } else {

            reset('input[name=password]');
            reset('input[name=confirmpassword]');
            current_page = 2;
            $('.pc1').parents('.registration-pane').removeClass('active-pane');
            $('.pc1').children('.pane-number').hide().siblings('.pane-check').show();
            $('.pc2').parents('.registration-pane').addClass('active-pane');
            $('.pc2').children('.pane-number').show().siblings('.pane-check').hide();
            $('.pc4').children('.pane-number').show().siblings('.pane-check').hide();
            $('.pc1').parents('.registration-pane').removeClass('active-pane');
            $('.pc3').parents('.registration-pane').removeClass('active-pane');
            $('button.prev-butt').show();
            $('.page1').hide();
            $('.page2').show();
            $('.page3').hide();
            $('.page4').hide();
        }
    }
    function onp3() {
        var val2 = validate_page2();
        if (val2.status === 'error') {
            if (val2.empid !== '') {
                error('input[name=emp-id]', val2.empid);
            } else {
                reset('input[name=emp-id]');
            }
            if (val2.regfname !== '') {
                error('input[name=reg-fname]', val2.regfname);
            } else {
                reset('input[name=reg-fname]');
            }
            if (val2.reglname !== '') {
                error('input[name=reg-lname]', val2.reglname);
            } else {
                reset('input[name=reg-lname]');
            }
            if (val2.regemail !== '') {
                error('input[name=reg-email]', val2.regemail);
            } else {
                reset('input[name=reg-email]');
            }
        } else {
            reset('input[name=emp-id]');
            reset('input[name=reg-fname]');
            reset('input[name=reg-lname]');
            reset('input[name=reg-email]');
            current_page = 3;
            $('.pc1').parents('.registration-pane').removeClass('active-pane');
            $('.pc1').children('.pane-number').hide().siblings('.pane-check').show();
            $('.pc2').children('.pane-number').hide().siblings('.pane-check').show();
            $('.pc3').parents('.registration-pane').addClass('active-pane');
            $('.pc3').children('.pane-number').hide().siblings('.pane-check').show();
            $('.pc1').parents('.registration-pane').removeClass('active-pane');
            $('.pc2').parents('.registration-pane').removeClass('active-pane');
            $('button.prev-butt').hide();
            $('button.next-butt').hide();
            $('.page1').hide();
            $('.page2').hide();
            $('.page3').show();
        }
    }
    
    $('button.next-butt').click(function() {
        if (current_page === 1) {
            onp2();
        } else
        if (current_page === 2) {
            onp3();
        }

    });
    $('button.prev-butt').click(function() {
        if (current_page === 2) {
            onp1();
        }
    });
});