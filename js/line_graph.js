//load the default
set_location_display();
set_graph_header();
function set_location_display() {
    var locname = $('#location-list-sidebar option:selected').text();
    $('.location-display').html(locname);
}

function set_graph_header() {
    var start = $.datepicker.formatDate('M dd, yy', new Date($('#start-date-sidebar').val()));
    var end = $.datepicker.formatDate('M dd, yy', new Date($('#end-date-sidebar').val()));
    $('#graph-header').html(start + ' - ' + end);
}

$('.toggleall').on('click', function() {//checkbox toggle
    $("#clinician-list").find(':checkbox').prop('checked', this.checked);
});



var selected = 'New Cases';
var subtitle = 'Total number of new evaulations';

$('#update-button-sidebar').click(function(e) {
    e.preventDefault();
    if (new Date($('#start-date-sidebar').val()).getTime() > new Date($('#end-date-sidebar').val()).getTime()) {
        $('#date-sidebar-error').html('Start Date should not be greater than End Date.');
        $('.startdate').addClass('has-error');
        $('.enddate').addClass('has-error');
    } else {
        $('#date-sidebar-error').html('');
        $('.startdate').removeClass('has-error');
        $('.enddate').removeClass('has-error');
        var text = 'New Cases';
        var subtext = 'Total number of new evaulations';
        var show = $('#btn-showonly li.active a').attr('rel');
        $.ajax({
            type: 'POST',
            url: 'line_graph/get_data',
            data: 'selected=' + text + '&show=' + show + '&location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
            dataType: 'json',
            beforeSend: function() {
                $('#update-button-sidebar').button('loading');
            },
            success: function(data) {
                $('#update-button-sidebar').button('reset');
                set_location_display();
                set_graph_header();
                $('#clinician-list').html(data.clinicians);
                generateGraph(text, subtext, data.names, data.datas, data.dates);
            }
        });
    }
});


$('#update-graph').click(function(e) {
    e.preventDefault();
    var text = selected;
    var subtext = subtitle;
    var show = $('#btn-showonly li.active a').attr('rel');
    $.ajax({
        type: 'POST',
        url: 'line_graph/switch_graph',
        data: $('#clinician-list').serialize() + '&selected=' + text + '&show=' + show,
        dataType: 'json',
        success: function(data) {
            $('#user-list-modal').modal('hide');
            generateGraph(text, subtext, data.names, data.datas, data.dates);
        }
    });

});

$('.showonly').click(function(e) {
    e.preventDefault();
    $('#btn-showonly li').each(function() {
        //remove the active class to all li
        $(this).removeClass('active');
    });
    $(this).parent().addClass('active');

    var text = selected;
    var subtext = subtitle;
    var show = $('#btn-showonly li.active a').attr('rel');
    $.ajax({
        type: 'POST',
        url: 'line_graph/switch_graph',
        data: $('#clinician-list').serialize() + '&selected=' + text + '&show=' + show,
        dataType: 'json',
        success: function(data) {
            generateGraph(text, subtext, data.names, data.datas, data.dates);
        }
    });

});

$('.line-graph-li li').click(function(e) {
    e.preventDefault();
    selected = $(this).text();
    subtitle = $(this).children('a').attr('title');
    var text = selected;
    var subtext = subtitle;
    var show = $('#btn-showonly li.active a').attr('rel');
    $.ajax({
        type: 'POST', 
        url: 'line_graph/switch_graph',
        data: $('#clinician-list').serialize() + '&selected=' + text + '&show=' + show + '&location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
        dataType: 'json',
        success: function(data) {
            console.log(data.datas);
            generateGraph(text, subtext, data.names, data.datas, data.dates);

        }
    });
});


//graph generator function
function generateGraph(title, subtext, names, datas, dates) {

    $('#legendlist').empty();
    var line = new Highcharts.Chart({
        chart: {
            renderTo: 'line-graph-container',
            type: 'line',
            zoomType: 'xy'
        },
        title: {
            text: title
        },
        subtitle: {
            text: subtext
        },
        xAxis: {
            type: 'datetime',
            maxZoom: 1,
            dateTimeLabelFormats: {
                month: '%e. %b',
                year: '%b'
            }
        },
        yAxis: {
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function() {
                if (title == 'Charges' || title == 'Estimated Collections' || title == 'Charge / Units' || title == 'Collected / Units' || title == 'Charges / Visits' || title == 'Collected / Visits' || title == 'Charges / Hours Worked' || title == 'Charges / Hours Worked' || title == 'Collected / Hours Worked') {
                    //money format
                    return  this.series.name + '<br/>' + Highcharts.dateFormat('%b %e', this.x) + '<br/>' + title + ': $ ' + this.y.formatMoney(2, '.', ',');
                } else if (title == 'Passive Discharge Rate' || title == 'Attendance Rate') {
                    //percentage format
                    return this.series.name + '<br/>' + Highcharts.dateFormat('%b %e', this.x) + '<br/>' + title + ': ' + parseFloat(this.y).toFixed(2) + ' %';
                }
                if (title.indexOf("/") > -1 || title == 'Hours Worked' || title == 'Clinical Hours Scheduled') {
                    //float format
                    return this.series.name + '<br/>' + Highcharts.dateFormat('%b %e', this.x) + '<br/>' + title + ': ' + this.y.toFixed(2);
                } else {
                    //default format
                    return this.series.name + '<br/>' + Highcharts.dateFormat('%b %e', this.x) + '<br/>' + title + ': ' + this.y;
                }
            }
        },
        legend: {
            enabled: false
        },
    });

    for (i = 0; i < names.length; i++) {
        var series = {
            id: 'series',
            name: names[i],
            data: []
        }
        $.each(dates[i], function(index, value) {

            xval = value.split("-");
            x = Date.UTC(xval[0], xval[1] - 1, xval[2]);
            series.data.push([
                x,
                parseFloat(datas[i][index])
            ]);

        });
        line.addSeries(series);
    }

    if (names.length > 0) {
        /* create a custom legend */
        var options = line.options.legend;
        function clickItem(series, $legendItem, $line) {
            series.setVisible();
            $legendItem.css(
                    options[series.visible ? 'itemStyle' : 'itemHiddenStyle']
                    );
            if (series.visible)
                $legendItem.css({fontSize: 11})
            $line.css({
                borderTop: '2px solid ' + (series.visible ? series.color :
                        options.itemHiddenStyle.color)
            });
        }
        /* Create the legend box */

        $('<h4>').css({marginBottom: 0}).html('<strong style="width:100%; text-align:center">Legend</strong>').appendTo('#legendlist');
        var $legend = $('<div>')
                .css({
                    maxHeight: 210,
                    padding: 10,
                    overflow: 'auto',
                    borderColor: options.borderColor,
                    borderWidth: options.borderWidth,
                    borderStyle: 'solid',
                    borderRadius: options.borderRadius,
                    backgroundColor: 'white'
                })
                .appendTo('#legendlist');

        $.each(line.series, function(i, series) {

            var $legendItem = $('<div>')
                    .css({
                        position: 'relative',
                        marginLeft: 32,
                    })
                    .css(options[series.visible ? 'itemStyle' : 'itemHiddenStyle'])
                    //.css({color: series.color})

                    .html(series.name).css({fontSize: 10})
                    .appendTo($legend);

            // create the line with each series color
            var $line = $('<div>')
                    .css({
                        width: 25,
                        position: 'absolute',
                        left: -30,
                        top: 8,
                        borderTop: '5px solid ' + (series.visible ? series.color :
                                options.itemHiddenStyle.color)
                    })
                    .appendTo($legendItem);

            // click handler 
            $legendItem.click(function() {
                clickItem(series, $legendItem, $line);
            });

        });
    }
}

Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

//initialize
var text = 'New Cases';
var subtext = 'Total number of new evaulations';
var show = $('#btn-showonly li.active a').attr('rel');
$.ajax({
    type: 'POST',
    url: 'line_graph/get_data',
    data: 'selected=' + text + '&show=' + show + '&location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
    dataType: 'json',
    beforeSend: function() {
        $('#update-button-sidebar').button('loading');
    },
    success: function(data) {
        $('#update-button-sidebar').button('reset');
        set_location_display();
        set_graph_header();
        $('#clinician-list').html(data.clinicians);
        generateGraph(text, subtext, data.names, data.datas, data.dates);
    }
});