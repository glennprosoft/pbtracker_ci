load_clinician_names();
function set_clinician_names(data) {
    $('#clinician-list-minimum').html(data);
    $('<input>').attr({
        type: 'hidden',
        id: 'location-minimum',
        name: 'location',
        value: $('#location-list-sidebar').val()
    }).appendTo('#clinician-list-minimum');
    $('<input>').attr({
        type: 'hidden',
        id: 'start-date-minimum',
        name: 'start-date-minimum',
        value: $('#start-date-sidebar').val()
    }).appendTo('#clinician-list-minimum');
    $('<input>').attr({
        type: 'hidden',
        id: 'end-date-minimum',
        name: 'end-date-minimum',
        value: $('#end-date-sidebar').val()
    }).appendTo('#clinician-list-minimum');
}


set_location_display();

function set_location_display() {
    var locname = $('#location-list-sidebar option:selected').text();
    $('.location-display').html(locname);
}

set_graph_header();

function set_graph_header() {
    var start = $.datepicker.formatDate('M dd, yy', new Date($('#start-date-sidebar').val()));
    var end = $.datepicker.formatDate('M dd, yy', new Date($('#end-date-sidebar').val()));
    $('#mindata ').html(start + ' - ' + end);
}

$('.update-button').on('click', function(e) {
    e.preventDefault();
    if (new Date($('#start-date-sidebar').val()).getTime() > new Date($('#end-date-sidebar').val()).getTime()) {
        $('#date-sidebar-error').html('Start Date should not be greater than End Date.');
        $('.startdate').addClass('has-error');
        $('.enddate').addClass('has-error');
    } else {
        $('#date-sidebar-error').html('');
        $('.startdate').removeClass('has-error');
        $('.enddate').removeClass('has-error');
        $.ajax({
            type: "POST",
            url: "minimum_dataset_dashboard/get_data",
            data: 'location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
            dataType: 'json',
            beforeSend: function() {
                $('.update-button').button('loading');
            },
            success: function(data) {
                $('.update-button').button('reset');
                set_graph_header();
                set_location_display();
                //set data in table
                var min = $('#minimum-maintenance-table').dataTable();
                min.fnClearTable();
                if (data.min != '') {
                    min.fnAddData(data.min, false);
                    min.fnDraw();
                }

            }
        });
    }
});




$('#update-button-sidebar').on('click', function() {
    load_clinician_names();
    
});

function load_clinician_names(){
    $.ajax({
        type: "POST",
        url: "minimum_dataset_dashboard/load_clinician_names",
        data: 'location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
        dataType: 'json',
        success: function(data) {
            set_clinician_names(data);
            func_disable();
        }
    });
}


$('#minimum-maintenance-table').dataTable({
    "aoColumns": [
        {"mDataProp": "emp"},
        {"mDataProp": "visitscase"},
        {"mDataProp": "unitsvisit"},
        {"mDataProp": "estcolrev"},
        {"mDataProp": "schedhourshour"},
        {"mDataProp": "unitshour"},
        {"mDataProp": "visistshour"},
        {"mDataProp": "chargeshour"},
        {"mDataProp": "canceledvisits"},
        {"mDataProp": "pdisadis"},
    ],
});

$('#generate-minimum-dataset-report').on('click', function(e) {
    e.preventDefault();
    $("#clinician-list-minimum").submit();
});


//initialize
if (new Date($('#start-date-sidebar').val()).getTime() > new Date($('#end-date-sidebar').val()).getTime()) {
    $('#date-sidebar-error').html('Start Date should not be greater than End Date.');
    $('.startdate').addClass('has-error');
    $('.enddate').addClass('has-error');
} else {
    $('#date-sidebar-error').html('');
    $('.startdate').removeClass('has-error');
    $('.enddate').removeClass('has-error');
    $.ajax({
        type: "POST",
        url: "minimum_dataset_dashboard/get_data",
        data: 'location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
        dataType: 'json',
        beforeSend: function() {
            $('.update-button').button('loading');
        },
        success: function(data) {
            $('.update-button').button('reset');
            set_graph_header();
            set_location_display();
            //set data in table
            var min = $('#minimum-maintenance-table').dataTable();
            min.fnClearTable();
            if (data.min != '') {
                min.fnAddData(data.min, false);
                min.fnDraw();
            }

        }
    });
}

