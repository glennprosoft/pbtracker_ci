$('#edit-form').on('click', '.submit-info' ,function(e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'profile/validate_fields',
        data: $('#edit-form').serialize(),
        dataType: 'json',
        beforeSend: function(){
             refresh_all_fields();
            $('.submit-info').text('Saving...');
        },
        success: function(data) {
            if (data.status === 'with-error'){
                $('.submit-info').text('Submit');
                if(data.employeeid !== ''){
                    error('input[name=employee-id]', data.employeeid);
                }else{
                    reset('input[name=employee-id]');
                }
                if(data.firstname !== ''){
                    error('input[name=first-name]', data.firstname);
                }else{
                    reset('input[name=first-name]');
                }
                if(data.lastname !== ''){
                    error('input[name=last-name]', data.lastname);
                }else{
                    reset('input[name=last-name]');
                }
                if(data.email !== ''){
                    error('input[name=email-address]', data.email);
                }else{
                    reset('input[name=email-address]');
                }
           }else if(data.status === 'success'){
                $('.submit-info').text('Submit');
                   new PNotify({
                    title: 'Success',
                    text: 'Your profile information has been updated.',
                    type: 'success'
                });
                refresh_all_fields();
           }
        }

    });
});

function error(element, html) {
    $(element).parent().parent().addClass('has-error');
    $(element).siblings('span').html(html);
}

function reset(element) {
    $(element).parent().parent().removeClass('has-error');
    $(element).siblings('span').html('');
}

function refresh_all_fields() {
    $('input').parent().parent().removeClass('has-error');
    $('input').siblings('span').html('');
    $('select').parent().parent().removeClass('has-error');
    $('select').siblings('span').html('');
}

function empty_fileds() {
    $('input').val('');
}

