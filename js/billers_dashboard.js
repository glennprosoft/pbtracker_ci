

$('.calendar-box').datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(dateText, inst) {
        $.ajax({
            async: false,
            type: "POST",
            url: 'billers_dashboard/get_users',
            data: {date_select: dateText, location_id: $('#location-list-sidebar').val()},
            dataType: 'json',
            success: function(data) {
                set_location_display();
                set_admin_header(data.daterange);
                set_day_dates(data.dates);

                var billers = $('#billers-table').dataTable();
                billers.fnClearTable();
                if (data.markups != '') {
                    billers.fnAddData(data.markups, false);
                    billers.fnDraw();
                }

            }
        });
    }
});


$('#location-list-sidebar').change(function() {
    var dateText = $('.calendar-box').val();
    $.ajax({
        async: false,
        type: "POST",
        url: 'billers_dashboard/get_users',
        data: {date_select: dateText, location_id: $('#location-list-sidebar').val()},
        dataType: 'json',
        success: function(data) {
            set_location_display();
            set_admin_header(data.daterange);
            set_day_dates(data.dates);
            var billers = $('#billers-table').dataTable();
            billers.fnClearTable();
            if (data.markups != '') {
                billers.fnAddData(data.markups, false);
                billers.fnDraw();
            }
        }
    });
});


function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z]+$/;
    if (regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault)
            theEvent.preventDefault();
    }
}

$('#submit-billing').click(function(e) {
    e.preventDefault();
    var dateText = $('.calendar-box').val();
    $.ajax({
        async: false,
        type: "POST",
        url: 'billers_dashboard/check_entry',
        data: $('#billers-form').serialize() + '&location_id=' + $('#location-list-sidebar').val(),
        dataType: 'json',
        success: function(data) {
            set_location_display();
            if (data.status == 'success') {
                  new PNotify({
                    title: 'Success',
                    text: 'The data has been saved.',
                    type: 'success'
                });
                
                initialize(dateText);
            } else if (data.status == 'error') {
                 
                $.each(data.names, function(index, value) {
                    $('input' + '[name="' + data.names[index] + '"]').css({
                        borderColor: '#A94442',
                    });
                })
            }
        }
    });
});

$('#billers-table').dataTable({
    "aoColumnDefs": [
        {'bSortable': false, 'aTargets': [1, 2, 3, 4, 5, 6, 7]},
        {'bSearchable': false, 'aTargets': [1, 2, 3, 4, 5, 6, 7]}
    ],
    "aoColumns": [
        {"mDataProp": "user"},
        {"mDataProp": "mon"},
        {"mDataProp": "tue"},
        {"mDataProp": "wed"},
        {"mDataProp": "thu"},
        {"mDataProp": "fri"},
        {"mDataProp": "sat"},
        {"mDataProp": "sun"},
    ]
});
function set_location_display() {
    var locname = $('#location-list-sidebar option:selected').text();
    $('.location-display').html(locname);
}

function set_admin_header(data) {
    var start = data[1];
    var end = data[7];
    $('.graph-header').html(start + ' - ' + end);
}

function set_day_dates(data) {
    var index = 1;
    $('.week-dates').each(function() {
        $(this).html(data[index]);
        index++;
    })
}
    var date = $.datepicker.formatDate('yy/mm/dd', new Date());
initialize(date);
function initialize(date) {
    $.ajax({
        async: false,
        type: "POST",
        url: 'billers_dashboard/get_users',
        data: {date_select: date, location_id: $('#location-list-sidebar').val()},
        dataType: 'json',
        success: function(data) {
            set_location_display();
            set_admin_header(data.daterange);
            set_day_dates(data.dates);
            var billers = $('#billers-table').dataTable();
            billers.fnClearTable();
            if (data.markups != '') {
                billers.fnAddData(data.markups, false);
                billers.fnDraw();
            }
        }
    });
}
