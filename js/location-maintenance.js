$(document).ready(function() {
    $('#location-maintenance-table').dataTable({
    });
    $('#location-closed-table').dataTable({
    });
    $('#close-date-container').hide();
    $('.edit_button').on('click', function(e) {
        $('span#notification').html('');
        var locid = $(this).attr('locid');
        e.preventDefault();
        $.ajax({
            async: false,
            type: "POST",
            url: 'location_maintenance/edit_location',
            data: {locid: $(this).attr('locid')},
            dataType: 'json',
            beforeSend: function() {
                $('#loc-main-modal-dialog').hide();
                $('#loader-img').show();
            },
            success: function(data) {
                $('#loader-img').hide();
                $('#loc-main-modal-dialog').show();
                $('#edit-loc-modal').modal('show');
                $('input[name=locid]').val(locid);
                $('input#practice-name').val(data.edit_locations[0]['practice_name']);
                $('input#location-code').val(data.edit_locations[0]['location_code']);
                $('input#location-name').val(data.edit_locations[0]['location_name']);
                $('input[name=start-date]').val($.datepicker.formatDate('mm/dd/yy', new Date(data.edit_locations[0]['start_date'])));
                $('input#address1').val(data.edit_locations[0]['address_1']);
                $('input#address2').val(data.edit_locations[0]['address_2']);
                $('input#city').val(data.edit_locations[0]['city']);
                $('select#state').val(data.edit_locations[0]['state']);
                $('input#zip-code').val(data.edit_locations[0]['zipcode']);
//                $('input#close-date').val(data.edit_locations[0]['closedate']);
                $('input#collection-rate').val(data.edit_locations[0]['collection_rate']);
                $('input#date-effective').val($.datepicker.formatDate('mm/dd/yy', new Date(data.edit_locations[0]['effective_date'])));
                $("input[name=status][value=" + data.edit_locations[0]['location_status'] + "]").prop('checked', true);
                $("input[name=reporting-mode][value=" + data.edit_locations[0]['reporting_mode'] + "]").prop('checked', true);
                $("input[name=main-office][value=" + data.edit_locations[0]['is_main'] + "]").prop('checked', true);

                if ($('input[name=status]:radio:checked').val() === '1') {
                    $('#close-date-container').hide();
                } else if ($('input[name=status]:radio:checked').val() === '0') {
                    $('#close-date-container').show();
                }

                $('input.status').click(function() {
                    if ($('input[name=status]:radio:checked').val() === '1') {
                        $('#close-date-container').hide();
                    } else if ($('input[name=status]:radio:checked').val() === '0') {
                        $('#close-date-container').show();
                    }
                });


                $('#coll-history').html(data.collection_rate_history);
  
                $('#coll-history').innerHTML = data.collection_rate_history;

            }
        });

    });

    $('button#submit-location-edited').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            async: false,
            type: "POST",
            url: 'location_maintenance/submit_edited_location',
            data: $('form#form-location-edit').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#submit-location-edited').text('sending');
            },
            success: function(data) {
                if (data.status == 'error1') {
                    $('span#notification').html('');
                    $('#submit-location-edited').text('Submit');
                    if (data.locationcode !== '') {
                        error('input[name=location-code]', data.locationcode);
                    } else {
                        reset('input[name=location-code]');
                    }
                    if (data.locationname !== '') {
                        error('input[name=location-name]', data.locationname);
                    } else {
                        reset('input[name=location-name]');
                    }
                    if (data.address1 !== '') {
                        error('input[name=address1]', data.address1);
                    } else {
                        reset('input[name=address1]');
                    }
                    if (data.city !== '') {
                        error('input[name=city]', data.city);
                    } else {
                        reset('input[name=city]');
                    }
                    if (data.state !== '') {
                        error('select[name=state]', data.state);
                    } else {
                        reset('select[name=state]');
                    }
                    if (data.zipcode !== '') {
                        error('input[name=zip-code]', data.zipcode);
                    } else {
                        reset('input[name=city]');
                    }
                    if (data.closedate !== '') {
                        error('input[name=close-date]', data.closedate);
                    } else {
                        reset('input[name=close-date]');
                    }
                    if (data.collectionrate !== '') {
                        error('input[name=collection-rate]', data.collectionrate);
                    } else {
                        reset('input[name=collection-rate]');
                    }
                    if (data.dateeffective !== '') {
                        error('input[name=date-effective]', data.dateeffective);
                    } else {
                        reset('input[name=date-effective]');
                    }
                } else if (data.status === 'success') {
                    $('#submit-location-edited').text('Submit');
                    refresh_all_fields();
                    $('#edit-loc-modal').modal('hide');
                    $('#edit-loc-modal').on('hidden.bs.modal', function() {
                        new PNotify({
                            title: 'Success',
                            text: 'The location has been updated.',
                            type: 'success'
                        });

                        window.setTimeout(function() {
                            location.reload();
                        }, 2000);


                    });



                }
            }

        });


    });



});

function error(element, html) {
    $(element).parent().parent().addClass('has-error');
    $(element).siblings('span').html(html);
}

function reset(element) {
    $(element).parent().parent().removeClass('has-error');
    $(element).siblings('span').html('');
}

function refresh_all_fields() {
    $('input').parent().parent().removeClass('has-error');
    $('input').siblings('span').html('');
}

function empty_fileds() {
    $('input').val('');

}
