<?php ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
   <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fonts/font-face.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/pnotify.custom.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="<?php echo base_url(); ?>js/vendor/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/highcharts.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/exporting.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/jquery.dataTables.js"></script>     
        <script src="<?php echo base_url(); ?>js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/pnotify.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>js/main.js"></script>
</head>
<body>

    <div id="container">
		<div class="pblogo-login">
            <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
        </div>
        <div id="dashboard-login">
            <div id="login-box">
                <form class="form-horizontal login-form" id="login-form">
                    <h3>Performance Tracker</h3>
                    <div class="control-group email-field-group">
                        <label class="control-label" id="lbl-email" for="txt-email">Email</label>
                        <div class="controls">
                            <input type="text" class="form-control txt-email" id="txt-email" name="email" placeholder="Ex. name@email.com">
                        </div>
                    </div>
                    <div class="control-group password-field-group">
                        <label class="control-label" id="lbl-password" for="txt-password" >Password</label>
                        <div class="controls">
                            <input type="password" class="form-control txt-password" id="txt-password" name="password">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls checkbox-div">
                            <input type="checkbox" name="remembe_me" value="1" id="checkbox">
                            <label class="checkbox check-box" id="lbl-checkbox" for="checkbox">Remember me</label>
                            
                        </div> 
                        <div class="login-footer">
                            <p id="lbl-security">For a secure account, please <br/> change your password regularly.</p>
                            <a id="forgot-pass-link" href="#" style="font-size:12px;" data-toggle="modal" data-target="#forgot-modal"><i>Forgot your password?</i></a>
                            <div class="controls btn-login">
                                <button type="submit" style="font-size:15px !important; width:110px !important; height:50px !important;"class="btn-original" id="btn-login" data-loading-text="Logging in...">Login</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- forgot password modal -->
    <div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title"> <strong>Forgot Password </strong></h4>
                </div>

                <div class="modal-body">
                    <form id="forgot-password-form">
                        <div class="form-group">
                            <label for="">Email Address</label>
                            <input type="email" name="email" class="form-control " id="position-code" placeholder="Email Address">
                            <span class="error"></span>
                        </div>


                        <div class="form-group">
                            <label for="">Confirm Email Address</label>
                            <input type="email" name="confirm-email" class="form-control " id="position-name" placeholder="Confirm Email Address">
                            <span class="error"></span>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="reset-password" data-loading-text="Sending Email...">Reset Password</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        
        $('#btn-login').click(function(e) {

            e.preventDefault();
            $.ajax({
                url: "login/login_ajax",
                async: false,
                type: "POST",
                data: $('#login-form').serialize(),
                dataType: "json",
				  beforeSend: function(){
                    $('#btn-login').html('Logging...');
                    $('#btn-login').css('font-size','12px');
                 },  
                success: function(data) {
					
                    if (data.status == 'success') {
                        //error 0 success no errors
                        //error 1 user not found
                        //error 2 account not activated
                        //error 3 password didn't match
                        //error 4 empty fields
                        $('#txt-email').popover('destroy');
                        $('#txt-password').popover('destroy');
                        switch (data.error) {
                            case 0:
                                window.location.href = "<?php echo site_url('pages') ?>";
                                break;
                            case 1:
                                $('#txt-email').attr('data-content','User not found!');
                                $('#txt-email').css('border','1px solid maroon');
                                $('#txt-email').popover('show');
								$('#btn-login').html('Login');
								$('#btn-login').css('font-size','15px');
                                break;
                            case 2:
                                $('#txt-email').attr('data-content','Account is not activated!');
                                $('#txt-email').css('border','1px solid maroon');
                                $('#txt-email').popover('show');
								$('#btn-login').html('Login');
								$('#btn-login').css('font-size','15px');
                                break;
                            case 3:
                                $('#txt-password').attr('data-content','Password did not match!');
                                $('#txt-password').css('border','1px solid maroon');
                                $('#txt-password').popover('show');
								$('#btn-login').html('Login');
								$('#btn-login').css('font-size','15px');
                                break;
                            case 4:
                                $('#txt-email').attr('data-content','Required Field');
                                $('#txt-password').attr('data-content','Required Field');
								
								if( $('#txt-email').val()==''){
								 $('#txt-email').css('border','1px solid maroon');
								   $('#txt-email').popover('show');
								}
								
								
								if( $('#txt-password').val()==''){
								$('#txt-password').css('border','1px solid maroon');
                                $('#txt-password').popover('show');
								}
								
								$('#btn-login').html('Login');
								$('#btn-login').css('font-size','15px');
                               
                               
                                break;
                        }

                    }

                }
            });
        });

        $('#reset-password').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: "login/reset_password",
                async: false,
                type: "POST",
                data: $('#forgot-password-form').serialize(),
                dataType: "json",
                beforeSend: function() {
				
                    $('#reset-password').button('loading');

                },
                success: function(data) {
                    $('#reset-password').button('reset');

                    if (data.status == 'error') {
                        if (data.email !== '') {
                            $('input[name=email]').parent().addClass('has-error');
                            $('input[name=email]').siblings('span').html(data.email);
                        } else {
                            $('input[name=email]').parent().removeClass('has-error');
                            $('input[name=email]').siblings('span').html('');
                        }

                        if (data.confirmemail !== '') {
                            $('input[name=confirm-email]').parent().addClass('has-error');
                            $('input[name=confirm-email]').siblings('span').html(data.confirmemail);
                        } else {
                            $('input[name=confirm-email]').parent().removeClass('has-error');
                            $('input[name=confirm-email]').siblings('span').html('');
                        }
                        new PNotify({
                            title: 'Error',
                            text: 'Please check the data you entered.',
                            type: 'error'
                        });
                    } else if(data.status=='success'){
                        new PNotify({
                            title: 'Success',
                            text: 'Please check your email to get your temporary password.',
                            type: 'success'
                        });
                        
                        $('#forgot-modal').modal('hide');
                        $('input[name=confirm-email]').parent().removeClass('has-error');
                        $('input[name=confirm-email]').siblings('span').html('');
                        $('input[name=confirm-email]').val('');
                        $('input[name=email]').parent().removeClass('has-error');
                        $('input[name=email]').siblings('span').html('');
                        $('input[name=email]').val('');
                        
                    }else if(data.status=='sendfail'){
                           new PNotify({
                            title: 'Error',
                            text: 'Email was not sent. Please contact your administrator.',
                            type: 'error'
                        });
                    }
                }
            });
        });
    </script>

</body>

