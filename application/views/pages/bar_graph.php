<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar'] ?>
                    </div>

                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Bar Graph</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display"></span></p>
                            <div class="btn-toolbar">
                                <div class="btn-group pull-right" >
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Show Only
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" id="btn-showonly">
                                        <!-- dropdown menu links -->
                                        <li class="active"><a href="" class="show-links showonly"  rel="1">Show Both</a></li>
                                        <li><a href="" class="show-links showonly" rel="2">Show Employee Name only</a></li>
                                        <li><a href="" class="show-links showonly" rel="3">Show Employee ID only</a></li>
                                    </ul>
                                </div>  
                                <div class="btn-group pull-right">
                                    <a class="btn btn-user-list" data-toggle="modal" data-target="#user-list-modal" href="#">Clinicians List</a>
                                </div>
                            </div>                 

                            <div id="bar-graph">
                                <div class="dashboard-table">
                                    <div class="graph-header" id="graph-header" style="height: 35px;
                                         background: none repeat scroll 0% 0% #999;
                                         color: #FFF;
                                         font-weight: bold;
                                         text-align: center;
                                         margin-bottom: 10px;
                                         padding-top: 5px;
                                         border-top-left-radius: 5px;
                                         border-top-right-radius: 5px;">
                                    </div>
                                    <ul class="nav nav-tabs" id="bar-graph-tab">
                                        <li class="active dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="">Volume<strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($viewMenu as $val): ?>
                                                    <li><a class="sub-menu"data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>        
                                        </li>

                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="">Billing<strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($billingMenu as $val): ?>
                                                    <li><a class="sub-menu" data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>        
                                        </li>

                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="">Unit Ratios<strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($unitRatiosMenu as $val): ?>
                                                    <li><a class="sub-menu" data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a class="dropdown-toggle"  data-toggle="dropdown" href="">Visit Ratios <strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($visitRatiosMenu as $val): ?>
                                                    <li><a class="sub-menu" data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>                                
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="">Case Management Ratios<strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($managementMenu as $val): ?>
                                                    <li><a class="sub-menu" data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="">Financial Ratios<strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($financialRatiosMenu as $val): ?>
                                                    <li><a class="sub-menu" data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown"  href="">Staffing Ratios<strong class="caret"></strong></a>
                                            <ul class="dropdown-menu bar-graph-li">
                                                <?php foreach ($staffingRatiosMenu as $val): ?>
                                                    <li><a class="sub-menu" data-toggle="tab"  href="" title="<?php echo $val['desc'] ?>"><?php echo $val['item'] ?></a></li>    
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    </ul>

                                    <!-- Container for the bar graph -->
                                    <div id="bar-graph-container">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>

    <!--MODAL for User List-->

    <div class="modal fade" id="user-list-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title"> <strong>Clinicians</strong></h4>   
                    <label class="checkbox" style="margin-top:10px; margin-bottom: -10px">
                        <input type="checkbox" value="" class="toggleall" checked="checked">
                        Select All
                    </label>  
                </div>

                <div class="modal-body">


                    <form id="clinician-list" style="overflow:auto; margin-top:-15px; margin-left: -2px">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="update-graph">Update Graph</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    
    $('#start-date-sidebar').val("<?php echo $graphdata['start_date'];?>");
    $('#end-date-sidebar').val("<?php echo $graphdata['end_date'];?>");
    $('#location-list-sidebar').val("<?php echo $graphdata['location_id'];?>");
    
    
    
    

</script>
