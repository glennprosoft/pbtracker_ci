<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <h2 class="main-title">Add Location</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Add Location</span></p>

                            <form class="form-horizontal" id="location-form" method="POST" role="form">
                                <div class="col-sm-6">
                                   
                                    <div class="form-group">
                                        <label for="location-code" class="col-sm-4 control-label">Location Code</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="location-code" id="location-code" placeholder="Location Code">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="location-name" class="col-sm-4 control-label">Location Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="location-name" id="location-name" placeholder="Location Name">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="start-date" class="col-sm-4 control-label">Start Date</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control global-calendar" name="start-date" placeholder="Start Date">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="address1" class="col-sm-4 control-label">Address 1</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="address1" id="address1" placeholder="Address 1">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address2" class="col-sm-4 control-label">Address 2</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="address2" id="address2" placeholder="Address 2">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="city" class="col-sm-4 control-label">City</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="city" id="city" placeholder="City">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    
                                   
                                    <div class="form-group">
                                        <label for="state" class="col-sm-4 control-label">State</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="state" id="state">
                                                <option value="0" selected="selected">Select a State</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option> 
                                                <option value="AZ">Arizona</option> 
                                                <option value="AR">Arkansas</option> 
                                                <option value="CA">California</option> 
                                                <option value="CO">Colorado</option> 
                                                <option value="CT">Connecticut</option> 
                                                <option value="DE">Delaware</option> 
                                                <option value="DC">District Of Columbia</option> 
                                                <option value="FL">Florida</option> 
                                                <option value="GA">Georgia</option> 
                                                <option value="HI">Hawaii</option> 
                                                <option value="ID">Idaho</option> 
                                                <option value="IL">Illinois</option> 
                                                <option value="IN">Indiana</option> 
                                                <option value="IA">Iowa</option> 
                                                <option value="KS">Kansas</option> 
                                                <option value="KY">Kentucky</option> 
                                                <option value="LA">Louisiana</option> 
                                                <option value="ME">Maine</option> 
                                                <option value="MD">Maryland</option> 
                                                <option value="MA">Massachusetts</option> 
                                                <option value="MI">Michigan</option> 
                                                <option value="MN">Minnesota</option> 
                                                <option value="MS">Mississippi</option> 
                                                <option value="MO">Missouri</option> 
                                                <option value="MT">Montana</option> 
                                                <option value="NE">Nebraska</option> 
                                                <option value="NV">Nevada</option> 
                                                <option value="NH">New Hampshire</option> 
                                                <option value="NJ">New Jersey</option> 
                                                <option value="NM">New Mexico</option> 
                                                <option value="NY">New York</option> 
                                                <option value="NC">North Carolina</option> 
                                                <option value="ND">North Dakota</option> 
                                                <option value="OH">Ohio</option> 
                                                <option value="OK">Oklahoma</option> 
                                                <option value="OR">Oregon</option> 
                                                <option value="PA">Pennsylvania</option> 
                                                <option value="RI">Rhode Island</option> 
                                                <option value="SC">South Carolina</option> 
                                                <option value="SD">South Dakota</option> 
                                                <option value="TN">Tennessee</option> 
                                                <option value="TX">Texas</option> 
                                                <option value="UT">Utah</option> 
                                                <option value="VT">Vermont</option> 
                                                <option value="VA">Virginia</option> 
                                                <option value="WA">Washington</option> 
                                                <option value="WV">West Virginia</option> 
                                                <option value="WI">Wisconsin</option> 
                                                <option value="WY">Wyoming</option>
                                            </select>
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="zip-code" class="col-sm-4 control-label">Zip Code</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="zip-code" id="zip-code" placeholder="Zip Code">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="collection-rate" class="col-sm-4 control-label">Collection Rate &#37;</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="collection-rate" id="collection-rate" placeholder="Collection Rate">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="main-office" class="col-sm-4 control-label">Main Office</label>
                                        <div class="col-sm-6">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="main-office" id="office1" value="1" checked>
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="main-office" id="office2" value="0">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="main-office" class="col-sm-4 control-label">Reporting Mode</label>
                                        <div class="col-sm-6">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="reporting-mode" id="report1" value="0" checked>
                                                    Daily
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="reporting-mode" id="report2" value="1">
                                                    Weekly
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        
                                        <div class="col-sm-offset-4 col-sm-6">
                                            <button type="button" id="submit-location" class="btn btn-default">Submit</button>
                                            <br><span id="notification" style="color:green;"></span>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
