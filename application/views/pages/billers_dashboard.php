<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url()?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Biller's Dashboard</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display"></span></p>
                            <div class="dashboard-toolbar admin-toolbar">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="button" data-toggle="modal" data-target="#billers-report-modal" class="btn btn-default">Print Report</button>
                                    </div>

                                </div>
                            </div>
                            <div class="dashboard-table">
                                <div class="dashboard-table-content" style="display: block;">
                                    <div class="graph-header" id="mindata" style="height: 35px;
                                         background: none repeat scroll 0% 0% #999;
                                         color: #FFF;
                                         font-weight: bold;
                                         text-align: center;
                                         margin-bottom: 10px;
                                         padding-top: 5px;
                                         border-top-left-radius: 5px;
                                         border-top-right-radius: 5px;">
                                    </div>
                                    <form id="billers-form" method="">
                                        <table class="table table-bordered" cellspacing="0" id="billers-table">
                                            <thead>
                                                <tr class="table-head table-week">
                                                    <th  style="color:WHITE">Clinician</th>
                                                    <th class="nosort"><span style="color:#F00">Monday</span><div class="week-dates"></div> </th> 
                                            <th class="nosort"><span style="color:#F00">Tuesday</span><div class="week-dates"></div></th>
                                            <th class="nosort"><span style="color:#F00">Wednesday </span><div class="week-dates"></div></th>
                                            <th class="nosort"><span style="color:#F00">Thursday</span> <div class="week-dates"></div></th> 
                                            <th class="nosort"><span style="color:#F00">Friday </span><div class="week-dates"></div></th>
                                            <th class="nosort"><span style="color:#F00">Saturday</span> <div class="week-dates"></div></th> 
                                            <th class="nosort"><span style="color:#F00">Sunday</span><div class="week-dates"></th>
                                                </tr>
                                                </thead>
                                                <tbody id="biller-table-data-loader">


                                                </tbody>
                                                <?php if(in_array_ar('Biller', $this->session->userdata('roles'))){?>
                                                <tfoot id="savebuttoncontainer"> <tr rowspan="1">
                                                        <td colspan="8" style="text-align: center">
                                                            <button type="button" id="submit-billing" class="btn btn-default" data-loading-text="Saving...">Submit</button>                                           
                                                        </td>
                                                    </tr>    
                                                </tfoot>
                                                <?php }?>
                                                
                                        </table>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

