<div class="container" style="padding-bottom: 15px  !important">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>
                
                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar'] ?>
                    </div>
                    
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <h2 class="main-title">Not Authorized</h2>
                            <div>
                                <p>You are not allowed to access this page. The pages available to you are listed below.</p>
                                <ul>
                                    <?php
                                    foreach ($pages as $page) {
                                        echo '<li>' . $page . '</li>';
                                    }
                                    ?>
                                </ul>

                            </div>

                        </div>                 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
