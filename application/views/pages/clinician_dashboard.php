<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Clinician Dashboard</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Clinician Dashboard</span></p>
                            <div class="dashboard-toolbar admin-toolbar">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="button" data-toggle="modal" data-target="#clinician-report-modal" class="btn btn-default">Print Report</button>
                                    </div>

                                </div>
                            </div>
                            <div class="dashboard-table">
                                <div class="dashboard-table-content" style="display: block;">
                                    <form id="clinician-form" method="POST">
                                        <table class="table table-bordered" cellspacing="0" id="clinician-table">
                                            <thead>
                                                <tr class="table-head">
                                                    <th class="table-date" colspan="10"></th>

                                                </tr>
                                                <tr class="table-head table-week">
                                                    <th colspan="2" class="week-dates"></th>
                                                    <th>Monday<div class="week-dates"></div></th>
                                            <th>Tuesday<div class="week-dates"></div></th>
                                            <th>Wednesday <div class="week-dates"></div></th>
                                            <th>Thursday <div class="week-dates"></div></th>
                                            <th>Friday <div class="week-dates"></div></th>
                                            <th>Saturday <div class="week-dates"></div></th>
                                            <th>Sunday<div class="week-dates"></div></th>
                                            <th>Total<div class="week-dates"></div></th>
                                            </tr>
                                            <tr class="table-volume table-head">
                                                <th colspan="10">Volume</th>

                                            </tr>
                                            </thead>
                                            <tbody id="clinician-table-data-loader">


                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

