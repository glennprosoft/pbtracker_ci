
<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Create User</h2>
                            <p class="breadcrumbs">User Maintenance >> <span class="location-display">Create User</span></p>
                            <form class="form-horizontal" id="user-form" role="form">
                                <h4>User Information</h4>
                                <div class="form-group">
                                    <label for="position-code" class="col-xs-2 control-label">Practice</label>
                                    <div class="col-xs-3">
                                        <select name="practice" class="form-control" id="practice">
                                            <?php
                                            if ($practices) {
                                                foreach ($practices as $prac) {

                                                    echo '<option value="' . $prac['id'] . '">' . $prac['practice_name'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="error"></span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="fname" class="col-xs-2 control-label">Employee ID</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="emp-id" id="first-name" placeholder="Employee ID">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lname" class="col-xs-2 control-label">First Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="first-name" id="first-name" placeholder="Last Name">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lname" class="col-xs-2 control-label">Last Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="last-name" id="last-name" placeholder="Last Name">
                                        <span class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-xs-2 control-label">Email Address</label>
                                    <div class="col-xs-3">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-xs-2 control-label">Roles</label>
                                    <div class="col-xs-3">
                                        <?php
                                        if ($roles) {
                                            foreach ($roles as $role) {
                                                if ($role['id'] != 1) {
                                                    echo '<div class="checkbox">
                                                        <label>
                                                            <input  name="roles[]" class="role-item" type="checkbox" value="' . $role['id']
                                                    . '">
                                                            ' . $role['role_name'] . '
                                                        </label>
                                                    </div>';
                                                }
                                            }
                                        }
                                        ?>

                                        <span class="error"  id="role-error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="position-code" class="col-xs-2 control-label">Position</label>
                                    <div class="col-xs-3">
                                        <select name="position" class="form-control" id="position">
                                            <?php
                                            if ($positions) {
                                                foreach ($positions as $pos) {

                                                    echo '<option value="' . $pos['id'] . '">' . $pos['position_name'] . ' (' . $pos['position_code'] . ')</option>';
                                                }
                                            }
                                            ?>
                                        </select>

                                        <span class="error"></span>
                                    </div>
                                </div>

                                <div id="user-locations">
                                    <div class="form-group">
                                        <label for="" class="col-xs-2 control-label">Locations</label>
                                        <div class="col-xs-10">
                                            <table class="table" style="width:500px; ">
                                                <thead >
                                                    <tr>

                                                        <th colspan="2" style=" text-align: center">Location</th>
                                                        <th class="initial-cases" style=" text-align: center">Initial Cases</th>
                                                        <th style=" text-align: center">Date of First Use</th>

                                                    </tr>
                                                </thead>
                                                <tbody id="loc-body">
                                                    <?php
                                                    if ($locations) {
                                                        foreach ($locations as $loc) {
                                                            echo '<tr>';
                                                            echo '<td style="vertical-align: middle !important">';
                                                            echo '<input type="checkbox" name="location[]" class="location-item" type="checkbox" value="' . $loc['id'] . '">  ';
                                                            echo '</td>';
                                                            echo '<td style="vertical-align: middle !important">';
                                                            echo $loc['location_name'];
                                                            echo '</td>';
                                                            echo '<td style="vertical-align: middle !important" class="initial-cases">';
                                                            echo '<input style="width:140px; margin:auto;" uid="' . $loc['id'] . '"disabled type="text" class="form-control initial-cases-input" name="initial-cases[' . $loc['id'] . ']" value="0" placeholder="Initial Cases">';
                                                            echo '</td>';
                                                            echo '<td style="vertical-align: middle !important">';
                                                            echo '<input  style="width:140px; margin:auto;" uid="' . $loc['id'] . '" disabled type="text" class="form-control date-first date-first-input" name="date-first-use[' . $loc['id'] . ']" placeholder="Date of First Use">';
                                                            echo '</td>';

                                                            echo '</tr>';
                                                        }
                                                    }
                                                    ?>  
                                                </tbody>
                                            </table>
                                        </div>
                                        <span class="error"  id="role-error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-offset-2 col-xs-4">
                                        <button type="button" id="save-user" class="btn btn-default" data-loading-text="Saving...">Save</button>
                                        <!--<button type="button" id="submit-invite" class="btn btn-default">Back</button>-->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.date-first').datepicker();

    $('#save-user').click(function(e) {
        $('td input').each(function() {
            $(this).css({borderColor: '#CCC', });
        });
        $('td input').each(function() {
            $(this).css({borderColor: '#CCC', });
        });

        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'admin_create_user/check_entry',
            data: $('#user-form').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#save-user').button('loading');
            },
            success: function(data) {
                if (data.status == 'error') {
                    if (data.firstname !== '') {
                        error('input[name=first-name]', data.firstname);
                    } else {
                        reset('input[name=first-name]');
                    }
                    if (data.lastname !== '') {
                        error('input[name=last-name]', data.lastname);
                    } else {
                        reset('input[name=last-name]');
                    }
                    if (data.email !== '') {
                        error('input[name=email]', data.email);
                    } else {
                        reset('input[name=email]');
                    }
                    if (data.empid !== '') {
                        error('input[name=emp-id]', data.empid);
                    } else {
                        reset('input[name=empid]');
                    }
                    $.each(data.initialcases, function(key, value) {
                        $(value).css({borderColor: '#A94442', });
                    });

                    $.each(data.datefirst, function(key, value) {
                        $(value).css({borderColor: '#A94442', });
                    });

                    new PNotify({
                        title: 'Error',
                        text: 'Please check the data you entered.',
                        type: 'error'
                    });


                } else if (data.status == 'success') {
                    reset('input');
                    new PNotify({
                        title: 'Success',
                        text: 'The user\'s information has been saved.',
                        type: 'success'
                    });

                    $('input[name="initial-cases[]"]').each(function() {
                        $(value).css({borderColor: '##CCC', });
                    });
                    $('input[name="date-first-use[]"]').each(function() {
                        $(value).css({borderColor: '##CCC', });
                    });

                }

                $('#save-user').button('reset');
            }
        });

    });



    $(".initial-cases").hide();

    $('.dashboard-body').on('click', '.role-item[value="4"]', function() {
        if ($(this).is(":checked")) {
            $(".initial-cases").show();

        } else {
            $(".initial-cases").hide();

        }
    });

    $('.dashboard-body').on('click', '.location-item', function() {
        var id = $(this).val();
        console.log(id);
        var elem = 'initial-cases[' + id + ']';
        var elem2 = 'date-first-use[' + id + ']';
        if ($(this).is(":checked")) {
            $('.initial-cases-input[name="' + elem + '"]').removeAttr('disabled');
            $('.date-first-input[name="' + elem2 + '"]').removeAttr('disabled');
        } else {
            $('.initial-cases-input[name="' + elem + '"]').attr('disabled', 'disabled');
            $('.date-first-input[name="' + elem2 + '"]').attr('disabled', 'disabled');
            $('.initial-cases-input[name="' + elem + '"]').val('0');
            $('.date-first-input[name="' + elem2 + '"]').val('');

        }
    }
    );

    function check_role() {
        if ($('.role-item[value="4"]').is(":checked")) {
            $(".initial-cases").show();

        } else {
            $(".initial-cases").hide();

        }
    }

    $('#practice').change(function() {

        $.ajax({
            type: 'POST',
            url: 'admin_create_user/generate_locations',
            data: 'practice_id=' + $('#practice').val() + '&practice=' + $('#practice').val(),
            dataType: 'json',
            success: function(data) {

                $('#loc-body').empty();
                $('#loc-body').html(data.locations);
                check_role();
                $('.date-first').datepicker();
                $('#position').html(data.posmarkup);


            }
        });
    });
</script>