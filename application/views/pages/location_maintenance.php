<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url()?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Location Maintenance</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Locations</span></p>
                            <div class="dashboard-toolbar admin-toolbar">
                                <h4 class="pull-left">Active Locations</h4>
                                <?php if(in_array_ar('Office Administrator', $this->session->userdata('roles')) || in_array_ar('Super Administrator', $this->session->userdata('roles'))){
                                    
                                }else { ?>
                                <button class="btn btn-default pull-right" onclick="window.window.location.href = '<?php echo site_url(); ?>location_maintenance/add_location'">Create Location</button>
                                <?php } ?>
                            </div>
                            <div class="dashboard-table-content">

                                <table class="table table-striped table-bordered" cellspacing="0" id="location-maintenance-table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Rate</th>
                                            <th>Start Date</th>
                                            <th>Address 1</th>
                                         
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Zip Code</th>
                                            <th>Main Office</th>
                                            <!--<th>Edited by</th>-->
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if($locations){
                                            foreach ($locations as $loc) { ?>
                                            <tr>
                                                <td><?php echo $loc['location_code']; ?></td>
                                                <td><?php echo $loc['location_name']; ?></td>
                                                <td><?php echo $loc['collection_rate']; ?> %</td>
                                                <td><?php echo date('m/d/Y', strtotime($loc['start_date'])); ?></td>
                                                <td><?php echo $loc['address_1']; ?></td>
                                               
                                             
                                                <td><?php echo $loc['city']; ?></td>
                                                     <td><?php echo $loc['state']; ?></td>
                                                     <td><?php echo $loc['zipcode']; ?></td>
                                                <td style="text-align: center"><?php
                                                    if ($loc['is_main'] == '0') {
                                                        echo 'No';
                                                    } else {
                                                        echo 'Yes';
                                                    }
                                                    ?></td>
                                          
                                                <td>
                                                    <a href="#" class="edit_button" locid="<?php echo $loc['location_id'] ?>" >
                                                        <img src="<?php echo base_url(); ?>img/pencil.png">
                                                    </a>
                                                </td>

                                            </tr>
                                            <?php }} ?>
                                    </tbody>
                                </table>
                                <!--Modal-->
                                <div class="modal fade" id="edit-loc-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <img src="<?php echo base_url();?>img/ajax-loader.gif" id="loader-img"class="center-block" style="margin-top:100px;width:150px;">
                                    <div class="modal-dialog modal-lg" id="loc-main-modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel"><strong>Edit Location</strong></h4>
                                            </div>
                                            <form class="form-horizontal" id="form-location-edit">
                                                <div class="modal-body" id="edit-loc-body">

                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="practice-name" class="col-sm-4 control-label">Practice Name</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" disabled name="practice-name" id="practice-name" placeholder="Practice Name">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="location-code" class="col-sm-4 control-label">Location Code</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="location-code" id="location-code" placeholder="Location Code">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="location-name" class="col-sm-4 control-label">Location Name</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="location-name" id="location-name" placeholder="Location Name">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="start-date" class="col-sm-4 control-label">Start Date</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" disabled class="form-control global-calendar" name="start-date" placeholder="Baseline Date">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="address1" class="col-sm-4 control-label">Address 1</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="address1" id="address1" placeholder="Address 1">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="address2" class="col-sm-4 control-label">Address 2</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="address2" id="address2" placeholder="Address 2">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">

                                                            <div class="form-group">
                                                                <label for="city" class="col-sm-4 control-label">City</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="city" id="city" placeholder="City">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="state" class="col-sm-4 control-label">State</label>
                                                                <div class="col-sm-6">
                                                                    <select class="form-control" name="state" id="state">
                                                                        <option value="0" selected="selected">Select a State</option>
                                                                        <option value="AL">Alabama</option>
                                                                        <option value="AK">Alaska</option> 
                                                                        <option value="AZ">Arizona</option> 
                                                                        <option value="AR">Arkansas</option> 
                                                                        <option value="CA">California</option> 
                                                                        <option value="CO">Colorado</option> 
                                                                        <option value="CT">Connecticut</option> 
                                                                        <option value="DE">Delaware</option> 
                                                                        <option value="DC">District Of Columbia</option> 
                                                                        <option value="FL">Florida</option> 
                                                                        <option value="GA">Georgia</option> 
                                                                        <option value="HI">Hawaii</option> 
                                                                        <option value="ID">Idaho</option> 
                                                                        <option value="IL">Illinois</option> 
                                                                        <option value="IN">Indiana</option> 
                                                                        <option value="IA">Iowa</option> 
                                                                        <option value="KS">Kansas</option> 
                                                                        <option value="KY">Kentucky</option> 
                                                                        <option value="LA">Louisiana</option> 
                                                                        <option value="ME">Maine</option> 
                                                                        <option value="MD">Maryland</option> 
                                                                        <option value="MA">Massachusetts</option> 
                                                                        <option value="MI">Michigan</option> 
                                                                        <option value="MN">Minnesota</option> 
                                                                        <option value="MS">Mississippi</option> 
                                                                        <option value="MO">Missouri</option> 
                                                                        <option value="MT">Montana</option> 
                                                                        <option value="NE">Nebraska</option> 
                                                                        <option value="NV">Nevada</option> 
                                                                        <option value="NH">New Hampshire</option> 
                                                                        <option value="NJ">New Jersey</option> 
                                                                        <option value="NM">New Mexico</option> 
                                                                        <option value="NY">New York</option> 
                                                                        <option value="NC">North Carolina</option> 
                                                                        <option value="ND">North Dakota</option> 
                                                                        <option value="OH">Ohio</option> 
                                                                        <option value="OK">Oklahoma</option> 
                                                                        <option value="OR">Oregon</option> 
                                                                        <option value="PA">Pennsylvania</option> 
                                                                        <option value="RI">Rhode Island</option> 
                                                                        <option value="SC">South Carolina</option> 
                                                                        <option value="SD">South Dakota</option> 
                                                                        <option value="TN">Tennessee</option> 
                                                                        <option value="TX">Texas</option> 
                                                                        <option value="UT">Utah</option> 
                                                                        <option value="VT">Vermont</option> 
                                                                        <option value="VA">Virginia</option> 
                                                                        <option value="WA">Washington</option> 
                                                                        <option value="WV">West Virginia</option> 
                                                                        <option value="WI">Wisconsin</option> 
                                                                        <option value="WY">Wyoming</option>
                                                                    </select>
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="zip-code" class="col-sm-4 control-label">Zip Code</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="zip-code" id="zip-code" placeholder="Zip Code">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="main-office" class="col-sm-4 control-label">Main Office</label>
                                                                <div class="col-sm-6">
                                                                    <div class="radio-inline">
                                                                        <label>
                                                                            <input type="radio" name="main-office" id="office1" value="1" checked>
                                                                            Yes
                                                                        </label>
                                                                    </div>
                                                                    <div class="radio-inline">
                                                                        <label>
                                                                            <input type="radio" name="main-office" id="office2" value="0">
                                                                            No
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-4 control-label">Reporting Mode</label>
                                                                <div class="col-sm-6">
                                                                    <div class="radio-inline">
                                                                        <label>
                                                                            <input type="radio" name="reporting-mode" id="report1" value="0" checked>
                                                                            Daily
                                                                        </label>
                                                                    </div>
                                                                    <div class="radio-inline">
                                                                        <label>
                                                                            <input type="radio" name="reporting-mode" id="report2" value="1">
                                                                            Weekly
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-4 control-label">Status</label>
                                                                <div class="col-sm-6">
                                                                    <div class="radio-inline">
                                                                        <label>
                                                                            <input type="radio" class="status" name="status" id="stat1" value="1" checked>
                                                                            Active
                                                                        </label>
                                                                    </div>
                                                                    <div class="radio-inline">
                                                                        <label>
                                                                            <input type="radio" class="status" name="status" id="stat2" value="0">
                                                                            Closed
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="close-date-container">
                                                                <label for="close-date" class="col-sm-4 control-label">Close Date</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="global-calendar form-control" name="close-date" id="close-date" placeholder="Close Date">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="collection-rate" class="col-sm-4 control-label">Collection Rate &#37;</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="collection-rate" id="collection-rate" placeholder="Collection Rate">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="collection-rate" class="col-sm-4 control-label">Date Effective</label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control global-calendar" name="date-effective" id="date-effective" placeholder="Date Effective" value="">
                                                                    <span class="error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-12">
                                                                <div class="history-table-title">Collection Rate History</div>
                                                                <table class="table table-bordered history-table">
                                                                    <thead>
                                                                        <tr >
                                                                            <th style="text-align:center">Collection Rate</th>
                                                                            <th style="text-align:center">Date Effective</th>
                                                                            <th style="text-align:center">Edited By</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="coll-history">
                                                                        
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">

                                                        <div class="col-sm-offset-4 col-sm-6">
                                                            <input type="hidden" name="locid">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" id="submit-location-edited" class="btn btn-default">Submit</button>
                                                            <br><span id="notification" style="color:green;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--End of modal-->
                            </div>
                            <div class="dashboard-toolbar admin-toolbar">
                                <h4 class="pull-left">Closed Location</h4>
                            </div>
                            <div class="dashboard-table-content">
                                <table class="table table-striped table-bordered" cellspacing="0" id="location-closed-table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Rate</th>
                                            <th>Date Closed</th>
                                            <th>Address 1</th>
                                       
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Zip Code</th>
                                            <th>Main Office</th>
                                            
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($locations_closed){
                                            foreach ($locations_closed as $loc) { ?>
                                            <tr>
                                                <td><?php echo $loc['location_code']; ?></td>
                                                <td><?php echo $loc['location_name']; ?></td>
                                                <td><?php echo $loc['collection_rate']; ?> %</td>
                                                <td><?php echo date('m/d/Y', strtotime($loc['close_date'])); ?></td>
                                                <td><?php echo $loc['address_1']; ?></td>
                                         
                                             
                                                <td><?php echo $loc['city']; ?></td>
                                                     <td><?php echo $loc['state']; ?></td>
                                                     <td><?php echo $loc['zipcode']; ?></td>
                                                <td style="text-align: center"><?php
                                                    if ($loc['is_main'] == '0') {
                                                        echo 'No';
                                                    } else {
                                                        echo 'Yes';
                                                    }
                                                    ?></td>
                                          
                                                <td>
                                                    <a href="#" class="edit_button" locid="<?php echo $loc['location_id'] ?>" >
                                                        <img src="<?php echo base_url(); ?>img/pencil.png">
                                                    </a>
                                                </td>

                                            </tr>
                                        <?php } }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>