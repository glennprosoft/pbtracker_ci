<tr>
    <td>1</td>
    <td title="Total number of new evaulations">New Cases</td>
    <td><input type="text" tabindex="1" name="newcase[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>"  class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['new_cases'] ?>"></td>
    <td><input type="text" tabindex="11" name="newcase[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['new_cases'] ?>"></td>
    <td><input type="text" tabindex="21" name="newcase[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['new_cases'] ?>"></td>
    <td><input type="text" tabindex="31" name="newcase[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['new_cases'] ?>"></td>
    <td><input type="text" tabindex="41" name="newcase[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['new_cases'] ?>"></td>
    <td><input type="text" tabindex="51" name="newcase[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['new_cases'] ?>"></td>
    <td><input type="text" tabindex="61" name="newcase[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['new_cases'] ?>"></td>
    <td><?php echo $clinician_total['new_cases']; ?></td>
</tr>
<tr>
    <td>2</td>
    <td title="Patients discharged by therapist">Active Discharges</td>
    <td><input type="text" tabindex="2" name="activedis[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['active_discharges'] ?>"></td>
    <td><input type="text" tabindex="12" name="activedis[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['active_discharges'] ?>"></td>
    <td><input type="text" tabindex="22" name="activedis[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['active_discharges'] ?>"></td>
    <td><input type="text" tabindex="32" name="activedis[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['active_discharges'] ?>"></td>
    <td><input type="text" tabindex="42" name="activedis[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['active_discharges'] ?>"></td>
    <td><input type="text" tabindex="52" name="activedis[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['active_discharges'] ?>"></td>
    <td><input type="text" tabindex="62" name="activedis[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['active_discharges'] ?>"></td>
    <td><?php echo $clinician_total['active_discharges']; ?></td>
</tr>
<tr>
    <td>3</td>
    <td title="Self Discharges">Passive Discharges</td>
    <td><input type="text" tabindex="3" name="passivedis[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['passive_discharges'] ?>"></td>
    <td><input type="text" tabindex="13" name="passivedis[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['passive_discharges'] ?>"></td>
    <td><input type="text" tabindex="23" name="passivedis[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['passive_discharges'] ?>"></td>
    <td><input type="text" tabindex="33" name="passivedis[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['passive_discharges'] ?>"></td>
    <td><input type="text" tabindex="43" name="passivedis[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['passive_discharges'] ?>"></td>
    <td><input type="text" tabindex="53" name="passivedis[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['passive_discharges'] ?>"></td>
    <td><input type="text" tabindex="63" name="passivedis[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['passive_discharges'] ?>"></td>
    <td><?php echo $clinician_total['passive_discharges']; ?></td>
</tr>
<tr>
    <td>4</td>
    <td title="Total cases not yet discharged">Active Cases</td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['active_cases'] ?>"></td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['active_cases'] ?>"></td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['active_cases'] ?>"></td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['active_cases'] ?>"></td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['active_cases'] ?>"></td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['active_cases'] ?>"></td>
    <td><input type="text" tabindex="-1" name="activecase[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" readonly class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['active_cases'] ?>"></td>
    <td><?php echo $clinician_total['active_cases']; ?></td>
</tr>
<tr>
    <td>5</td>
    <td title="Appointments that are kept">Visits Attended</td>
    <td><input type="text" tabindex="4" name="visitsattend[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['visits_attended'] ?>"></td>
    <td><input type="text" tabindex="14" name="visitsattend[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['visits_attended'] ?>"></td>
    <td><input type="text" tabindex="24" name="visitsattend[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['visits_attended'] ?>"></td>
    <td><input type="text" tabindex="34" name="visitsattend[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['visits_attended'] ?>"></td>
    <td><input type="text" tabindex="44" name="visitsattend[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['visits_attended'] ?>"></td>
    <td><input type="text" tabindex="54" name="visitsattend[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['visits_attended'] ?>"></td>
    <td><input type="text" tabindex="64" name="visitsattend[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['visits_attended'] ?>"></td>
    <td><?php echo $clinician_total['visits_attended']; ?></td>
</tr>
<tr>
    <td>6</td>
    <td title="Appointments not kept and not rescheduled with at least 24 hours notice">Visits Cancels &amp; No shows</td>
    <td><input type="text" tabindex="5" name="visitscancels[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['no_show'] ?>"></td>
    <td><input type="text" tabindex="15" name="visitscancels[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['no_show'] ?>"></td>
    <td><input type="text" tabindex="25" name="visitscancels[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['no_show'] ?>"></td>
    <td><input type="text" tabindex="35" name="visitscancels[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['no_show'] ?>"></td>
    <td><input type="text" tabindex="45" name="visitscancels[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['no_show'] ?>"></td>
    <td><input type="text" tabindex="55" name="visitscancels[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['no_show'] ?>"></td>
    <td><input type="text" tabindex="65" name="visitscancels[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['no_show'] ?>"></td>
    <td><?php echo $clinician_total['no_show']; ?></td>
</tr>
<tr>
    <td>7</td>
    <td title="Visits Scheduled = Visits Attended + Visits Cancels & No-Shows">Visits Scheduled</td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[1]; ?>]" readonly date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['visits_scheduled'] ?>"></td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[2]; ?>]" readonly date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['visits_scheduled'] ?>"></td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[3]; ?>]" readonly date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['visits_scheduled'] ?>"></td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[4]; ?>]" readonly date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['visits_scheduled'] ?>"></td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[5]; ?>]" readonly date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['visits_scheduled'] ?>"></td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[6]; ?>]" readonly date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['visits_scheduled'] ?>"></td>
    <td><input type="text" tabindex="-1" name="visitssched[<?php echo $week_date_attr[7]; ?>]" readonly date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['visits_scheduled'] ?>"></td>
    <td><?php echo $clinician_total['visits_scheduled']; ?></td>
</tr>
<tr>
    <td>8</td>
    <td title="Units Charged">Units Charged</td>
    <td><input type="text" tabindex="7" name="unitscharged[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[1]]['units_charged']; ?>"></td>
    <td><input type="text" tabindex="17" name="unitscharged[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[2]]['units_charged']; ?>"></td>
    <td><input type="text" tabindex="27" name="unitscharged[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[3]]['units_charged']; ?>"></td>
    <td><input type="text" tabindex="37" name="unitscharged[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[4]]['units_charged']; ?>"></td>
    <td><input type="text" tabindex="47" name="unitscharged[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[5]]['units_charged']; ?>"></td>
    <td><input type="text" tabindex="57" name="unitscharged[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[6]]['units_charged']; ?>"></td>
    <td><input type="text" tabindex="67" name="unitscharged[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" value="<?php echo $clinician_data[$week_date_attr[7]]['units_charged']; ?>"></td>
    <td><?php echo $clinician_total['units_charged']; ?></td>
</tr>
<tr>
    <td>9</td>
    <td title="Days Worked">Days Worked</td>
    <td><input type="text" tabindex="8" name="daysworked[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[1]]['days_worked'] ?>"></td>
    <td><input type="text" tabindex="18" name="daysworked[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[2]]['days_worked'] ?>"></td>
    <td><input type="text" tabindex="28" name="daysworked[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[3]]['days_worked'] ?>"></td>
    <td><input type="text" tabindex="38" name="daysworked[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[4]]['days_worked'] ?>"></td>
    <td><input type="text" tabindex="48" name="daysworked[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[5]]['days_worked'] ?>"></td>
    <td><input type="text" tabindex="58" name="daysworked[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[6]]['days_worked'] ?>"></td>
    <td><input type="text" tabindex="68" name="daysworked[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input not-decimal" readonly value="<?php echo $clinician_data[$week_date_attr[7]]['days_worked'] ?>"></td>
    <td><?php echo $clinician_total['days_worked']; ?></td>
</tr>
<tr>
    <td>10</td>
    <td title="Total number of hours worked including patient care meetings, documentation, and case management">Hours Worked</td>
    <td><input type="text" tabindex="9" name="hoursworked[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[1]]['hours_worked'], 2); ?>"></td>
    <td><input type="text" tabindex="19" name="hoursworked[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[2]]['hours_worked'], 2); ?>"></td>
    <td><input type="text" tabindex="29" name="hoursworked[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[3]]['hours_worked'], 2); ?>"></td>
    <td><input type="text" tabindex="39" name="hoursworked[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[4]]['hours_worked'], 2); ?>"></td>
    <td><input type="text" tabindex="49" name="hoursworked[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[5]]['hours_worked'], 2); ?>"></td>
    <td><input type="text" tabindex="59" name="hoursworked[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[6]]['hours_worked'], 2); ?>"></td>
    <td><input type="text" tabindex="69" name="hoursworked[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[7]]['hours_worked'], 2); ?>"></td>
    <td><?php echo number_format($clinician_total['hours_worked'], 2); ?></td>
</tr>
<tr>
    <td>11</td>
    <td title="Total aggregate hours scheduled (e.g 2 patients scheduled for one hour each = 2 hours scheduled, even if patients are scheduled at the same time) ">Clinical Schedules</td>
    <td><input type="text" tabindex="10" name="clinicshed[<?php echo $week_date_attr[1]; ?>]" date="<?php echo $week_date_attr[1]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[1]]['hours_scheduled'], 2); ?>"></td>
    <td><input type="text" tabindex="20" name="clinicshed[<?php echo $week_date_attr[2]; ?>]" date="<?php echo $week_date_attr[2]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[2]]['hours_scheduled'], 2); ?>"></td>
    <td><input type="text" tabindex="30"name="clinicshed[<?php echo $week_date_attr[3]; ?>]" date="<?php echo $week_date_attr[3]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[3]]['hours_scheduled'], 2); ?>"></td>
    <td><input type="text" tabindex="40" name="clinicshed[<?php echo $week_date_attr[4]; ?>]" date="<?php echo $week_date_attr[4]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[4]]['hours_scheduled'], 2); ?>"></td>
    <td><input type="text" tabindex="50" name="clinicshed[<?php echo $week_date_attr[5]; ?>]" date="<?php echo $week_date_attr[5]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[5]]['hours_scheduled'], 2); ?>"></td>
    <td><input type="text" tabindex="60" name="clinicshed[<?php echo $week_date_attr[6]; ?>]" date="<?php echo $week_date_attr[6]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[6]]['hours_scheduled'], 2); ?>"></td>
    <td><input type="text" tabindex="70" name="clinicshed[<?php echo $week_date_attr[7]; ?>]" date="<?php echo $week_date_attr[7]; ?>" class="form-control clinician-input decimal" value="<?php echo number_format($clinician_data[$week_date_attr[7]]['hours_scheduled'], 2); ?>"></td>
    <td><?php echo number_format($clinician_total['hours_scheduled'], 2); ?></td>
</tr>
<tr id="tr-check">
    <td></td>
    <td title=""><a style="color:black;">Save Data</a></td><pre><?php print_r($clinician_data[$week_date_attr[3]]['id']); ?></pre>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[1]" date="<?php echo $week_date_attr[1]; ?>" value="<?php echo $clinician_data[$week_date_attr[1]]['id']; ?>"></td>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[2]" date="<?php echo $week_date_attr[2]; ?>" value="<?php echo $clinician_data[$week_date_attr[2]]['id']; ?>"></td>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[3]" date="<?php echo $week_date_attr[3]; ?>" value="<?php echo $clinician_data[$week_date_attr[3]]['id']; ?>"></td>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[4]" date="<?php echo $week_date_attr[4]; ?>" value="<?php echo $clinician_data[$week_date_attr[4]]['id']; ?>"></td>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[5]" date="<?php echo $week_date_attr[5]; ?>" value="<?php echo $clinician_data[$week_date_attr[5]]['id']; ?>"></td>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[6]" date="<?php echo $week_date_attr[6]; ?>" value="<?php echo $clinician_data[$week_date_attr[6]]['id']; ?>"></td>
    <td style="padding-right: 27px;"><input class="input-check" style="width: 18px; height: 18px;" type="checkbox" name="check-col[7]" date="<?php echo $week_date_attr[7]; ?>" value="<?php echo $clinician_data[$week_date_attr[7]]['id']; ?>"></td>
    <td style="text-align:right;vertical-align:middle"></td>
</tr>
<tr>
    <td colspan="10" class="save-container"><button class="btn btn-default save-button" id="clinician-save-button">Save</button> <a class="close-weekly-button" href="#">Close Weekly Data</a></td>

</tr>
<tr class="table-sub-header">
    <td colspan="9" class="billing-container">Billing</td>
    <td class="total">Total</td>
</tr>
<tr>
    <td>12</td>
    <td title="Total $ amount charges for services">Charges</td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['charges']; ?></td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['charges']; ?></td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['charges']; ?></td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['charges']; ?></td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['charges']; ?></td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['charges']; ?></td>
    <td class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['charges']; ?></td>
    <td class="numbers">$ <?php echo number_format($clinician_total['charges'],2); ?></td>
</tr>
<tr>
    <td>13</td>
    <td title="Estimated Collections = Charge * Collection Rate">Estimated<br>Collections</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['collected']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['collected']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['collected']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['collected']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['collected']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['collected']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['collected']; ?></td>
    <td  class="numbers">$ <?php echo number_format($clinician_total['collected'], 2); ?></td>
</tr>
<tr>
    <td>14</td>
    <td title="Number of dates for which services were delivered">Billing Days</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['bdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['bdays']; ?></td>
</tr>
<tr class="table-sub-header">
    <td colspan="9" class="billing-container">Unit Ratios</td>
    <td class="total">Average</td>
</tr>
<tr>
    <td>15</td>
    <td title="Units/Visit = Units Charged/Visits Attended">Units / Visit</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['units_visits']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['units_visits']; ?></td>
</tr>
<tr>
    <td>16</td>
    <td title="Units/Active Case = Units Charged/Active Cases">Units / Active Case</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['unitsactive']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['unitsactive']; ?></td>
</tr>
<tr>
    <td>17</td>
    <td title="Units/Hour Worked = Units Charged/Hours Worked">Units / Hour Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['unitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['unitshour']; ?></td>
</tr>
<tr>
    <td>18</td>
    <td title="Units/8 Hours Worked = Units Charged/(Hours Worked/8)">Units / 8 Hours Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['units8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['units8hour']; ?></td>
</tr>
<tr>
    <td>19</td>
    <td title="Charge/Unit = Charges/Units charged">Charge / Unit</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['chargeunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['chargeunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['chargeunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['chargeunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['chargeunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['chargeunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['chargeunit']; ?></td>
    <td  class="numbers">$ <?php echo $clinician_total['chargeunit']; ?></td>
</tr>
<tr>
    <td>20</td>
    <td title="Collected/Unit = Estimated Collections/Units Charged">Collected / Unit</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['collectedunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['collectedunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['collectedunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['collectedunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['collectedunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['collectedunit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['collectedunit']; ?></td>
    <td  class="numbers">$ <?php echo $clinician_total['collectedunit']; ?></td>
</tr>
<tr class="table-sub-header">
    <td colspan="9" class="billing-container">Visit Ratios</td>
    <td class="total">Average</td>
</tr>
<tr>
    <td>21</td>
    <td title="Visits/Case = Total Visits/Active Cases">Visits / Case</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['visitscase']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['visitscase']; ?></td>
</tr>
<tr>
    <td>22</td>
    <td title="Visits/Hour Worked = Total Visits/Hours Worked">Visits / Hour<br> Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['visitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['visitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['visitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['visitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['visitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['visitshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['visitshour']; ?></td>

    <td  class="numbers"><?php echo $clinician_total['visitshour']; ?></td>
</tr>
<tr>
    <td>23</td>
    <td title="Visits/8 Hours Worked = Total Visits(Hours Worked/8)">Visits / 8 Hours<br> Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['visits8hour']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['visits8hour']; ?></td>
</tr>
<tr>
    <td>24</td>
    <td title="Charge/Visit Attended = Charges/Total Visits">Charges / Visit</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['chargevisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['chargevisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['chargevisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['chargevisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['chargevisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['chargevisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['chargevisit']; ?></td>
    <td  class="numbers">$ <?php echo $clinician_total['chargevisit']; ?></td>
</tr>
<tr>
    <td>25</td>
    <td title="Collected Visit = Estimated Collections/Total Visits">Collected Visit / Visit</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['collectedvisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['collectedvisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['collectedvisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['collectedvisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['collectedvisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['collectedvisit']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['collectedvisit']; ?></td>
    <td  class="numbers">$ <?php echo $clinician_total['collectedvisit']; ?></td>
</tr>
<tr class="table-sub-header">
    <td colspan="9" class="billing-container">Case Management Ratios</td>
    <td class="total">Average</td>
</tr>
<tr>
    <td>26</td>
    <td title="Active Cases/Day = Active Cases/Days Worked">Active Cases / Day</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['casesday']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['casesday']; ?></td>
</tr>
<tr>
    <td>27</td>
    <td title="Passive Discharge Rate = Passive Discharges/(Active Discharges + Passive Discharge)">Passive Discharge Rate</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['passiverate']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['passiverate']; ?> &percnt;</td>
</tr>
<tr>
    <td>28</td>
    <td title="Attendance Rate = Visits Attended/Visits Scheduled">Attendance Rate</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['attendrate']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['attendrate']; ?> &percnt;</td>
</tr>
<tr class="table-sub-header">
    <td colspan="9" class="billing-container">Financial Ratios</td>
    <td class="total">Average</td>
</tr>
<tr>
    <td>29</td>
    <td title="Charges/Hour Worked = Charges/Hours Worked">Charge / Hour <br> Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['chargeshour']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['chargeshour']; ?></td>
</tr>
<tr>
    <td>30</td>
    <td title="Collected/Hour Worked = Estimated Collections/Hours Worked">Collected / Hour <br> Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['collectedhour']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['collectedhour']; ?></td>
</tr>
<tr class="table-sub-header">
    <td colspan="9" class="billing-container">Staffing Ratios</td>
    <td class="total">Average</td>
</tr>
<tr>
    <td>31</td>
    <td title="Units/Billing Day = Units Charged/Billing Days">Units / Billing Day</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['unitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['unitsbilling']; ?></td>
</tr>
<tr>
    <td>32</td>
    <td title="Visits/Billing Day = Visits Attended/Billing Days">Visits / Billing Day</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['visitsbilling']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['visitsbilling']; ?></td>
</tr>
<tr>
    <td>33</td>
    <td title="Total Hours Worked/Days Worked = Hours Worked/Days Worked">Total Hours <br> Worked / Days <br> Worked</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['thourdays']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['thourdays']; ?></td>
</tr>
<tr>
    <td>34</td>
    <td title="Scheduled Density = Clinical Hours Scheduled/Hours Worked">Scheduled Density</td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[1]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[2]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[3]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[4]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[5]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[6]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_computation[$week_date_attr[7]]['scheddensity']; ?></td>
    <td  class="numbers"><?php echo $clinician_total['scheddensity']; ?></td>
</tr>

<!--</form>-->

<script>



    high_light_date('<?php echo $selected_date; ?>');
    disable_dates('<?php echo $baseline_date; ?>', '<?php echo $termination_date; ?>', '<?php echo $today; ?>');
    $('select#location-list-sidebar').change(function() {
        location_id_var = $(this).val();

    });

<?php if (isset($type_request)) { ?>
        var json = '<?php echo $type_request; ?>';
        var obj = JSON.parse(json);
    <?php for ($ctr = 1; $ctr <= 7; $ctr++) { ?>
            if (obj[<?php echo $ctr; ?>].hours_worked == 1) {
                //                console.log('nyur nyur');
                $('input.clinician-input[name="hoursworked[<?php echo $week_date_attr[$ctr]; ?>]"]').each(function() {
                    if ($(this).attr('date') === '<?php echo $week_date_attr[$ctr]; ?>') {
                        $(this).parents('td').css('background', '#f2dede');
                    }
                });
            } else {

            }
            if (obj[<?php echo $ctr; ?>].update == 1) {
                $('input.clinician-input').each(function() {
                    if ($(this).attr('date') === '<?php echo $week_date_attr[$ctr]; ?>') {

                    }
                });
            } else {

            }
            if (obj[<?php echo $ctr; ?>].deleted == 1) {
                $('input.clinician-input').each(function() {
                    if ($(this).attr('date') === '<?php echo $week_date_attr[$ctr]; ?>') {
                        $(this).parents('td').css('background', '#3a87ad');
                    }
                });
            } else {

            }
            if (obj[<?php echo $ctr; ?>].created == 1) {
                $('input.clinician-input').each(function() {
                    if ($(this).attr('date') === '<?php echo $week_date_attr[$ctr]; ?>') {
                        $(this).parents('td').css('background', '#d4ebcb');
                    }
                });
            } else {

            }

    <?php } ?>


<?php } ?>




    $('.clinician-input').each(function() {
        if ($(this).val() === '') {
            $(this).val('0');
        }

        $(this).on('focus', function() {
            if ($(this).val() === '0') {
                $(this).val('');
            }

        });
        $(this).on('focusout', function() {
            if (!$(this).val()) {
                $(this).val('0');
            }

        });
    });


    $('#clinician-save-button').on('click', function(e) {
        e.preventDefault();
//        var formdata = $('#clinician-form').serialize();

        var location_list_id = location_id_var;
        $.ajax({
            async: false,
            type: 'POST',
            url: 'clinician_dashboard/submit_clinician_data',
            data: $('#clinician-form').serialize() + '&location_id=' + location_list_id + '&selected_date=<?php echo $selected_date; ?>',
            dataType: 'json',
            beforeSend: function() {
                $('#clinician-save-button').html('Saving...');
            },
            success: function(data) {
                var hours_worked_error = false;
                var update = false;
                var deleted = false;
                var created = false;
                var error_date_arr = new Array();
                for (var ctr = 1; ctr <= 7; ctr++) {
                    if (data.status[ctr]['update']) {
                        if (data.status[ctr]['update'] === 1) {
                            update = true;
                            error_date_arr[ctr] = {update: 1};
                        } else {
                            update = false;
                            error_date_arr[ctr] = {update: 0};
                        }
                    } else {
                        error_date_arr[ctr] = {update: 0};
                    }

                    if (data.status[ctr]['delete']) {
                        if (data.status[ctr]['delete'] === 1) {
                            deleted = true;
                            error_date_arr[ctr] = {deleted: 1};
                        } else {
                            deleted = false;
                            error_date_arr[ctr] = {deleted: 0};
                        }
                    }
                    if (data.status[ctr]['save']) {
                        if (data.status[ctr]['save'] === 1) {
                            created = true;
                            error_date_arr[ctr] = {created: 1};
                        } else {
                            created = false;
                            error_date_arr[ctr] = {created: 0};
                        }
                    }
                    if (data.status[ctr]['hours_worked_error']) {
                        if (data.status[ctr]['hours_worked_error'] === 1) {
                            hours_worked_error = true;
                            error_date_arr[ctr] = {hours_worked: 1};
                        } else {
                            hours_worked_error = false;
                            error_date_arr[ctr] = {hours_worked: 0};
                        }
                    }

                }


                if (hours_worked_error) {
                    new PNotify({
                        text: 'Data not save because Hours worked is 0',
                        type: 'error'
                    });
                }
                if (created) {
                    new PNotify({
                        text: 'Data was created',
                        type: 'success'
                    });
                } else if (deleted) {
                    new PNotify({
                        text: 'The column highlighted in blue was deleted.',
                        type: 'info'
                    });
                } else if (update) {
                    new PNotify({
                        text: 'Data was updated',
                        type: 'success'
                    });
                }



                var date = $(".calendar-box").datepicker({dateFormat: 'dd-mm-yy'}).val();
                $('#clinician-table-data-loader').load('clinician_dashboard/load_clinician_table', {'date_selected': date, location_id: location_list_id, 'is_error': error_date_arr});




            }
        });

    });


    $("input.not-decimal").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $("input.decimal").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57) || e.which === 47) {
            return false;
        }
    });

    var date;
    function high_light_date(date) {
        $("input.clinician-input").each(function(index, value) {
            if ($(this).attr('date') === date) {
                $(this).parents('td').css('background', '#eeeeee');
            }
        });


    }

    $("input.clinician-input").each(function(index, value) {
        if ($(this).val() == '0.00') {
            $(this).val('0');
        }
    });

    function high_light_error(column_error) {
<?php for ($ctr = 1; $ctr <= 7; $ctr++) { ?>
            $("input.clinician-input").each(function(index, value) {
                if ($(this).attr('date') == '<?php echo $week_date_attr[$ctr]; ?>') {


                }

            });
<?php } ?>
    }


    function disable_dates(start_date, termination_date, today) {
//        var date = '2014-05-29';
        $("input.clinician-input").each(function(index, value) {

            if ($(this).attr('date') < start_date) {
                $(this).attr('readonly', true);
            }
            if (termination_date !== '0000-00-00') {
                if ($(this).attr('date') >= termination_date) {
                    $(this).attr('readonly', true);
                } else {
//                alert('tamay leng');
                }
            }
            if ($(this).attr('date') > today) {
                $(this).attr('readonly', true);
            }
        });
        /*-- K --*/
        $('input.input-check').each(function() {
            if($(this).attr('date') > today) {
                $(this).attr('disabled', true);
            }
            if ($(this).val()) {
                $(this).attr('checked', true);
            }
        });
        /*- K -*/
    }


    }
<?php
if ($is_pta) {
    for ($ctr = 1; $ctr <= 7; $ctr++) {
        ?>
            $('input.clinician-input[name="newcase[<?php echo $week_date_attr[$ctr]; ?>]"]').each(function() {
                $(this).attr('readonly', true);
            });

        <?php
    }
}
?>
</script>


