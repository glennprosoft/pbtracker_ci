<div id="location_<?php echo $location_id ?>">
    <div class="form-group">
        <label class="col-sm-2 control-label">Location</label>
        <div class="col-sm-3">
        
            <input type="hidden" value="<?php echo $location_id ?>" name="location-id[]">
            <input type="text" class="form-control" readonly="read-only" value="<?php echo $location_name ?>" >
            <span class="error"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" >Date of First Use</label>
        <div class="col-sm-3">
            <input type="text" class="form-control first-date date_<?php echo $location_id?>"  value="<?php echo date('m/d/Y', strtotime($start_date)) ?>" name="date-first-use[]" >
            <span class="error"></span>
        </div>
    </div>
    <div class="form-group initial-cases">
        <label class="col-sm-2 control-label" >Initial Cases</label>
        <div class="col-sm-3">
            <input type="text" class="form-control case_<?php echo $location_id?>"  value="<?php echo $initial_cases ?>" name="initial-cases[]" id="initial-cases">
            <span class="error"></span>
        </div>
    </div>

    <div class="form-group">
        <label for="" class="col-sm-2 control-label">Employment Status</label>
        <div class="col-sm-3">
            <div class="radio-inline">
                <label>
                    <?php
                    $termid = 'term_' . $location_id;
                    if ($work_status == 1) {
                        $ishidden = 'hidden';
                        echo '<input class="term" type="radio" name="work-status['.$location_id.']" term=' . $termid . '  value="1" checked>';
                    } else {
                        $ishidden = '';
                        echo '<input class="term" type="radio" name="work-status['.$location_id.']" term=' . $termid . ' value="1">';
                    }
                    ?>
                    Active
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <?php
                    if ($work_status == 0) {
                        echo ' <input class="term" type="radio" name="work-status['.$location_id.']" id="report2" term=' . $termid . 'value="0" checked>';
                    } else {
                        echo ' <input class="term" type="radio" name="work-status['.$location_id.']" id="report2" term=' . $termid . ' value="0" >';
                    }
                    ?>

                    Terminated
                </label>
            </div>
        </div>
    </div>


    <div class="form-group" <?php echo $ishidden?> id="term_<?php echo $location_id ?>">
        <label class="col-sm-2 control-label" >Termination Date</label>
        <div class="col-sm-3">
            <input type="text" class="form-control first-date tdate_<?php echo $location_id ?>" value="<?php if($termination_date != '0000-00-00'){echo date('m/d/Y', strtotime($termination_date)); }?>" name="termination-date[]"  placeholder="">
            <span class="error"></span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-3">
            <button type="button" id="remove-location"  locid="<?php echo $location_id ?>"class="btn btn-default remove-loc">Remove</button>
        </div>
    </div>
    <hr>
</div>





