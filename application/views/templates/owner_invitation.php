<?php ?>
<html>
    <body style="text-align: center; font-family:Arial, Helvetica, sans-serif; color: gray;">
        <div class="wrapper" style="width: 500px; margin: 0 auto;">
            <div style="border: 2px solid #dedede; border-radius: 10px;">
                <div style="height: 30px; background-image: url(<?php echo $headerBgLink ?>); text-align: center; font-family: Arial, Helvetica, sans-serif; color: gray;">
                    <h3> Performance Tracker </h3>
                </div>
                <div id='email-body' style="text-align: left; margin: 20px; font-family:Arial, Helvetica, sans-serif; color: gray;">
                    <?php echo date('F d, Y'); ?>
                    <img src="<?php echo $pbLogoLink; ?>" style="position: relative; left: 5px; top: -15px; float: right;">
                    <br/>
                    <br/>
                    <br/>
                    <p>Dear <?php echo $firstName . " " . $lastName; ?>,
                        <br/>
                        <br/>
                        Welcome to the Performance Tracker.
                        <br/>
                        <br/>
                        Please click on the link below and follow the process to activate your account. 
                        After completing the process, you will now be able to login to the Performance Tracker.
                        <br/>
                        <br/>
                        <a href='<?php echo $siteurl; ?>owner_registration?hash=<?php echo $hash ?>'> Activation Link </a>
                        <br/>
                        <br/>
                        <br/>
                        Thank you,
                        <br/>
                    </p>
                </div>
                <div id="email-footer" style="text-align: left; margin: 20px; font-family:Arial, Helvetica, sans-serif; color: gray;">
                    <p>
                        Bob Wiersma
                        <br/> </p>
                    <p style="font-style: italic;">President, Performance Builders</p>
                   
                </div>
            </div>
        </div>
    </body>
</html>