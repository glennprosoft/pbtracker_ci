<div class="sidebar">
    <ul>
        <li><a class="border-li" href="<?php echo site_url() ?>profile">Profile</a></li>
        <li><a class="border-li" href="#" data-toggle="modal" data-target="#change-pass-modal">Password</a></li>
        <li><a href="<?php echo site_url() ?>logout/logout_redirect">Logout</a></li>
    </ul>
    <div class="sidebar-details">
        <strong><?php echo $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'); ?></strong>
        <div>
            <?php
            $roles = $this->session->userdata('roles');

            foreach ($roles as $role) {
                echo '<span style="font-size:12px">' . $role['role_name'] . "</span><br/>";
            }
            ?>

        </div>
        <div>
            <strong>Employee ID:</strong>
            <?php echo '<span style="font-size:12px">' . $this->session->userdata('employee_id') . '</span>'; ?>
        </div>
        <!--           <div>
                    <strong>Practice ID:</strong>
        <?php echo $this->session->userdata('practice_id'); ?>
                </div>-->

    </div>

    <?php
    if (current_url() == site_url() . "line_graph" || current_url() == site_url() . "bar_graph" || current_url() == site_url() . "admin_dashboard" || current_url() == site_url() . "minimum_dataset_dashboard"
    ) {
        ?>
        <form class="control-dates">
            <div class="form-group startdate">
                <label for="start-date">Start Date</label>
                <input type="text"  class="form-control datepicker glyphicon glyphicon-calendar calendar" name="start-date" id="start-date-sidebar"  value="<?php echo $this->session->userdata('startdate'); ?>">

            </div>
            <div class="form-group enddate">
                <label for="end-date">End Date</label>
                <input type="text"  class="form-control datepicker glyphicon glyphicon-calendar calendar" name="end-date" id="end-date-sidebar" value="<?php echo $this->session->userdata('enddate'); ?>">

            </div>
            <span class="error" id="date-sidebar-error" style="font-size:12px;")></span> 
        </form>
    <?php } else if (current_url() == site_url() . "clinician_dashboard" || current_url() == site_url() . "billers_dashboard") { ?>

        <div class="calendar-box">

        </div>

    <?php } else { ?>
        <div class="all-calendar-box">

        </div>
    <?php } ?>

    <?php
    if (current_url() == site_url() . "admin_dashboard" || current_url() == site_url() . "minimum_dataset_dashboard" || current_url() == site_url() . "clinician_dashboard" || current_url() == site_url() . "billers_dashboard" || current_url() == site_url() . "bar_graph" || current_url() == site_url() . "line_graph"):
        ?>
        <h2 class="sidebar-name">Locations</h2>

        <select name="location-list-sidebar" id='location-list-sidebar' class="form-control location-selector">
            <?php
            if (count($location_data) != 1) {
                if (current_url() == site_url() . "admin_dashboard" || current_url() == site_url() . "minimum_dataset_dashboard") {
                    echo '<option value = "0">All</option>';
                }
            }
            ?>
            <?php
            if ($location_data) {
                foreach ($location_data as $locdata) {

                    if ($locdata['location_id'] != 1) {
                        ?>
                        <option value="<?php echo $locdata['location_id']; ?>"><?php echo $locdata['location_name']; ?></option>
                        <?php
                    }
                }
            }
            ?>
        </select>
        <?php
        if (current_url() == site_url() . "line_graph" || current_url() == site_url() . "bar_graph" || current_url() == site_url() . "admin_dashboard" || current_url() == site_url() . "minimum_dataset_dashboard"
        ) {
            ?>
            <br/>        
            <button class="btn btn-default update-button pull-right" data-loading-text="Updating..." id="update-button-sidebar">Update</button>
            <br/>               
        <?php } ?>
    <?php endif; ?>

    <?php if (current_url() == site_url() . "line_graph"): ?>
        <div  id="legendlist" style="margin-top:15px">

        </div>
    <?php endif; ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".datepicker").datepicker({
            buttonImage: '<?php echo base_url(); ?>img/calendar.gif',
            buttonImageOnly: true, changeMonth: false,
            changeYear: false,
            showOn: 'both'
        });
    });

    $('.all-calendar-box').datepicker();


</script>

