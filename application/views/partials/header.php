<?php

function in_array_ar($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_ar($needle, $item, $strict))) {
            return true;
        }
    }
    return false;
}
?>

<style>
    a:hover{
        cursor: pointer;
    }
</style>

<div class="header">
    <div class="navbar navbar-fixed-top" style="height:40px;" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse" id="main-nav-collapse">
                <ul class="nav navbar-nav">

                    <?php if (in_array_ar('Super Administrator', $this->session->userdata('roles'))) { ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Admin <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url() ?>invite">Invite</a></li>
                                <li><a href="<?php echo site_url() ?>admin_create_user">Create User</a></li>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if (in_array_ar('Super Administrator', $this->session->userdata('roles')) || in_array_ar('Practice Owner', $this->session->userdata('roles')) || in_array_ar('Office Administrator', $this->session->userdata('roles'))) { ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Maintenance <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url(); ?>user_maintenance">Users Maintenance</a></li>
                                <li><a href="<?php echo site_url(); ?>location_maintenance">Location Maintenance</a></li>
                                <!--<li><a href="<?php echo site_url(); ?>role_maintenance">Role Maintenance</a></li>-->
                            </ul>
                        </li>
                    <?php } ?>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Dashboard <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php if (in_array_ar('Super Administrator', $this->session->userdata('roles')) || in_array_ar('Practice Owner', $this->session->userdata('roles')) || in_array_ar('Office Administrator', $this->session->userdata('roles'))) { ?>
                                <li><a href="<?php echo site_url(); ?>admin_dashboard">Admin Dashboard</a></li>
                                <li><a href="<?php echo site_url(); ?>minimum_dataset_dashboard">Minimum Data Set Dashboard</a></li>
                            <?php } ?>

                            <?php if (in_array_ar('Clinician', $this->session->userdata('roles'))) { ?>
                                <li >
                                    <a href="<?php echo site_url(); ?>clinician_dashboard">Clinician Dashboard</a>
<!--                                    <ul class="dropdown-menu">
                                        
                                        <li><a data-toggle="modal" data-target="#clinician-report-modal">Report</a></li>
                                    </ul>-->
                                </li>
                            <?php } ?>

                            <?php if (in_array_ar('Biller', $this->session->userdata('roles')) || in_array_ar('Super Administrator', $this->session->userdata('roles')) || in_array_ar('Practice Owner', $this->session->userdata('roles')) || in_array_ar('Office Administrator', $this->session->userdata('roles'))) { ?>
                                <li>
                                    <a href="<?php echo site_url(); ?>billers_dashboard">Billers Dashboard</a>
<!--                                    <ul class="dropdown-menu">
                                        <li><a data-toggle="modal" data-target="#billers-report-modal">Report</a></li>
                                    </ul>-->
                                </li>
                            <?php } ?>

                        </ul>
                    </li>

                    <?php if (in_array_ar('Super Administrator', $this->session->userdata('roles')) || in_array_ar('Practice Owner', $this->session->userdata('roles')) || in_array_ar('Office Administrator', $this->session->userdata('roles'))) { ?>
<!--                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Graphs <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php // echo site_url(); ?>bar_graph">Bar</a></li>
                                <li><a href="<?php // echo site_url(); ?>line_graph">Line</a></li>
                            </ul>
                        </li>-->


                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Help <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a href="#">Users Maintenance</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>/pdf/help_create_user.pdf" target="_blank">Creating a user</a></li>
    <!--                                    <li><a href="<?php echo base_url(); ?>/pdf/help_create_user.pdf" target="_blank">Editing a user</a></li>
                                        <li><a href="<?php echo base_url(); ?>/pdf/help_create_user.pdf" target="_blank">Terminating a user</a></li>-->
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a href="#">Location Maintenance</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>/pdf/help_create_location.pdf" target="_blank">Creating a location</a></li>
    <!--                                    <li><a href="<?php echo base_url(); ?>/pdf/help_create_user.pdf" target="_blank">Editing a location</a></li>
                                        <li><a href="<?php echo base_url(); ?>/pdf/help_create_user.pdf" target="_blank">Closing a location</a></li>-->
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> &nbsp;&nbsp;Account <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url(); ?>profile">Profile</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#change-pass-modal">Password</a></li>
                            <li><a href="<?php echo site_url(); ?>logout/logout_redirect" id="btn-logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </div>
</div>

<div class="modal fade" id="change-pass-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close xbutton" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>Change Password</strong></h4>    
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="password-form" role="form"> 
                    <p id="changepassprompt" hidden> Please change your password to improve your account's security. Thank you. 
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">Old Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" name="old-password" id="old-password" placeholder="Old Password">

                            <span class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">New Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" name="new-password" id="password" placeholder="New Password">
                            <label id="strength" style="font-size:12px"></label>

                            <span class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">Confirm Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" name="confirm-password" id="confirm-password" placeholder="Confirm Password">

                            <span class="error"></span>
                        </div>
                    </div>
                </form> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default xbutton" data-dismiss="modal">Cancel</button> 
                <button type="button" class="btn btn-primary" id="btn-savepass">Save</button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-sm" id="clinician-report-modal" tabindex="-1" role="dialog" aria-labelledby="clinician-report-modal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>Clinician Report</strong></h4>
            </div>
            <form class="clinician-report-form" target="_blank" action="<?php echo base_url(); ?>dashboard_reports/generate_clinician_report" method="POST">
                <div class="modal-body">

                    <div class="form-group startdate">
                        <label for="start-date">Start Date</label><br/>
                        <input type="text"  class="form-control datepicker glyphicon glyphicon-calendar calendar" id="start-date" name="start-date" value="<?php echo date('m/d/Y');?>">
                    </div>
                    <div class="form-group enddate">
                        <label for="end-date">End Date</label><br/>
                        <input type="text" name="end-date" class="form-control datepicker glyphicon glyphicon-calendar calendar" id="end-date" value="<?php echo date('m/d/Y');?>">
                    </div>
                    <div class="form-group">
                        <label for="location-list">Locations</label>
                        <select name="location-list-header" id='location-list-header' class="form-control location-selector">
                            <?php
                            if ($location_data) {
                                foreach ($location_data as $locdata) {
                                    ?>
                                    <option value="<?php echo $locdata['location_id']; ?>"><?php echo $locdata['location_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <!--enclosed with anchor tag for test purposes -->
                    <button type="submit" class="btn btn-default generate-clinician-btn">Generate Report (PDF)</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="billers-report-modal" tabindex="-1" role="dialog" aria-labelledby="billers-report-modal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Biller Report</h4>
            </div>
            <form class="biller-report-form" target="_blank" method="POST" action="<?php echo base_url() ?>dashboard_reports/generate_billers_report">
                <div class="modal-body">

                    <div class="form-group startdate">
                        <label for="start-date">Start Date</label><br/>
                        <input type="text" name="start-date-biller" class="form-control datepicker glyphicon glyphicon-calendar calendar" id="start-date-biller" value="<?php echo date('m/d/Y');?>">
                    </div>
                    <div class="form-group enddate">
                        <label for="end-date">End Date</label><br/>
                        <input type="text" name="end-date-biller" class="form-control datepicker glyphicon glyphicon-calendar calendar" id="end-date-biller" value="<?php echo date('m/d/Y');?>">
                    </div>
                    <div class="form-group">
                        <label for="location-list-biller-header">Locations</label>
                        <select id="location-list-biller-header" name="location-list-biller-header" class="form-control location-selector">

                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <!--enclosed with anchor tag for test purposes -->
                    <button type="submit" class="btn btn-default generate-billers-btn">Generate Report (PDF)</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>
    if (<?php echo $this->session->userdata('changepass') ?> == 1) {
        $('.xbutton').hide();
        $('#changepassprompt').show();
        $('#change-pass-modal').modal('show');
    }
    
    $.ajax({
        async: false,
        type: "POST",
        url: 'dashboard_reports/get_location_data',
        data: {report_type: 'biller'},
        dataType: 'json',
        success: function(data) {
            $.each(data.location_data, function(key, value) {
                $('#location-list-biller-header').append($("<option></option>").attr("value", value['location_id']).text(value['location_name']));
            });
        }
    });

   
//for the changepassword
    $('#btn-savepass').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'change_password/validate_fields',
            data: $('#password-form').serialize(),
            dataType: 'json',
            beforeSend: function() {
                $('#btn-savepass').text('Saving...');
            },
            success: function(data) {
                if (data.status === 'with-error') {
                    $('#btn-savepass').text('Submit');
                    if (data.oldpassword !== '') {
                        error('input[name=old-password]', data.oldpassword);
                    } else {
                        reset('input[name=old-password]');
                    }
                    if (data.newpassword !== '') {
                        error('input[name=new-password]', data.newpassword);
                    } else {
                        reset('input[name=new-password]');
                    }
                    if (data.confirmpassword !== '') {
                        error('input[name=confirm-password]', data.confirmpassword);
                    } else {
                        reset('input[name=confirm-password]');
                    }
                } else if (data.status === 'success') {
                    $('#btn-savepass').text('Submit');
                    $('#change-pass-modal').modal('hide');
                    $('#strength').html('');
                    $("input").val("");
                    refresh_all_fields();
                    
                    $('.xbutton').show();
                    $('#changepassprompt').hide();
   
                    
                    new PNotify({
                        title: 'Success',
                        text: 'Your password has been changed.',
                        type: 'success'
                    });
                }
            }

        });
    });

    function scorePassword(pass) {
        var score = 0;
        if (!pass)
            return score;

        // award every unique letter until 5 repetitions
        var letters = new Object();
        for (var i = 0; i < pass.length; i++) {
            letters[pass[i]] = (letters[pass[i]] || 0) + 1;
            score += 5.0 / letters[pass[i]];
        }

        // bonus points for mixing it up
        var variations = {
            digits: /\d/.test(pass),
            lower: /[a-z]/.test(pass),
            upper: /[A-Z]/.test(pass),
            nonWords: /\W/.test(pass),
        }

        variationCount = 0;
        for (var check in variations) {
            variationCount += (variations[check] == true) ? 1 : 0;
        }
        score += (variationCount - 1) * 10;

        return parseInt(score);
    }

    function checkPassStrength(pass) {
        var score = scorePassword(pass);
        if (score > 70)
            return 4;

        if (score > 40)
            return 3;

        if (score >= 20)
            return 2;

        if (score >= 10)
            return 1;

        return "";
    }


    $('#password').on('input', function() {
        var strength = checkPassStrength(document.getElementById('password').value);
        var color = "#FF0000";
        var str = "Weak";
        switch (strength) {
            case 1:
                var color = "#FF0000";
                var str = "Weak";
                break;
            case 2:
                var color = "#FF9900";
                var str = "Average";
                break;
            case 3:
                var color = "#999900";
                var str = "Good";
                break;
            case 4:
                var color = "#99FF66";
                var str = "Strong";
                break;
        }
        if (document.getElementById('password').value == "") {
            document.getElementById("strength").innerHTML = "";
        } else {
            document.getElementById("strength").innerHTML = "Password Strength: " + "<span style='font-weight:bold; color:" + color + ";'>" + str + "</span>";
        }
    });
    
     $(".datepicker").datepicker({
            buttonImage: '<?php echo base_url(); ?>img/calendar.gif',
            buttonImageOnly: true,
            changeMonth: false,
            changeYear: false,
            showOn: 'both'
        });

</script>