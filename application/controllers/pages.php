<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {
    public function index() {
        if ($this->in_array_r('Clinician', $this->session->userdata('roles'))) {
             redirect('/clinician_dashboard');
        } else if ($this->in_array_r('Biller', $this->session->userdata('roles'))) {
             redirect('/billers_dashboard');
        } if ($this->in_array_r('Office Administrator', $this->session->userdata('roles')) ||
                $this->in_array_r('Practice Owner', $this->session->userdata('roles')) ||
                $this->in_array_r('Super Administrator', $this->session->userdata('roles'))) {
             redirect('/admin_dashboard');
        }
    }

    public function get_default_page($user_id) {
        
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

}
