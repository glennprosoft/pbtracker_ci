<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Create_role extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('cliniciandatas');
        $this->load->model('userlocations');
        $this->load->model('users');
        $this->load->model('collectionrates');
        $this->load->model('roles');
    }

    public function check_if_authorized() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) ||
                $this->in_array_r("Practice Owner", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function index() {
        $this->check_if_authorized();

        $this->template->set_layout('default');
        $this->template->title('Create Role');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/role_maintenance.js") . '"></script>');
        $this->template->build('pages/create_role');
    }

    public function submit_role() {
        $role_item = $this->input->post('role-item');
        $role_name = $this->input->post('role-name');
        $role_type = $this->input->post('role-type');

        $rules = array(
            array(
                'field' => 'role-name',
                'label' => 'Role Name',
                'rules' => 'trim|required'
            )
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $data = array(
                'error' => '1',
                'role_name' => form_error('role-name')
            );
            echo json_encode($data);
        } else {
            $data = array(
                'error' => '0'
            );
            echo json_encode($data);
            
            $res;
            
            
        }
    }

}
