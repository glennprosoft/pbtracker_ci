<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Create_user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('roles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('positions');
        $this->load->model('mailer');
    }

    public function check_if_authorized() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }

        $roles = $this->session->userdata('roles');
        if (
//                   $this->in_array_r("Super Administrator", $roles) 
                $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        $this->check_if_authorized();

        $locIds = array();
        $this->session->set_userdata('locIds', $locIds);

        $pos = $this->positions->get_positions_by_practice($this->session->userdata('practice_id'));
        $roles = $this->roles->get_all_roles();
        $locOptions = $this->set_user_locs();

        $data = array(
            'positions' => $pos,
            'roles' => $roles,
            'locOptions' => $locOptions
        );

        $this->load->vars($data);
        $this->template->set_layout('default');
        $this->template->title('Create User');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/user-maintenance.js") . '"></script>');
        $this->template->build('pages/create_user');
    }

    public function validate_loc() {
        $rules = array(
            array(
                'field' => 'date-first-use',
                'label' => 'Date of First Use',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'initial-cases',
                'label' => 'Initial Cases',
                'rules' => 'trim|required'
            ),
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error',
                'date' => form_error('date-first-use'),
                'initial' => form_error('initial-cases'),
            );
            echo json_encode($data);
        } else {

            return true;
        }
    }

    public function add_location() {
        if ($this->validate_loc()) {

            $locIds = $this->session->userdata('locIds');
            array_push($locIds, $this->input->post('location-id'));
            $this->session->set_userdata('locIds', $locIds);


            $location = array(
                'locId' => $this->input->post('location-id'),
                'locName' => $this->input->post('location-name'),
                'startDate' => $this->input->post('date-first-use'),
                'initialCases' => $this->input->post('initial-cases')
            );

            $markup = $this->set_up_markup($location);
            $locOptions = $this->set_user_locs();

            $addbutton = $this->count_locs();

            echo json_encode(
                    array(
                        'status' => "success",
                        'id' => $this->input->post('location-id'),
                        'markup' => $markup,
                        'addbutton' => $addbutton,
                        'locOptions' => $locOptions
                    )
            );
        }
    }

    public function remove_location() {
        $locId = $this->input->post('location-id');
        $locIds = $this->session->userdata('locIds');

        if (($key = array_search($locId, $locIds)) !== false) {
            unset($locIds[$key]);
        }

        $this->session->set_userdata('locIds', $locIds);


        $locOptions = $this->set_user_locs();
        $addbutton = $this->count_locs();
        echo json_encode(
                array(
                    'status' => "success",
                    'locOptions' => $locOptions,
                    'addbutton' => $addbutton,
                )
        );
    }

    public function set_user_locs() {
        if ($this->in_array_r('Practice Owner', $this->session->userdata('roles'))) {
            $locations = $this->userlocations->get_all_location($this->session->userdata('practice_id'));
            $locIds = $this->session->userdata('locIds');

            $options = '';
            foreach ($locations as $loc) {
                if (!in_array($loc['id'], $locIds)) {
                    $options .= '<option value="' . $loc['id'] . '">' . $loc['location_name'] . '</option>';
                }
            }
        } else if ($this->in_array_r('Office Administrator', $this->session->userdata('roles'))) {
            $locations = $this->userlocations->get_admin_locs($this->session->userdata('user_id'));
            $locIds = $this->session->userdata('locIds');

            $options = '';
            foreach ($locations as $loc) {
                if (!in_array($loc['id'], $locIds)) {
                    $options .= '<option value="' . $loc['location_id'] . '">' . $loc['location_name'] . '</option>';
                }
            }
        }
        return $options;
    }

    public function set_up_markup($location) {
        $markup = $this->load->view('templates/user_location', $location, true);
        return $markup;
    }

    public function count_locs() {
        if ($this->in_array_r('Practice Owner', $this->session->userdata('roles'))) {
            $plocs = $this->userlocations->get_all_location($this->session->userdata('practice_id'));
            $slocs = $this->session->userdata('locIds');
        } else if ($this->in_array_r('Office Administrator', $this->session->userdata('roles'))) {
            $plocs = $this->userlocations->get_admin_locs($this->session->userdata('user_id'));
            $slocs = $this->session->userdata('locIds');
        }

        if (count($plocs) == count($slocs)) {
            return 1;
        }

        return 0;
    }

    public function validate_entry() {

        $rules = array(
            array(
                'field' => 'emp-id',
                'label' => 'Employee ID',
                'rules' => 'trim|required|is_unique[tblusers.employee_id]'
            ),
            array(
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ), array(
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ), array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|is_unique[tblusers.email]'
            ),
            array(
                'field' => 'roles',
                'label' => 'Role',
                'rules' => 'callback_role_check'
            ),
        );
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has already been used.');

        $noloc = '';
        $valDate = '';
        $valCase = '';
        $error = true;
        if ($this->input->post('roles')) {
            if (!$this->input->post('location-id')) {
                $noloc = 'noloc';
                $error = false;
            } else {
                $vDate = $this->check_dates();
                if ($vDate) {
                    $valDate = $vDate;
                    $error = false;
                }

                $vCase = $this->check_cases();
                if ($vCase) {
                    $valCase = $vCase;
                    $error = false;
                }
            }
        }
        if ($this->form_validation->run() == false || $error == false) {
            $data = array(
                'status' => 'error',
                'empid' => form_error('emp-id'),
                'fname' => form_error('first-name'),
                'lname' => form_error('last-name'),
                'email' => form_error('email'),
                'role' => form_error('roles'),
                'noloc' => $noloc,
                'valdate' => $valDate,
                'valcase' => $valCase,
            );
            echo json_encode($data);
        } else {
            $this->send_invite();
        }
    }

    public function check_dates() {
        $dates = $this->input->post('date-first-use');
        $locations = $this->input->post('location-id');
        $index = 0;
        $val = array();
        foreach ($locations as $loc) {
            if ($dates[$index] == '') {
                $val[] = 'date_' . $loc;
            }

            $index++;
        }

        return $val;
    }

    public function check_cases() {
        $cases = $this->input->post('initial-cases');
        $locations = $this->input->post('location-id');
        $roles = $this->input->post('roles');
        $index = 0;
        $val = array();
        foreach ($locations as $loc) {
            if ($this->in_array_r('Clinician', $roles)) {
                if ($cases[$index] == '') {
                    $val[] = 'case_' . $loc;
                }
            }

            $index++;
        }

        return $val;
    }

    public function role_check() {
        if ($this->input->post('roles')) {
            return true;
        } else {
            $this->form_validation->set_message('role_check', 'Please select at least 1 %s.');
            return false;
        }
    }

    public function send_invite() {
        $status = 'senderror';
        $hash = $this->create_validationcode($this->input->post('email'));
        $practicename = $this->practices->get_practice_by_id($this->session->userdata('practice_id'));
        $owner = $this->users->get_user_by_id($practicename[0]['user_id']);
        $data = array(
            'pbLogoLink' => base_url() . 'img/pb-logo-2.jpg',
            'headerBgLink' => base_url() . 'img/nav-bg.png',
            'firstName' => $this->input->post('first-name'),
            'lastName' => $this->input->post('last-name'),
            'hash' => $hash,
            'pName' => $practicename[0]['practice_name'],
            'siteurl' => site_url(),
            'fromname' => $owner[0]['first_name'] . ' ' . $owner[0]['last_name']
        );

        $mailBody = $this->load->view('templates/user_invitation', $data, true);
        $send = $this->mailer->send_mail(//send mail function in mailer.php
                $this->input->post('email'), //destination email
                'Performance Tracker Invitation', //subject
                $mailBody//email body
        );

        if ($send) {
            //CC to prosoft-phils
            $this->mailer->send_mail(//send mail function in mailer.php
                    'support@prosoft-phils.com', //destination email
                    'Performance Tracker - Invitation', //subject
                    $mailBody//email body
            );

            /*
              //CC to test
              $this->mailer->send_mail(//send mail function in mailer.php
              ' prosoft.ph@gmail.com', //destination email
              'Performance Tracker - Invitation', //subject
              $mailBody//email body
              );
             */

            $status = 'success';
            $this->save_user($hash); //save user
            $userId = mysql_insert_id(); //get last inserted user 
            $this->save_user_prac($userId);
            $this->save_user_role($userId);
            $this->save_user_pos($userId);
            $this->save_user_loc($userId);
        }

        echo json_encode(
                array(
                    "status" => $status
                )
        );
    }

    function create_validationcode($salt) {
        // Read the user agent, IP address, current time, and a random number:
        $data = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] .
                time() . rand() . $salt;
        // Return this value hashed via sha1
        return sha1($data);
    }

    public function save_user($hash) {

        $data = array(
            'employee_id' => "'" . $this->input->post('emp-id') . "'",
            'first_name' => "'" . $this->input->post('first-name') . "'",
            'last_name' => "'" . $this->input->post('last-name') . "'",
            'email' => "'" . $this->input->post('email') . "'",
            'validation_code' => "'" . $hash . "'",
        );

        $this->users->save_userdata($data);
    }

    public function save_user_loc($userId) {
        $locIds = $this->input->post('location-id');
        $dateFirstUse = $this->input->post('date-first-use');
        $initialCases = $this->input->post('initial-cases');
        $index = 0;
        foreach ($locIds as $loc) {
            if (in_array('4', $this->input->post('roles'))) {
                $startdate = date('Y-m-d', strtotime($dateFirstUse[$index]));
                $data1 = array(
                    'user_id' => $userId,
                    'location_id' => $locIds[$index],
                    'work_status' => 1, //1 = active, 0 = terminated
                    'initial_cases' => $initialCases[$index],
                    'start_date' => "'" . $startdate . "'",
                    'termination_date' => '"0000-00-00"'
                );
            } else {
                $startdate = date('Y-m-d', strtotime($dateFirstUse[$index]));
                $data1 = array(
                    'user_id' => $userId,
                    'location_id' => $locIds[$index],
                    'work_status' => 1, //1 = active, 0 = terminated
                    'initial_cases' => 0,
                    'start_date' => "'" . $startdate . "'",
                    'termination_date' => '"0000-00-00"'
                );
            }
            $this->userlocations->save_userlocation($data1);

            $index ++;
        }
    }

    public function save_user_pos($userId) {
        $pos = $this->input->post('position');
        $data = array(
            'user_id' => $userId,
            'position_id' => $pos
        );

        $this->userpositions->save_userposition($data);
    }

    public function save_user_role($userId) {
        $roles = $this->input->post('roles');
        $index = 0;

        foreach ($roles as $role) {
            $data = array(
                'user_id' => $userId,
                'role_id' => $roles[$index]
            );

            $this->userroles->save_userrole($data);

            $index++;
        }
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    function save_user_prac($userId) {
        $data = array(
            'user_id' => $userId,
            'practice_id' => $this->session->userdata('practice_id')
        );

        $this->userpractices->save_userpractice($data);
    }

    public function create_pos() {
        $rules = array(
            array(
                'field' => 'position-code',
                'label' => 'Position Code',
                'rules' => 'trim|required|is_unique[tblposition.position_code]'
            ),
            array(
                'field' => 'position-name',
                'label' => 'Position Name',
                'rules' => 'trim|required|is_unique[tblposition.position_name]'
            )
        );

        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has already been used.');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error',
                'pcode' => form_error('position-code'),
                'pname' => form_error('position-name'),
            );
            echo json_encode($data);
        } else {

            $this->save_pos();
            $markup = $this->pos_markup();
            $data = array(
                'status' => 'success',
                'posmarkup' => $markup,
            );

            echo json_encode($data);
        }
    }

    public function save_pos() {
        $data = array(
            'position_code' => "'" . strtoupper($this->input->post('position-code')) . "'",
            'position_name' => "'" . $this->input->post('position-name') . "'",
            'practice_id' => $this->session->userdata('practice_id'),
            'is_clinician' => 0,
        );

        $this->positions->save_pos($data);
    }

    public function pos_markup() {
        $pos = $this->positions->get_positions_by_practice($this->session->userdata('practice_id'));
        $markup = '';
        foreach ($pos as $p) {
            $markup .= '<option value="' . $p['id'] . '">' . $p['position_name'] . ' (' . $p['position_code'] . ')</option>';
        }
        return $markup;
    }

}
