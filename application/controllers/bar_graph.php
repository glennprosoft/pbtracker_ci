<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bar_graph extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('cliniciandatas');
        $this->load->model('collectionrates');
    }

    public function check_if_authorized() {

        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $location_id = $this->input->post('location_id');


        $this->check_if_authorized();

        $data['viewMenu'] = array(
            array('item' => 'New Cases', 'desc' => 'Total number of new evaulations'),
            array('item' => 'Active Discharges', 'desc' => 'Patients discharged by therapist'),
            array('item' => 'Passive Discharges', 'desc' => 'Self Discharges'),
            array('item' => 'Active Cases', 'desc' => 'Total cases not yet discharged'),
            array('item' => 'Visits Attended', 'desc' => 'Appointments that are kept'),
            array('item' => 'Visits Cancels & No-Shows', 'desc' => 'Appointments not kept and not rescheduled with at least 24 hours notice'),
            array('item' => 'Visits Scheduled', 'desc' => 'Visits Scheduled = Visits Attended + Visits Cancels & No-Shows '),
            array('item' => 'Units Charged', 'desc' => ''),
            array('item' => 'Days Worked', 'desc' => ''),
            array('item' => 'Hours Worked', 'desc' => 'Total number of hours worked including patient care meetings, documentation, and case management'),
            array('item' => 'Clinical Hours Scheduled', 'desc' => 'Total aggregate hours scheduled (e.g 2 patients scheduled for one hour each = 2 hours scheduled, even if patients are scheduled at the same time) ')
        );


        $data['billingMenu'] = array(
            array('item' => 'Charges', 'desc' => 'Total $ amount charges for services'),
            array('item' => 'Estimated Collections', 'desc' => 'Estimated Collections = Charge * Collection Rate'),
            array('item' => 'Billing Days', 'desc' => 'Number of dates for which services were delivered'),
        );


        $data['unitRatiosMenu'] = array(
            array('item' => 'Units / Visits', 'desc' => 'Units/Visit = Units Charged/Visits Attended'),
            array('item' => 'Units / Cases', 'desc' => 'Units/Active Case = Units Charged/Active Cases'),
            array('item' => 'Units / Hours Worked', 'desc' => 'Units/Hour Worked = Units Charged/Hours Worked'),
            array('item' => 'Units / 8 Hours Worked', 'desc' => 'Units/8 Hours Worked = Units Charged/(Hours Worked/8)'),
            array('item' => 'Charge / Units', 'desc' => 'Charge/Unit = Charges/Units charged'),
            array('item' => 'Collected / Units', 'desc' => 'Collected/Unit = Estimated Collections/Units Charged')
        );

        $data['visitRatiosMenu'] = array(
            array('item' => 'Visits / Cases', 'desc' => 'Visits/Case = Total Visits/Active Cases'),
            array('item' => 'Visits / Hours Worked', 'desc' => 'Visits/Hour Worked = Total Visits/Hours Worked'),
            array('item' => 'Visits / 8 Hours Worked', 'desc' => 'Visits/8 Hours Worked = Total Visits(Hours Worked/8)'),
            array('item' => 'Charges / Visits', 'desc' => 'Charge/Visit Attended = Charges/Total Visits'),
            array('item' => 'Collected / Visits', 'desc' => 'Collected Visit = Estimated Collections/Total Visits')
        );

        $data['managementMenu'] = array(
            array('item' => 'Cases / Days', 'desc' => 'Active Cases/Day = Active Cases/Days Worked'),
            array('item' => 'Passive Discharge Rate', 'desc' => 'Passive Discharge Rate = Passive Discharges/(Active Discharges + Passive Discharge)'),
            array('item' => 'Attendance Rate', 'desc' => 'Attendance Rate = Visits Attended/Visits Scheduled')
        );

        $data['financialRatiosMenu'] = array(
            array('item' => 'Charges / Hours Worked', 'desc' => 'Charges/Hour Worked = Charges/Hours Worked'),
            array('item' => 'Collected / Hours Worked', 'desc' => 'Collected/Hour Worked = Estimated Collections/Hours Worked')
        );

        $data['staffingRatiosMenu'] = array(
            array('item' => 'Units / Billing Days', 'desc' => 'Units/Billing Day = Units Charged/Billing Days'),
            array('item' => 'Visits / Billing Days', 'desc' => 'Visits/Billing Day = Visits Attended/Billing Days'),
            array('item' => 'Total Hours Worked / Days Worked', 'desc' => 'Total Hours Worked/Days Worked = Hours Worked/Days Worked'),
            array('item' => 'Schedule Density', 'desc' => 'Scheduled Density = Clinical Hours Scheduled/Hours Worked')
        );

        $roles = $this->session->userdata('roles');
        $practiceId = $this->session->userdata('practice_id');
//check roles
        if ($this->in_array_r('Super Administrator', $roles)) { //if Super ADmin
//get all locations
            $locations = $this->userlocations->get_all();
        } elseif ($this->in_array_r('Practice Owner', $roles)) {//if Practice Owner
//get practice locations
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
//get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        $data1 = array(
            'location_data' => $locations,
        );
        if (isset($start_date)) {
            $sessArr = array(
                'graphrequesteddata' => array(
                    'sdate' => $start_date,
                    'edate' => $end_date,
                    'locationid' => $location_id,
                )
            );
            $data1['graphdata'] = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'location_id' => $location_id,
            );
        } else {
            $sessArr = array(
                'graphrequesteddata' => array(
                    'sdate' => '',
                    'edate' => '',
                    'locationid' => '',
                )
            );
        }
        $this->session->set_userdata($sessArr);


        $this->load->vars($data1);
        $this->template->set_layout('default');
        $this->template->title('Bar Graph');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/bar_graph.js") . '"></script>');
        $this->load->view('pages/bar_graph', $data);
        $this->template->build('pages/bar_graph');
    }

    public function get_data() {
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date')));
        $locationId = $this->input->post('location');


        $this->session->set_userdata('startdate', date('m/d/Y', strtotime($sDate)));
        $this->session->set_userdata('enddate', date('m/d/Y', strtotime($eDate)));

        $arr = array(
            'sdate' => $sDate,
            'edate' => $eDate,
            'locationid' => $locationId,
        );

        $this->session->set_userdata('graphrequesteddata', $arr);

        $usersData = $this->get_bargraph_data($sDate, $eDate, $locationId);
        $names = array();
        $value = array();

//        format for names
        foreach ($usersData as $data) {
            if ($this->input->post('show') == 1) {
                $names[] = '<strong>' . strtoupper($data['lname']) . '</strong>,<br/>' . $data['fname'] . ' (' . $data['empid'] . ')';
            } else if ($this->input->post('show') == 2) {
                $names[] = '<strong>' . strtoupper($data['lname']) . '</strong>,<br/>' . $data['fname'];
            } else
            if ($this->input->post('show') == 3) {
                $names[] = '<strong>' . $data['empid'] . '</strong>';
            }
        }

        foreach ($usersData as $data) {
            $value[] = $data['newcases'];
        }

        $strClinicians = $this->set_clinician_list($usersData);
        $data = array(
            'clinicians' => $strClinicians,
            'names' => $names,
            'value' => $value,
        );

        echo json_encode($data);
    }

    public function set_clinician_list($usersData) {
        $userid = array();
        $name = array();
        $strClinicians = '';
        $ind = 0;
        foreach ($usersData as $data) {
            $userid[] = $data['uid'];
            $name[] = '<strong>' . strtoupper($data['lname']) . '</strong>, ' . $data['fname'] . ' (' . $data['empid'] . ')';
        }
        foreach ($userid as $id) {
            $strClinicians .= '<label class="checkbox">';
            $strClinicians .= '<input type="checkbox" value="' . $userid[$ind] . '" name="userid[]" checked="checked">';
            $strClinicians .= $name[$ind];
            $strClinicians .= '</label>';
            $ind++;
        }

        return $strClinicians;
    }

    public function switch_graph() {
        $postarr = $this->session->userdata('graphrequesteddata');

        $sDate = $postarr['sdate'];
        $eDate = $postarr['edate'];
        $locationId = $postarr['locationid'];

        $usersData = $this->get_bargraph_data($sDate, $eDate, $locationId);
        $userids = array();
        $selclin = array();
        $nothingselected = false;
        foreach ($usersData as $id) {
            $userids[] = $id['uid'];
        }

        if ($this->input->post('userid')) {
            $selclin = $this->input->post('userid');
        } else {
            $nothingselected = true;
        }


        $indx = 0;
        foreach ($userids as $uid) {
            if (!in_array($uid, $selclin) || $nothingselected) {
                unset($usersData[$indx]);
            }
            $indx++;
        }


        $names = array();
        $value = array();
        foreach ($usersData as $data) {
            if ($this->input->post('show') == 1) {
                $names[] = '<strong>' . strtoupper($data['lname']) . '</strong>,<br/>' . $data['fname'] . ' (' . $data['empid'] . ')';
            } else if ($this->input->post('show') == 2) {
                $names[] = '<strong>' . strtoupper($data['lname']) . '</strong>,<br/>' . $data['fname'];
            } else
            if ($this->input->post('show') == 3) {
                $names[] = '<strong>' . $data['empid'] . '</strong>';
            }
        }

        switch ($this->input->post('selected')) {

//VOLUME------------------------------------------------------------
            case 'New Cases':
                foreach ($usersData as $data) {

                    $value[] = (float) $data['newcases'];
                }
                break;
            case 'Active Discharges':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['adis'];
                }
                break;
            case 'Passive Discharges':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['pdis'];
                }
                break;
            case 'Active Cases':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['activecases'];
                }
                break;
            case 'Visits Attended':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['vattended'];
                }
                break;
            case 'Visits Cancels ':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['vcanceled'];
                }
                break;
            case 'Visits Scheduled':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['vscheduled'];
                }
                break;
            case 'Units Charged':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['ucharged'];
                }
                break;
            case 'Days Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['dworked'];
                }
                break;
            case 'Hours Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['hworked'];
                }
                break;

            case 'Clinical Hours Scheduled':
                foreach ($usersData as $data) {
                    $value[] = (float) $data['hscheduled'];
                }
                break;


//BILLING-----------------------------------------------------------
            case 'Charges':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $data['charges']);
                }

                break;
            case 'Estimated Collections':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $data['collected']);
                }
                break;
            case 'Billing Days':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $data['bdays']);
                }
                break;


//UNIT RATIOS-------------------------------------------------------
            case 'Units / Visits':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['ucharged'], $data['vattended']));
                }
                break;
            case 'Units / Cases':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['ucharged'], $data['activecases']));
                }
                break;
            case 'Units / Hours Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['ucharged'], $data['hworked']));
                }
                break;
            case 'Units / 8 Hours Worked':
                foreach ($usersData as $data) {
                    $hworked = $this->divide($data['hworked'], 8);
                    $value[] = (float) str_replace(',', '', $this->divide($data['ucharged'], $hworked));
                }
                break;
            case 'Charge / Units':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['charges'], $data['ucharged']));
                }
                break;
            case 'Collected / Units':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['collected'], $data['ucharged']));
                }
                break;

//VISITS RATIOS-----------------------------------------------------
            case 'Visits / Cases':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['vattended'], $data['activecases']));
                }
                break;
            case 'Visits / Hours Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['vattended'], $data['hworked']));
                }
                break;
            case 'Visits / 8 Hours Worked':
                foreach ($usersData as $data) {
                    $hworked = $this->divide($data['hworked'], 8);
                    $value[] = (float) str_replace(',', '', $this->divide($data['vattended'], $hworked));
                }
                break;
            case 'Charges / Visits':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['charges'], $data['vattended']));
                }
                break;
            case 'Collected / Visits':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['collected'], $data['vattended']));
                }
                break;

//CASE MANAGEMENT RATIOS--------------------------------------------
            case 'Cases / Days':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['activecases'], $data['dworked']));
                }
                break;
            case 'Passive Discharge Rate':
                foreach ($usersData as $data) {
                    $dividened = $data['adis'] + $data['pdis'];
                    $value[] = (float) str_replace(',', '', $this->divide2($data['pdis'], $dividened));
                }
                break;
            case 'Attendance Rate':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide2($data['vattended'], $data['vscheduled']));
                }
                break;


//FINANCIAL RATIOS--------------------------------------------------
            case 'Charges / Hours Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['charges'], $data['hworked']));
                }
                break;
            case 'Collected / Hours Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['collected'], $data['hworked']));
                }
                break;


//STAFFING RATIOS---------------------------------------------------
            case 'Units / Billing Days':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['ucharged'], $data['bdays']));
                }
                break;
            case 'Visits / Billing Days':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['vattended'], $data['bdays']));
                }
                break;
            case 'Total Hours Worked / Days Worked':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['hworked'], $data['dworked']));
                }
                break;
            case 'Schedule Density':
                foreach ($usersData as $data) {
                    $value[] = (float) str_replace(',', '', $this->divide($data['hscheduled'], $data['hworked']));
                }
                break;
        }

        $data = array(
            'names' => $names,
            'value' => $value,
        );



//JSON_NUMERIC_CHECK - number integer strings will be treated as real integer
        echo json_encode($data);
    }

    public function get_bargraph_data($sDate, $eDate, $locationId) {
        $usersData = array();
        if ($sDate == '') {
            return $usersData;
        }
        $userIds = $this->userlocations->get_users_underloc($locationId);



        foreach ($userIds as $id) {
            $tnewCases = 0;
            $taDis = 0;
            $tpDis = 0;
            $addCases = 0;
            $remCases = 0;
            $activeCase = 0;
            $vAttended = 0;
            $vScheduled = 0;
            $vCanceled = 0;
            $uCharged = 0;
            $dWorked = 0;
            $hWorked = 0;
            $hScheduled = 0;
            $collected = 0;
            $charges = 0;
            $bdays = 0;
            $emp_id = '';
            $fname = '';
            $lname = '';
            $include = false;
            $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $locationId, $sDate, $eDate);

            foreach ($userData as $data) {
                $collectionrate = $this->collectionrates->get_collection_rate_by_date($locationId, $data['end_date']);

                if ($data) {
                    $emp_id = $data['employee_id'];
                    $fname = $data['first_name'];
                    $lname = $data['last_name'];
                    $tnewCases += $data['new_cases'];
                    $taDis += $data['active_discharges'];
                    $tpDis += $data['passive_discharges'];
                    $addCases += $data['added_cases'];
                    $remCases += $data['removed_cases'];
                    $activeCases = $data['active_cases'];
                    $vAttended += $data['visits_attended'];
                    $vScheduled += $data['visits_scheduled'];
                    $vCanceled += $data['no_show'];
                    $uCharged += $data['units_charged'];
                    $dWorked +=$data['days_worked'];
                    $hWorked += $data['hours_worked'];
                    $hScheduled +=$data['hours_scheduled'];
                    $collected += $data['charges'] * ( $collectionrate / 100);
                    $charges +=$data['charges'];
                    $bdays +=$data['billing_days'];
                    $include = true;
                }
            }
            if ($include) {
                $usersData[] = array(
                    'uid' => $id['user_id'],
                    'empid' => $emp_id,
                    'fname' => $fname,
                    'lname' => $lname,
                    'newcases' => $tnewCases,
                    'adis' => $taDis,
                    'pdis' => $tpDis,
                    'addcases' => $addCases,
                    'remcases' => $remCases,
                    'activecases' => $activeCases,
                    'vattended' => $vAttended,
                    'vscheduled' => $vScheduled,
                    'vcanceled' => $vCanceled,
                    'ucharged' => $uCharged,
                    'dworked' => $dWorked,
                    'hworked' => $hWorked,
                    'hscheduled' => $hScheduled,
                    'collected' => $collected,
                    'charges' => $charges,
                    'bdays' => $bdays
                );
            }
        }

        return $usersData;
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    function divide($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format($dividend / $divisor, 2);
            return $quotient;
        }
    }

    function divide2($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor) * 100, 2);
            return $quotient;
        }
    }

}
