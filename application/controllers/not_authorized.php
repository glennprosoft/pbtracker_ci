<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Not_authorized extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $pages = $this->get_available_pages();

        $this->load->vars('pages', $pages);
        $this->template->set_layout('default');
        $this->template->title('Not Authorized');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->build('pages/not_authorized');
    }

    public function get_available_pages() {
        $roles = $this->session->userdata('roles');
        $pages = array();
        $url = site_url();
        if ($this->in_array_r('Practice Owner', $roles) || $this->in_array_r('Office Administrator', $roles)) {
            $pages[] = '<a href="' . $url . 'user_maintenance">User Maintenance</a>';
            $pages[] = '<a href="' . $url . 'location_maintenance">Location Maintenance</a>';
            $pages[] = '<a href="' . $url . 'admin_dashboard">Admin Dashboard</a>';
            $pages[] = '<a href="' . $url . 'minimum_dataset_dashboard">Minimum Data Set Dashboard</a>';

            if (!$this->in_array_r('Biller', $roles)) {
                $pages[] = '<a href="' . $url . 'billers_dashboard">Billers Dashboard</a>';
            }

            $pages[] = '<a href="' . $url . 'bar_graph">Bar Graph</a>';
            $pages[] = '<a href="' . $url . 'line_graph">Line Graph</a>';
        }

        if ($this->in_array_r('Clinician', $roles)) {
            $pages[] = '<a href="' . $url . 'clinician_dashboard">Clinician Dashboard</a>';
        }

        if ($this->in_array_r('Biller', $roles)) {
            $pages[] = '<a href="' . $url . 'billers_dashboard">Billers Dashboard</a>';
        }

        $pages[] = '<a href="' . $url . 'profile">Profile</a>';

        return $pages;
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

}
