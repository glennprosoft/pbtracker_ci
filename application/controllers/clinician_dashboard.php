<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clinician_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('positions');
        $this->load->model('mailer');
        $this->load->model('cliniciandata');
        $this->load->model('cliniciandatas');
        $this->load->model('collectionrates');
        $this->load->model('clinicianbaseline');
    }

    public function check_if_authorized() {

        $roles = $this->session->userdata('roles');
        if (
//                   $this->in_array_r("Super Administrator", $roles) 
//                || $this->in_array_r("Practice Owner", $roles) 
//                || $this->in_array_r("Office Administrator", $roles) 
//                || $this->in_array_r("Biller", $roles)
                $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }

        $this->check_if_authorized();

        $roles = $this->session->userdata("roles");
        $user_id = $this->session->userdata('user_id');
        $current_date = date('Y-m-d');
        $location_data = $this->userlocations->get_locdata($user_id);
        $display_data = $this->display_data_dashboard($current_date, false);
        $week_date_attr = $display_data['week_date_attr'];
        $data = array(
            'week_date_attr' => $week_date_attr,
            'location_data' => $location_data,
            'selected_date' => $current_date
        );

        $this->load->vars($data);
        $this->template->set_layout('default');
        $this->template->title('Clinician Dashboard');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/clinician_dashboard.js") . '"></script>');
        $this->template->build('pages/clinician_dashboard');
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function not_authorized() {
        $this->load->view("error");
    }

    public function set_display_dashboard() {
        $selected_date = $this->input->post('date_select');
        $this->display_data_dashboard($selected_date, true);
    }

    public function range_week($datestr) {
        $dt = strtotime($datestr);
        $res['start'] = date('N', $dt) == 1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
        $res['end'] = date('N', $dt) == 7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
        return $res;
    }

    public function get_week_dates($date) {
        $week_dates = array();
        $ts = strtotime($date);
        $week_number = date('W', $ts);
        $year = date('Y', $ts);
        ;
        for ($day = 1; $day <= 7; $day++) {
            $week_dates[$day] = date('m/d/Y', strtotime($year . "W" . $week_number . $day));
        }
        return $week_dates;
    }

    public function display_data_dashboard($selected_date, $is_json) {
        $current_date = date('Y-m-d');
        $week_dates_header = $this->convert_date_format('F d', $this->get_week_dates($current_date));
        $week_date_attr = $this->convert_date_format('Y-m-d', $this->get_week_dates($current_date));
        if ($selected_date) {
            $date_range = $selected_date;
            $week_dates_header = $this->convert_date_format('F d', $this->get_week_dates($selected_date));
            $week_date_attr = $this->convert_date_format('Y-m-d', $this->get_week_dates($selected_date));
        } else {
            $date_range = $current_date;
        }
        $range = $this->range_week($date_range);
        $range['start'] = date('F d, Y', strtotime($range['start']));
        $range['end'] = date('F d, Y', strtotime($range['end']));


        $data = array(
            'current_date' => $current_date,
            'selected_date' => $selected_date,
            'range' => $range,
            'week_dates_header' => $week_dates_header,
            'week_date_attr' => $week_date_attr
        );
        if ($is_json) {
            echo json_encode($data);
        } else {
            return $data;
        }
    }

    public function load_clinician_table() {


        $position = $this->session->userdata('position');

        if ($this->input->post('user_id')) {
            $user_id = $this->input->post('user_id');
        } else {
            $user_id = $this->session->userdata('user_id');
        }
        $clinician_data = array();
        $clinician_computation = array();
        $clinician_total = array();
        $clinician_total['collected'] = 0;
        $clinician_total['charges'] = 0;
        $clinician_total['bdays'] = 0;
        $clinician_total['units_visits'] = 0;
        $clinician_total['unitsactive'] = 0;
        $clinician_total['unitshour'] = 0;
        $clinician_total['units8hour'] = 0;
        $clinician_total['chargeunit'] = 0;
        $clinician_total['collectedunit'] = 0;
        $clinician_total['visitscase'] = 0;
        $clinician_total['visitshour'] = 0;
        $clinician_total['visits8hour'] = 0;
        $clinician_total['chargevisit'] = 0;
        $clinician_total['collectedvisit'] = 0;
        $clinician_total['casesday'] = 0;
        $clinician_total['passiverate'] = 0;
        $clinician_total['attendrate'] = 0;
        $clinician_total['chargeshour'] = 0;
        $clinician_total['collectedhour'] = 0;
        $clinician_total['unitsbilling'] = 0;
        $clinician_total['visitsbilling'] = 0;
        $clinician_total['thourdays'] = 0;
        $clinician_total['scheddensity'] = 0;

        $clinician_total['new_cases'] = 0;
        $clinician_total['active_discharges'] = 0;
        $clinician_total['passive_discharges'] = 0;
        $clinician_total['active_cases'] = 0;
        $clinician_total['visits_attended'] = 0;
        $clinician_total['no_show'] = 0;
        $clinician_total['visits_scheduled'] = 0;
        $clinician_total['units_charged'] = 0;
        $clinician_total['days_worked'] = 0;
        $clinician_total['hours_worked'] = 0;
        $clinician_total['hours_scheduled'] = 0;
        $clinician_total['billing_days'] = 0;

        $temp = array();
        if ($this->input->post('date_selected') != '0') {
            $display_data = $this->display_data_dashboard($this->input->post('date_selected'), false);
            $week_date_attr = $display_data['week_date_attr'];
            $selected_date = $this->input->post('date_selected');
            $location_id = $this->input->post('location_id');
            $week_dates = $this->get_week_dates($selected_date);
            $week_dates_assign = $this->convert_date_format('Y-m-d', $week_dates);
            $check_clinician_res = $this->check_clinician_baseline($user_id, $location_id);
            $baseline_date = $check_clinician_res['start'];
            $termination_date = $check_clinician_res['termination'];
            for ($ctr = 1; $ctr <= 7; $ctr++) {
                $date = $week_dates_assign[$ctr];
                $clinician_data[$date] = $this->load_data_from_clinician($date, $date, $location_id);
                if (!$clinician_data[$date] || $clinician_data[$date] == '0' || $clinician_data[$date] == '') {
                    $clinician_data[$date] = 0;
                }
            }
        } else {
            $display_data = $this->display_data_dashboard(date('Y-m-d'), false);
            $week_date_attr = $display_data['week_date_attr'];
            $selected_date = date('Y-m-d');
            $location_id = $this->input->post('location_id');
            $week_dates = $this->get_week_dates($selected_date);
            $week_dates_assign = $this->convert_date_format('Y-m-d', $week_dates);
            $check_clinician_res = $this->check_clinician_baseline($user_id, $location_id);
            $baseline_date = $check_clinician_res['start'];
            $termination_date = $check_clinician_res['termination'];
            for ($ctr = 1; $ctr <= 7; $ctr++) {
                $date = $week_dates_assign[$ctr];
                $clinician_data[$date] = $this->load_data_from_clinician($date, $date, $location_id);
                if (!$clinician_data[$date] || $clinician_data[$date] == '0' || $clinician_data[$date] == '') {
                    $clinician_data[$date] = 0;
                }
            }
        }
        for ($ctr = 1; $ctr <= 7; $ctr++) {
            $date = $week_dates_assign[$ctr];
            $clinician_total['new_cases'] += $clinician_data[$date]['new_cases'];
            $clinician_total['active_discharges'] += $clinician_data[$date]['active_discharges'];
            $clinician_total['passive_discharges'] += $clinician_data[$date]['passive_discharges'];
            if ($clinician_data[$date]['active_cases'] != 0) {
                $clinician_total['active_cases'] = $clinician_data[$date]['active_cases'];
            }
            $clinician_total['visits_attended'] += $clinician_data[$date]['visits_attended'];
            $clinician_total['no_show'] += $clinician_data[$date]['no_show'];
            $clinician_total['visits_scheduled'] += $clinician_data[$date]['visits_scheduled'];
            $clinician_total['units_charged'] += $clinician_data[$date]['units_charged'];
            $clinician_total['days_worked'] += $clinician_data[$date]['days_worked'];
            $clinician_total['hours_worked'] += $clinician_data[$date]['hours_worked'];
            $clinician_total['hours_scheduled'] += $clinician_data[$date]['hours_scheduled'];
            $clinician_total['billing_days'] += $clinician_data[$date]['billing_days'];
        }


        for ($ctr = 1; $ctr <= 7; $ctr++) {
            $collectionrate = $this->collectionrates->get_collection_rate_by_date($location_id, $selected_date);
            $date = $week_dates_assign[$ctr];

            //biling days

            $clinician_computation[$date]['collected'] = number_format($clinician_data[$date]['charges'] * ( $collectionrate / 100), 2);
            $clinician_computation[$date]['charges'] = $clinician_data[$date]['charges'];
            $clinician_computation[$date]['bdays'] = $clinician_data[$date]['billing_days'];
            //compute for the total for billing days
            $clinician_total['collected'] += $clinician_computation[$date]['collected'];
            $clinician_total['charges'] += $clinician_computation[$date]['charges'];
            $clinician_total['bdays'] += $clinician_computation[$date]['bdays'];




//Unit Ratios
            $clinician_computation[$date]['units_visits'] = $this->divide($clinician_data[$date]['units_charged'], $clinician_data[$date]['visits_attended']);
            $clinician_computation[$date]['unitsactive'] = $this->divide($clinician_data[$date]['units_charged'], $clinician_data[$date]['active_cases']);
            $clinician_computation[$date]['unitshour'] = $this->divide($clinician_data[$date]['units_charged'], $clinician_data[$date]['hours_worked']);
            $clinician_computation[$date]['units8hour'] = $this->divide($clinician_data[$date]['units_charged'], ($clinician_data[$date]['hours_worked'] / 8));
            $clinician_computation[$date]['chargeunit'] = '$ ' . $this->divide($clinician_data[$date]['charges'], $clinician_data[$date]['units_charged']);
            $clinician_computation[$date]['collectedunit'] = '$ ' . $this->divide($clinician_data[$date]['charges'] * ( $collectionrate / 100), $clinician_data[$date]['units_charged']);
            //totals for Unit ratios
            $clinician_total['units_visits'] = $this->divide($clinician_total['units_charged'], $clinician_total['visits_attended']);
            $clinician_total['unitsactive'] = $this->divide($clinician_total['units_charged'], $clinician_total['active_cases']);
            $clinician_total['unitshour'] = $this->divide($clinician_total['units_charged'], $clinician_total['hours_worked']);
            $clinician_total['units8hour'] = $this->divide($clinician_total['units_charged'], ($clinician_total['hours_worked'] / 8));
            $clinician_total['chargeunit'] = $this->divide($clinician_total['charges'], $clinician_total['units_charged']);
            $clinician_total['collectedunit'] = $this->divide($clinician_total['charges'] * ( $collectionrate / 100), $clinician_total['units_charged']);





            //Visit Ratios
            $clinician_computation[$date]['visitscase'] = $this->divide($clinician_data[$date]['visits_attended'], $clinician_data[$date]['active_cases']);
            $clinician_computation[$date]['visitshour'] = $this->divide($clinician_data[$date]['visits_attended'], $clinician_data[$date]['hours_worked']);
            $clinician_computation[$date]['visits8hour'] = $this->divide($clinician_data[$date]['visits_attended'], ($clinician_data[$date]['hours_worked'] / 8));
            $clinician_computation[$date]['chargevisit'] = '$ ' . $this->divide($clinician_data[$date]['charges'], $clinician_data[$date]['visits_attended']);
            $clinician_computation[$date]['collectedvisit'] = '$ ' . $this->divide($clinician_data[$date]['charges'] * ( $collectionrate / 100), $clinician_data[$date]['visits_attended']);
            $clinician_total['visitscase'] = $this->divide($clinician_total['visits_attended'], $clinician_total['active_cases']);
            $clinician_total['visitshour'] = $this->divide($clinician_total['visits_attended'], $clinician_total['hours_worked']);
            $clinician_total['visits8hour'] = $this->divide($clinician_total['visits_attended'], ($clinician_total['hours_worked'] / 8));
            $clinician_total['chargevisit'] = $this->divide($clinician_total['charges'], $clinician_total['visits_attended']);
            $clinician_total['collectedvisit'] = $this->divide($clinician_total['charges'] * ( $collectionrate / 100), $clinician_total['visits_attended']);

            //Case Management Ratios
            $clinician_computation[$date]['casesday'] = $this->divide($clinician_data[$date]['active_cases'], $clinician_data[$date]['days_worked']);
            $clinician_computation[$date]['passiverate'] = $this->divide($clinician_data[$date]['passive_discharges'], ($clinician_data[$date]['active_discharges'] + $clinician_data[$date]['passive_discharges'])) . ' %';
            $clinician_computation[$date]['attendrate'] = number_format($this->divide2($clinician_data[$date]['visits_attended'], $clinician_data[$date]['visits_scheduled']), 2);

            $clinician_total['casesday'] = $this->divide($clinician_total['active_cases'], $clinician_total['days_worked']);
            $clinician_total['passiverate'] = number_format($this->divide2($clinician_total['passive_discharges'], ($clinician_total['active_discharges'] + $clinician_total['passive_discharges'])), 2);
            $clinician_total['attendrate'] = number_format($this->divide2(($clinician_total['visits_attended']), ($clinician_total['visits_scheduled'])), 2);

            //Financial Ratios
            $clinician_computation[$date]['chargeshour'] = '$ ' . $this->divide($clinician_data[$date]['charges'], $clinician_data[$date]['hours_worked']);
            $clinician_computation[$date]['collectedhour'] = '$ ' . $this->divide($clinician_data[$date]['charges'] * ( $collectionrate / 100), $clinician_data[$date]['hours_worked']);
            $clinician_total['chargeshour'] = '$ ' . $this->divide($clinician_total['charges'], $clinician_total['hours_worked']);
            $clinician_total['collectedhour'] = '$ ' . $this->divide($clinician_total['charges'] * ( $collectionrate / 100), $clinician_total['hours_worked']);


            //Staffing
            $clinician_computation[$date]['unitsbilling'] = $this->divide($clinician_data[$date]['units_charged'], $clinician_data[$date]['billing_days']);
            $clinician_computation[$date]['visitsbilling'] = $this->divide($clinician_data[$date]['visits_attended'], $clinician_data[$date]['billing_days']);
            $clinician_computation[$date]['thourdays'] = $this->divide($clinician_data[$date]['hours_worked'], $clinician_data[$date]['days_worked']);
            $clinician_computation[$date]['scheddensity'] = $this->divide($clinician_data[$date]['hours_scheduled'], $clinician_data[$date]['hours_worked']);
            $clinician_total['unitsbilling'] = $this->divide($clinician_total['units_charged'], $clinician_total['billing_days']);
            $clinician_total['visitsbilling'] = $this->divide($clinician_total['visits_attended'], $clinician_total['billing_days']);
            $clinician_total['thourdays'] = $this->divide($clinician_total['hours_worked'], $clinician_total['days_worked']);
            $clinician_total['scheddensity'] = $this->divide($clinician_total['hours_scheduled'], $clinician_total['hours_worked']);
        }


        for ($ctr = 1; $ctr <= 7; $ctr++) {
            $date = $week_dates_assign[$ctr];
            if ($clinician_data[$date]['hours_worked'] == 0) {

                if ($clinician_computation[$date]['collected'] == 0 || $clinician_computation[$date]['collected'] == '' || $clinician_computation[$date]['collected'] == 0.00) {
                    $clinician_computation[$date]['collected'] = '--';
                }
                if ($clinician_computation[$date]['charges'] == 0 || $clinician_computation[$date]['charges'] == '' || $clinician_computation[$date]['charges'] == 0.00) {
                    $clinician_computation[$date]['charges'] = '--';
                }
                if ($clinician_computation[$date]['bdays'] == 0 || $clinician_computation[$date]['bdays'] == '') {
                    $clinician_computation[$date]['bdays'] = '--';
                }


                if ($clinician_computation[$date]['units_visits'] == 0 || $clinician_computation[$date]['units_visits'] == '' || $clinician_computation[$date]['units_visits'] == 0.00) {
                    $clinician_computation[$date]['units_visits'] = '--';
                }
                if ($clinician_computation[$date]['unitsactive'] == 0 || $clinician_computation[$date]['unitsactive'] == '' || $clinician_computation[$date]['unitsactive'] == 0.00) {
                    $clinician_computation[$date]['unitsactive'] = '--';
                }
                if ($clinician_computation[$date]['units_visits'] == 0 || $clinician_computation[$date]['units_visits'] == '' || $clinician_computation[$date]['units_visits'] == 0.00) {
                    $clinician_computation[$date]['units_visits'] = '--';
                }
                if ($clinician_computation[$date]['unitshour'] == 0 || $clinician_computation[$date]['unitshour'] == '' || $clinician_computation[$date]['unitshour'] == 0.00) {
                    $clinician_computation[$date]['unitshour'] = '--';
                }
                if ($clinician_computation[$date]['units8hour'] == 0 || $clinician_computation[$date]['units8hour'] == '' || $clinician_computation[$date]['units8hour'] == 0.00) {
                    $clinician_computation[$date]['units8hour'] = '--';
                }
                if ($clinician_computation[$date]['chargeunit'] == 0 || $clinician_computation[$date]['chargeunit'] == '' || $clinician_computation[$date]['chargeunit'] == 0.00) {
                    $clinician_computation[$date]['chargeunit'] = '--';
                }
                if ($clinician_computation[$date]['collectedunit'] == 0 || $clinician_computation[$date]['collectedunit'] == '' || $clinician_computation[$date]['collectedunit'] == 0.00) {
                    $clinician_computation[$date]['collectedunit'] = '--';
                }



                if ($clinician_computation[$date]['visitscase'] == 0 || $clinician_computation[$date]['visitscase'] == '' || $clinician_computation[$date]['visitscase'] == 0.00) {
                    $clinician_computation[$date]['visitscase'] = '--';
                }
                if ($clinician_computation[$date]['visitshour'] == 0 || $clinician_computation[$date]['visitshour'] == '' || $clinician_computation[$date]['visitshour'] == 0.00) {
                    $clinician_computation[$date]['visitshour'] = '--';
                }
                if ($clinician_computation[$date]['visits8hour'] == 0 || $clinician_computation[$date]['visits8hour'] == '' || $clinician_computation[$date]['visits8hour'] == 0.00) {
                    $clinician_computation[$date]['visits8hour'] = '--';
                }
                if ($clinician_computation[$date]['chargevisit'] == 0 || $clinician_computation[$date]['chargevisit'] == '' || $clinician_computation[$date]['chargevisit'] == 0.00) {
                    $clinician_computation[$date]['chargevisit'] = '--';
                }
                if ($clinician_computation[$date]['collectedvisit'] == 0 || $clinician_computation[$date]['collectedvisit'] == '' || $clinician_computation[$date]['collectedvisit'] == 0.00) {
                    $clinician_computation[$date]['collectedvisit'] = '--';
                }


                if ($clinician_computation[$date]['casesday'] == 0 || $clinician_computation[$date]['casesday'] == '' || $clinician_computation[$date]['casesday'] == 0.00) {
                    $clinician_computation[$date]['casesday'] = '--';
                }
                if ($clinician_computation[$date]['passiverate'] == 0 || $clinician_computation[$date]['passiverate'] == '' || $clinician_computation[$date]['passiverate'] == 0.00) {
                    $clinician_computation[$date]['passiverate'] = '--';
                }
                if ($clinician_computation[$date]['attendrate'] == 0 || $clinician_computation[$date]['attendrate'] == '' || $clinician_computation[$date]['attendrate'] == 0.00) {
                    $clinician_computation[$date]['attendrate'] = '--';
                }



                if ($clinician_computation[$date]['chargeshour'] == 0 || $clinician_computation[$date]['chargeshour'] == '') {
                    $clinician_computation[$date]['chargeshour'] = '--';
                }
                if ($clinician_computation[$date]['collectedhour'] == 0 || $clinician_computation[$date]['collectedhour'] == '') {
                    $clinician_computation[$date]['collectedhour'] = '--';
                }

                if ($clinician_computation[$date]['unitsbilling'] == 0 || $clinician_computation[$date]['unitsbilling'] == '') {
                    $clinician_computation[$date]['unitsbilling'] = '--';
                }
                if ($clinician_computation[$date]['visitsbilling'] == 0 || $clinician_computation[$date]['visitsbilling'] == '') {
                    $clinician_computation[$date]['visitsbilling'] = '--';
                }
                if ($clinician_computation[$date]['scheddensity'] == 0 || $clinician_computation[$date]['scheddensity'] == '') {
                    $clinician_computation[$date]['scheddensity'] = '--';
                }
                if ($clinician_computation[$date]['thourdays'] == 0 || $clinician_computation[$date]['thourdays'] == '') {
                    $clinician_computation[$date]['thourdays'] = '--';
                }
            } else {

                $clinician_computation[$date]['charges'] = '$ ' . number_format($clinician_computation[$date]['charges'], 2);
                $clinician_computation[$date]['collected'] = '$ ' . $clinician_computation[$date]['collected'];
            }

            if ($clinician_computation[$date]['collected'] != 0 || $clinician_computation[$date]['charges'] != 0.00) {
                $clinician_computation[$date]['collected'] = '$ ' . $clinician_computation[$date]['collected'];
            }
            if ($clinician_computation[$date]['charges'] != 0 || $clinician_computation[$date]['charges'] != 0.00) {
                $clinician_computation[$date]['charges'] = '$ ' . number_format($clinician_computation[$date]['charges'], 2);
            }
            if ($clinician_computation[$date]['bdays'] != 0) {
                $clinician_computation[$date]['bdays'] = $clinician_computation[$date]['bdays'];
            }
            if ($clinician_computation[$date]['chargeunit'] != 0 || $clinician_computation[$date]['chargeunit'] != 0.00) {
                $clinician_computation[$date]['chargeunit'] = '$ ' . number_format($clinician_computation[$date]['chargeunit'], 2);
            }
            if ($clinician_computation[$date]['collectedunit'] != 0 || $clinician_computation[$date]['collectedunit'] != 0.00) {
                $clinician_computation[$date]['collectedunit'] = '$ ' . number_format($clinician_computation[$date]['collectedunit'], 2);
            }
            if ($clinician_computation[$date]['chargeshour'] != 0 || $clinician_computation[$date]['chargeshour'] != 0.00) {
                $clinician_computation[$date]['chargeshour'] = '$ ' . number_format($clinician_computation[$date]['chargeshour'], 2);
            }
            if ($clinician_computation[$date]['collectedhour'] != 0 || $clinician_computation[$date]['collectedhour'] != 0.00) {
                $clinician_computation[$date]['collectedhour'] = '$ ' . number_format($clinician_computation[$date]['collectedhour'], 2);
            }
            if ($clinician_computation[$date]['chargevisit'] != 0 || $clinician_computation[$date]['chargevisit'] != 0.00) {
                $clinician_computation[$date]['chargevisit'] = '$ ' . $clinician_computation[$date]['chargevisit'];
            }
            if ($clinician_computation[$date]['collectedvisit'] != 0) {
                $clinician_computation[$date]['collectedvisit'] = '$ ' . number_format($clinician_computation[$date]['collectedvisit'], 2);
            }
            if ($clinician_computation[$date]['passiverate'] != 0 || $clinician_computation[$date]['passiverate'] != 0.00) {
                $clinician_computation[$date]['passiverate'] = $clinician_computation[$date]['passiverate'] . ' %';
            }
            if ($clinician_computation[$date]['attendrate'] != 0 || $clinician_computation[$date]['attendrate'] != 0.00) {
                $clinician_computation[$date]['attendrate'] = $clinician_computation[$date]['attendrate'] . ' %';
            }
        }

//        echo 'Position is'.print_r($position);
        $is_pta = false;
        $position = $this->userpositions->get_position_by_id($this->session->userdata('user_id'));


        if ($position[0]['position_id'] == '2') {
            $is_pta = true;
        }



        //if daily reporting


        $type_request = $this->input->post('is_error');
        $type_arr = array();
        if (!$type_request) {
            $data = array(
                'week_date_attr' => $week_date_attr,
                'selected_date' => $selected_date,
                'today' => date('Y-m-d'),
                'clinician_data' => $clinician_data,
                'baseline_date' => $baseline_date,
                'termination_date' => $termination_date,
                'clinician_computation' => $clinician_computation,
                'clinician_total' => $clinician_total,
                'is_pta' => $is_pta
            );
            $this->load->view('templates/clinician_dashboard', $data);
        } else {

            $data = array(
                'week_date_attr' => $week_date_attr,
                'selected_date' => $selected_date,
                'today' => date('Y-m-d'),
                'clinician_data' => $clinician_data,
                'baseline_date' => $baseline_date,
                'termination_date' => $termination_date,
                'clinician_computation' => $clinician_computation,
                'clinician_total' => $clinician_total,
                'type_request' => json_encode($type_request),
                'is_pta' => $is_pta
            );
            $this->load->view('templates/clinician_dashboard', $data);
        }
    }

    public function load_data_from_clinician($start_date, $end_date, $location_id) {
        $user_id = $this->session->userdata('user_id');

        $ret = $this->cliniciandata->get_clinician_data($user_id, $location_id, $start_date, $end_date);
        if (isset($ret[0])) {
            return $ret[0];
        } else {
            return $ret;
        }
    }

    public function submit_clinician_data() {
        $error = array();
        $warning = array();
        $error['status'] = 0;

        $error_date = array();
        $newcase = $this->input->post('newcase');
        $activedis = $this->input->post('activedis');
        $passivedis = $this->input->post('passivedis');
//        $activecase = $this->input->post('activecase');
        $visitsattend = $this->input->post('visitsattend');
        $visitscancels = $this->input->post('visitscancels');
//        $visitssched = (int)$this->input->post('visitsattend') + (int)$this->input->post('visitscancels');
        $unitscharged = $this->input->post('unitscharged');
//        $daysworked = $this->input->post('daysworked');
        $hoursworked = $this->input->post('hoursworked');
        $clinicshed = $this->input->post('clinicshed');
        $selected_date = $this->input->post('selected_date');
        $location_id = $this->input->post('location_id');
        $user_id = $this->session->userdata('user_id');
        $week_dates = $this->get_week_dates($selected_date);
        $week_dates_assign = $this->convert_date_format('Y-m-d', $week_dates);
        $status = array();
        for ($ctr = 1; $ctr <= 7; $ctr++) {
            $date = $week_dates_assign[$ctr];
            $newcase_temp = $newcase[$date];
            $activedis_todb = $activedis[$date];
            $passivedis_todb = $passivedis[$date];
            $visitsattend_todb = $visitsattend[$date];
            $visitscancels_todb = $visitscancels[$date];
            $visitssched_todb = $visitsattend_todb + $visitscancels_todb;
            $unitscharged_todb = $unitscharged[$date];
//            $daysworked_todb = $daysworked[$date];
            $hoursworked_todb = $hoursworked[$date];
            $clinicshed_todb = $clinicshed[$date];


            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'location_id' => $location_id,
                'start_date' => "'" . $date . "'",
                'end_date' => "'" . $date . "'",
                'new_cases' => $newcase_temp,
                'active_discharges' => $activedis_todb,
                'passive_discharges' => $passivedis_todb,
                'added_cases' => 0,
                'removed_cases' => 0,
                'visits_attended' => $visitsattend_todb,
                'visits_scheduled' => $visitssched_todb,
                'no_show' => $visitscancels_todb,
                'units_charged' => number_format($unitscharged_todb, 2),
                'days_worked' => 1,
                'hours_worked' => number_format($hoursworked_todb, 2),
                'hours_scheduled' => number_format($clinicshed_todb, 2)
            );
//            print_r($data);
            $update = $this->check_if_update($date, $location_id);
            $baseline_res = $this->check_clinician_baseline($user_id, $location_id);
            $baseline_date = $baseline_res['start'];
            $termination_date = $baseline_res['termination'];


            $allow = 0;
            if(isset($_POST['check-col'])){
                $check = $this->input->post('check-col');
                foreach($check as $key => $value){
                    if($ctr == $key) {
                        $allow = 1;
                    }
                }
            }

            if ($update) {
                if ($allow) {
                    //update
                    $activecase_todb = $this->compute_active_cases($date, $user_id, $location_id, $newcase_temp, $activedis_todb, $passivedis_todb);
                    $data['active_cases'] = $activecase_todb;
                    $res = $this->update_clinician($data, $user_id, $location_id, $date, $date);

                    if ($res != 0) {
                        $status[$ctr]['update'] = 1;
                        $status[$ctr]['hours_worked_error'] = 0;
                    } else if ($res == 0) {
                        $status[$ctr]['update'] = 0;
                        $status[$ctr]['hours_worked_error'] = 0;
                    }
                } else {
                    //delete
                    $billingexists = $this->cliniciandatas->checkbilling($user_id,$date, $location_id);
                    if (!$billingexists) {
                        $this->delete_clinician_data($user_id, $location_id, $date);
                        $status[$ctr]['delete'] = 1;
                    } else {
                        $data = array(
                            'new_cases' => 0,
                            'active_discharges' => 0,
                            'passive_discharges' => 0,
                            'visits_attended' => 0,
                            'visits_scheduled' => 0,
                            'no_show' => 0,
                            'units_charged' => 0,
                            'days_worked' =>0,
                            'hours_worked' => 0,
                            'hours_scheduled' => 0,  
                        );
                        $this->update_clinician($data, $user_id, $location_id, $date, $date);
                         $status[$ctr]['delete'] = 1;
                    }
                }
            } else {

                if($allow){
                    //create
                    if (strtotime($baseline_date) <= strtotime($date)) {
                        if ($termination_date == '0000-00-00') {
                            $activecase_todb = $this->compute_active_cases($date, $user_id, $location_id, $newcase_temp, $activedis_todb, $passivedis_todb, false);
                            $data['active_cases'] = $activecase_todb;
                            $this->save_clinician_to_db($data);
                            $status[$ctr]['save'] = 1;
                        } else if ($termination_date != '0000-00-00') {
                            if (strtotime($termination_date) > strtotime($date)) {
                                $activecase_todb = $this->compute_active_cases($date, $user_id, $location_id, $newcase_temp, $activedis_todb, $passivedis_todb);
                                $data['active_cases'] = $activecase_todb;
                                $this->save_clinician_to_db($data);
                                $status[$ctr]['save'] = 1;
                            } else {
                                $status[$ctr]['save_error'] = 1;
                                $error['main'] = 1;
                                $error['termination'] = 'Data can\'t be saved because your termination date is ' . $termination_date;
                            }
                        }
                    } else {
                        $error['main'] = 1;
                        $error['baseline'] = 'Data can\'t be saved because your baseline date is ' . $baseline_date;
                    }
                } else if ($allow) {
                    //error
//$error['hours_worked'] = "Data can\'t be saved. No hours worked detected";
                    $status[$ctr]['create'] = 0;
                    $status[$ctr]['error_saving'] = 0;
                    if ($newcase_temp != '0' || $activedis_todb != '0' || $passivedis_todb != '0' || $visitsattend_todb != '0' || $visitscancels_todb != '0' || $unitscharged_todb != '0' || $clinicshed_todb != '0') {
                        $status[$ctr]['hours_worked_error'] = 1;
                    } else {
                        $status[$ctr]['hours_worked_error'] = 0;
                    }
                }
            }
        }
        $st = array();



        $this->recompute_active_cases($user_id, $location_id, $date, $date);
        $response = array(
            'status' => $status,
            'error' => $error,
            'error_date' => $error_date,
            'dates' => $week_dates_assign
        );
        echo json_encode($response);
    }

    public function check_if_update($selected_date, $location_id) {
        $data = $this->display_data_dashboard($selected_date, false);
        $user_id = $this->session->userdata('user_id');
        $response = $this->cliniciandata->get_clinician_data($user_id, $location_id, $selected_date, $selected_date);
        if ($response) {
            return true;
        } else {
            return false;
        }
    }

    public function compute_active_cases($date, $user_id, $location_id, $new_case, $active_discharges, $passive_discharges) {

        $res = $this->cliniciandata->check_previous_active_case($date, $user_id, $location_id);
//        print_r($res[0]);
        $initial_case = $this->userlocations->get_initial_case($this->session->userdata('user_id'), $location_id);
        if (isset($res[0])) {
//            print_r($res[0]);
            $previous_case = (int) $res[0]['active_cases'];
            $active_case = ($previous_case + $new_case) - ($active_discharges + $passive_discharges);
        } else {
            $previous_case = (int) $initial_case[0]['initial_cases'];
//            print_r('Initial case: '.$previous_case);
            $active_case = ($previous_case + $new_case) - ($active_discharges + $passive_discharges);
        }

        return $active_case;
    }

    public function recompute_active_cases($user_id, $location_id, $start_date, $end_date) {
        $res = $this->cliniciandata->get_all_dates_forth($user_id, $location_id, $start_date, $end_date);
        $clinician_data = array();
        foreach ($res as $clinician_data) {
            $active_case = $this->compute_active_cases($clinician_data['start_date'], $user_id, $location_id, $clinician_data['new_cases'], $clinician_data['active_discharges'], $clinician_data['passive_discharges']);
            $data = array('active_cases' => $active_case);
            $this->update_clinician($data, $user_id, $location_id, $clinician_data['start_date'], $clinician_data['start_date']);
        }
    }

    public function check_clinician_baseline($user_id, $location_id) {
        $data = array();
        $res = $this->userlocations->get_clinician_start_end_date($user_id, $location_id);
        $data['start'] = $res[0]['start_date'];
        $data['termination'] = $res[0]['termination_date'];
        return $data;
    }

    public function save_clinician_to_db($data) {
        if ($this->cliniciandata->create_clinician_data($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_clinician($data, $user_id, $location_id, $start_date, $end_date) {
        $res = $this->cliniciandata->update_clinician_data($data, $user_id, $location_id, $start_date, $end_date);
        return $res;
    }

    public function delete_clinician_data($user_id, $location_id, $selected_date) {
        if ($this->cliniciandata->delete_clinician_data($user_id, $location_id, $selected_date, $selected_date)) {
            return true;
        } else {
            return false;
        }
    }

    public function convert_date_format($format, $date_array) {
        $date = array();
        for ($i = 1, $h = 1; $i <= count($date_array); $i++, $h++) {
            $date[$i] = strtotime($date_array[$h]);
            $date[$i] = date($format, $date[$i]);
        }
        return $date;
    }

    function divide($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor), 2);
            return $quotient;
        }
    }

    function divide2($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor) * 100, 2);
            return $quotient;
        }
    }

}
