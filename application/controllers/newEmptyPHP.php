<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Minimum_dataset_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('practices');
        $this->load->model('userpractices');
        $this->load->model('locations');
        $this->load->model('cliniciandatas');
        $this->load->model('userlocations');
        $this->load->model('users');
        $this->load->model('collectionrates');
    }

    public function check_if_authorized() {
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }

        $this->check_if_authorized();

        $roles = $this->session->userdata('roles');
        $practiceId = $this->session->userdata('practice_id');
//check roles
        if ($this->in_array_r('Super Administrator', $roles)) { //if Super ADmin
//get all locations
            $locations = $this->userlocations->get_all();
        } elseif ($this->in_array_r('Practice Owner', $roles)) {//if Practice Owner
//get practice locations
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
//get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        $data1 = array(
            'location_data' => $locations,
        );

        $this->load->vars($data1);
        $this->template->set_layout('default');
        $this->template->title('Minimum Data Set Dashboard');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/minimum_data_set.js") . '"></script>');
        $this->template->build('pages/minimum_dataset_dashboard');
    }

    public function get_all_minimum_report($user_array, $start_date, $end_date) {
        $roles = $this->session->userdata('roles');
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date-minimum')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date-minimum')));



        $locationId = $this->input->post('location');
        $usersData = array();
//get users under location selected

        $response = array();
        $res_data = array();
        foreach ($user_array as $userID => $individual) {
//            $response[$userID] = $this->cliniciandata->get_clinician_data_by_date($uid, $locID, date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));
            $tnewCases = 0;
            $taDis = 0;
            $tpDis = 0;
            $addCases = 0;
            $remCases = 0;
            $activeCases = 0;
            $vAttended = 0;
            $vScheduled = 0;
            $vCanceled = 0;
            $uCharged = 0;
            $dWorked = 0;
            $hWorked = 0;
            $hScheduled = 0;
            $collected = 0;
            $charges = 0;
            $bdays = 0;
            $emp_id = '';
            $fname = '';
            $lname = '';
            $include = false;
            
            foreach ($individual as $locID => $uid) {
                $response[$uid][$locID] = $this->cliniciandata->get_clinician_data_by_date($uid, $locID, date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));
                foreach ($response[$uid][$locID] as $key => $new_data) {
                    $collectionrate = $this->collectionrates->get_collection_rate_by_date($locID, $new_data['end_date']);
                    
                    $res_data[$uid][$locID][$key]['visitscase'] = $this->divide($new_data['visits_attended'], $new_data['active_cases']);
                     $res_data[$uid][$locID][$key]['unitsvisit'] = $this->divide($new_data['units_charged'], $new_data['visits_attended']);
                     
                    
                }
            }


            return $usersData;
        }
    }

    public function generate_all_minimum_report($start_date, $end_date, $user_array) {
        $roles = $this->session->userdata('roles');
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date-biller')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date-biller')));
        if ($this->in_array_r('Practice Owner', $roles)) {
            $practiceId = $this->session->userdata('practice_id');
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
//get locations of admin only
            $locations = $this->session->userdata('locs');
        }
        $user_data_loc = array();
        $res = $this->get_all_minimum_report($user_array, $start_date, $end_date);
        foreach ($user_array as $userID => $individual) {
            $username = $this->users->get_user_by_id($userID);


            $str = '<h6> Name: ' . $username[0]['first_name'] . ' ' . $username[0]['last_name'] . '</h6>';
            foreach ($individual as $locID => $uid) {
                
            }
        }



//        print_r($user_array);
    }

    function generate_minimum_dataset_report() {
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date-minimum')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date-minimum')));
        $location = $this->input->post('location');
        if ($location == '0') {

            $user_array = $this->input->post('userid');

            $this->generate_all_minimum_report($sDate, $eDate, $user_array);
        } else {




            $loc_name = $this->locations->get_location_by_id($location);
            $user_id = $this->session->userdata('user_id');
            $practice_id = $this->userpractices->get_practice($user_id);
            $practice_name = $this->practices->get_practice_by_id($practice_id[0]['practice_id']);
            $min_data = $this->generate_mindata_report();
            include_once APPPATH . '/third_party/mpdf/mpdf.php';
            ini_set('memory_limit', '128M');
            $mpdf = new mPDF('utf-8', 'A4', 8, //fontsize
                    '', //fontfamily
                    15, //margin_left
                    15, //margin_right
                    16, //margin_top
                    16, //margin_bottom
                    9, //margin_header
                    9, //margin_footer
                    'L'//orientation      
            );
            $mpdf->SetHeader($practice_name[0]['practice_name'] . " : Minimum Dataset Dashboard" . "||" . "Inclusive Dates: " . date('m/d/Y', strtotime($sDate)) . " to " . date('m/d/Y', strtotime($eDate)));
            $mpdf->SetFooter("Report Generated on " . date(DATE_RFC822) . '||Page {PAGENO}');
            $str = '';
            $style = '<style>
                    table,td,th{
                        border: 1px solid;
                        border-collapse: collapse;
                    }
                    
                    td{
                        text-align: right;
                    }
                  </style>';
            $mpdf->WriteHTML($style);
            $str .= '<h6>  Location: ' . $loc_name[0]['location_name'] . '</h6>';

            $str .= '<table width="100%">';
            $str .= '<tr>'
                    . '<th rowspan="2">Clinician</th>'
                    . '<th colspan="3">Utilization</th>'
                    . '<th colspan="4">Productivity</th>'
                    . '<th colspan="3">Satisfaction</th>'
                    . '</tr>'
                    . '<tr>'
                    . '<th></th>'
                    . '<th>Visits/Case</th>'
                    . '<th>Units/Visits</th>'
                    . '<th>Estimated Collected Revenue</th>'
                    . '<th>Scheduled Hours/Worked Hour</th>'
                    . '<th>Units/Worked Hour</th>'
                    . '<th>Visits/Worked Hour</th>'
                    . '<th>Charges/Worked Hour</th>'
                    . '<th>Visits Canceled/Visits Att.</th>'
                    . '<th>Passive D/C /Active D/C</th>'
                    . '</tr>';

            if ($this->input->post('userid')) {
                if ($min_data) {
                    foreach ($min_data as $data) {
                        if (in_array($data['mindata']['userid'], $this->input->post('userid'))) {
                            $str .= '<tr>';
                            $str .= '<td width="12%" style="text-align: left;">' . $data['mindata']['emp'] . '</td>';
                            $str .= '<td></td>';
                            $str .= '<td width="7%">' . $data['mindata']['visitscase'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['unitsvisit'] . '</td>';
                            $str .= '<td width="10%">' . $data['mindata']['estcolrev'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['schedhourshour'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['unitshour'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['visistshour'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['chargeshour'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['canceledvisits'] . '</td>';
                            $str .= '<td width="7%">' . $data['mindata']['pdisadis'] . '</td>';
                            $str .= '</tr>';
                        }
                    }
                }
                $str .= '</table>';
                $mpdf->WriteHTML($str);
                $mpdf->Output('minimum_dataset.pdf', 'I');
            }
        }
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    function divide($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format($dividend / $divisor, 2);
            return $quotient;
        }
    }

    function divide2($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor) * 100, 2);
            return $quotient;
        }
    }

    public function get_data() {
        $data = '';
        $locid = $this->input->post('location');
        if ($locid == '0') {
            $usersData = $this->get_all_min_data();
        } else {
            $usersData = $this->get_min_data();
        }
        $newData = $this->set_data_array($usersData);
        if ($usersData) {
            $data = array(
                'min' => $newData['min'],
            );
        }

        echo json_encode($data);
    }

    public function set_data_array($data) {
        $min = '';

        foreach ($data as $dat) {
            if ($dat) {
                $min[] = $dat['mindata'];
            }
        }

        $newData = array(
            'min' => $min,
        );

        return $newData;
    }

    public function get_clinician_names() {
        $location_id = $this->input->post('location');
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date')));
        $user_id = $this->session->userdata('user_id');
        $practice_id = $this->userpractices->get_practice($user_id);
        $location = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);


        $clinician_names = $this->locations->get_clinician_list($location_id, $start_date, $end_date);
        $strClinicians = '';
        foreach ($clinician_names as $clini_names) {
            $strClinicians .= '<label class="checkbox">';
            $strClinicians .= '<input type="checkbox" value="' . $clini_names['id'] . '" name="userid[]" checked="checked">';
            $strClinicians .= strtoupper($clini_names['last_name']) . ', ' . $clini_names['first_name'] . ' (' . $clini_names['employee_id'] . ')';
            $strClinicians .= '</label>';
        }

        return $strClinicians;
    }

    function get_all_min_data() {
//get posted dates and location id
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date')));
        $roles = $this->session->userdata('roles');
        $usersData = array();
        if ($this->in_array_r('Practice Owner', $roles)) {
            $practiceId = $this->session->userdata('practice_id');
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
//get locations of admin only
            $locations = $this->session->userdata('locs');
        }



        $this->session->set_userdata('startdate', date('m/d/Y', strtotime($sDate)));
        $this->session->set_userdata('enddate', date('m/d/Y', strtotime($eDate)));



//        $locationId = $this->input->post('location');
        $usersData = array();
        foreach ($locations as $location) {

//get users under location selected
            $userIds = $this->userlocations->get_users_underloc($location['location_id']);

            foreach ($userIds as $id) { //set data values
                $tnewCases = 0;
                $taDis = 0;
                $tpDis = 0;
                $addCases = 0;
                $remCases = 0;
                $activeCases = 0;
                $vAttended = 0;
                $vScheduled = 0;
                $vCanceled = 0;
                $uCharged = 0;
                $dWorked = 0;
                $hWorked = 0;
                $hScheduled = 0;
                $collected = 0;
                $charges = 0;
                $bdays = 0;
                $emp_id = '';
                $fname = '';
                $lname = '';
                $include = false;
//get data per user id passed
                $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $location['location_id'], $sDate, $eDate);

                foreach ($userData as $data) {
//get collection rate per data where date
                    $collectionrate = $this->collectionrates->get_collection_rate_by_date($location['location_id'], $data['end_date']);
                    if ($data) { //sum all data per user within the dates
                        $emp_id = $data['employee_id'];
                        $fname = $data['first_name'];
                        $lname = $data['last_name'];
                        $tnewCases += $data['new_cases'];
                        $taDis += $data['active_discharges'];
                        $tpDis += $data['passive_discharges'];
                        $addCases += $data['added_cases'];
                        $remCases += $data['removed_cases'];
                        $activeCases = $data['active_cases'];
                        $vAttended += $data['visits_attended'];
                        $vScheduled += $data['visits_scheduled'];
                        $vCanceled += $data['no_show'];
                        $uCharged += $data['units_charged'];
                        $dWorked +=$data['days_worked'];
                        $hWorked += $data['hours_worked'];
                        $hScheduled +=$data['hours_scheduled'];
                        $collected += $data['charges'] * ( $collectionrate / 100);
                        $charges +=$data['charges'];
                        $bdays +=$data['billing_days'];
                        $include = true;
                    }
                }
                if ($include) {//set the array to the computed sums per user
                    $usersData[] = array(
//MIN  DATA
                        'mindata' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br>' . $location['location_name'],
                            'visitscase' => $this->divide($vAttended, $activeCases),
                            'unitsvisit' => $this->divide($uCharged, $vAttended),
                            'estcolrev' => '$ ' . $this->divide($collected, 1),
                            'schedhourshour' => $this->divide($hScheduled, $hWorked),
                            'unitshour' => $this->divide($uCharged, $hWorked),
                            'visistshour' => $this->divide($vAttended, $hWorked),
                            'chargeshour' => '$ ' . $this->divide($charges, $hWorked),
                            'canceledvisits' => number_format($this->divide2($vCanceled, $vAttended), 2) . ' %',
                            'pdisadis' => number_format($this->divide2($tpDis, $taDis), 2) . ' %',
                        ),
                    );
                }
            }
        }
        return $usersData;
    }

    function get_min_data() {
//get posted dates and location id
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date')));


        $this->session->set_userdata('startdate', date('m/d/Y', strtotime($sDate)));
        $this->session->set_userdata('enddate', date('m/d/Y', strtotime($eDate)));



        $locationId = $this->input->post('location');
        $usersData = array();
//get users under location selected
        $userIds = $this->userlocations->get_users_underloc($locationId);

        foreach ($userIds as $id) { //set data values
            $tnewCases = 0;
            $taDis = 0;
            $tpDis = 0;
            $addCases = 0;
            $remCases = 0;
            $activeCases = 0;
            $vAttended = 0;
            $vScheduled = 0;
            $vCanceled = 0;
            $uCharged = 0;
            $dWorked = 0;
            $hWorked = 0;
            $hScheduled = 0;
            $collected = 0;
            $charges = 0;
            $bdays = 0;
            $emp_id = '';
            $fname = '';
            $lname = '';
            $include = false;
//get data per user id passed
            $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $locationId, $sDate, $eDate);

            foreach ($userData as $data) {
//get collection rate per data where date
                $collectionrate = $this->collectionrates->get_collection_rate_by_date($locationId, $data['end_date']);
                if ($data) { //sum all data per user within the dates
                    $emp_id = $data['employee_id'];
                    $fname = $data['first_name'];
                    $lname = $data['last_name'];
                    $tnewCases += $data['new_cases'];
                    $taDis += $data['active_discharges'];
                    $tpDis += $data['passive_discharges'];
                    $addCases += $data['added_cases'];
                    $remCases += $data['removed_cases'];
                    $activeCases = $data['active_cases'];
                    $vAttended += $data['visits_attended'];
                    $vScheduled += $data['visits_scheduled'];
                    $vCanceled += $data['no_show'];
                    $uCharged += $data['units_charged'];
                    $dWorked +=$data['days_worked'];
                    $hWorked += $data['hours_worked'];
                    $hScheduled +=$data['hours_scheduled'];
                    $collected += $data['charges'] * ( $collectionrate / 100);
                    $charges +=$data['charges'];
                    $bdays +=$data['billing_days'];
                    $include = true;
                }
            }
            if ($include) {//set the array to the computed sums per user
                $usersData[] = array(
//MIN  DATA
                    'mindata' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'visitscase' => $this->divide($vAttended, $activeCases),
                        'unitsvisit' => $this->divide($uCharged, $vAttended),
                        'estcolrev' => '$ ' . $this->divide($collected, 1),
                        'schedhourshour' => $this->divide($hScheduled, $hWorked),
                        'unitshour' => $this->divide($uCharged, $hWorked),
                        'visistshour' => $this->divide($vAttended, $hWorked),
                        'chargeshour' => '$ ' . $this->divide($charges, $hWorked),
                        'canceledvisits' => number_format($this->divide2($vCanceled, $vAttended), 2) . ' %',
                        'pdisadis' => number_format($this->divide2($tpDis, $taDis), 2) . ' %',
                    ),
                );
            }
        }
        return $usersData;
    }

    public function load_names() {
        $user_id = $this->session->userdata('user_id');
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date')));

        if ($this->in_array_r('Practice Owner', $this->session->userdata('roles'))) {
            $practice_id = $this->userpractices->get_practice($user_id);
            $locations = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);
        } else {
            $locations = $this->session->userdata('locs');
        }

        $strClinicians = '<input type="hidden" name="location-id" value="0"/>';
        foreach ($locations as $location) {

            $clinician_names = $this->locations->get_clinician_list($location['location_id'], $start_date, $end_date);

            if ($clinician_names) {
                $strClinicians .= '<span>' . $location['location_name'] . '</span>';

                foreach ($clinician_names as $clini_names) {
                    $strClinicians .= '<label class="checkbox">';
                    $strClinicians .= '<input type="checkbox" value="' . $clini_names['id'] . '" name="userid[' . $clini_names['id'] . '][' . $location['location_id'] . ']" checked="checked">';
                    $strClinicians .= strtoupper($clini_names['last_name']) . ', ' . $clini_names['first_name'] . ' (' . $clini_names['employee_id'] . ')';
                    $strClinicians .= '</label>';
                }
                $strClinicians .= '<br/>';
            }
        }
        return $strClinicians;
    }

    function generate_mindata_report() {
//get posted dates and location id
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date-minimum')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date-minimum')));

        $this->session->set_userdata('startdate', date('m/d/Y', strtotime($sDate)));
        $this->session->set_userdata('enddate', date('m/d/Y', strtotime($eDate)));

        $locationId = $this->input->post('location');
        $usersData = array();
//get users under location selected
        $userIds = $this->userlocations->get_users_underloc($locationId);

        foreach ($userIds as $id) { //set data values
            $tnewCases = 0;
            $taDis = 0;
            $tpDis = 0;
            $addCases = 0;
            $remCases = 0;
            $activeCases = 0;
            $vAttended = 0;
            $vScheduled = 0;
            $vCanceled = 0;
            $uCharged = 0;
            $dWorked = 0;
            $hWorked = 0;
            $hScheduled = 0;
            $collected = 0;
            $charges = 0;
            $bdays = 0;
            $emp_id = '';
            $fname = '';
            $lname = '';
            $include = false;
//get data per user id passed
            $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $locationId, $sDate, $eDate);

            foreach ($userData as $data) {
//get collection rate per data where date
                $collectionrate = $this->collectionrates->get_collection_rate_by_date($locationId, $data['end_date']);
                if ($data) { //sum all data per user within the dates
                    $emp_id = $data['employee_id'];
                    $fname = $data['first_name'];
                    $lname = $data['last_name'];
                    $tnewCases += $data['new_cases'];
                    $taDis += $data['active_discharges'];
                    $tpDis += $data['passive_discharges'];
                    $addCases += $data['added_cases'];
                    $remCases += $data['removed_cases'];
                    $activeCases = $data['active_cases'];
                    $vAttended += $data['visits_attended'];
                    $vScheduled += $data['visits_scheduled'];
                    $vCanceled += $data['no_show'];
                    $uCharged += $data['units_charged'];
                    $dWorked +=$data['days_worked'];
                    $hWorked += $data['hours_worked'];
                    $hScheduled +=$data['hours_scheduled'];
                    $collected += $data['charges'] * ( $collectionrate / 100);
                    $charges +=$data['charges'];
                    $bdays +=$data['billing_days'];
                    $include = true;
                }
            }
            if ($include) {//set the array to the computed sums per user
                $usersData[] = array(
//MIN  DATA
                    'mindata' => array(
                        'userid' => $id['user_id'],
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . '(' . $emp_id . ')',
                        'visitscase' => $this->divide($vAttended, $activeCases),
                        'unitsvisit' => $this->divide($uCharged, $vAttended),
                        'estcolrev' => '$ ' . $this->divide($collected, 1),
                        'schedhourshour' => $this->divide($hScheduled, $hWorked),
                        'unitshour' => $this->divide($uCharged, $hWorked),
                        'visistshour' => $this->divide($vAttended, $hWorked),
                        'chargeshour' => '$ ' . $this->divide($charges, $hWorked),
                        'canceledvisits' => number_format($this->divide2($vCanceled, $vAttended), 2) . ' %',
                        'pdisadis' => number_format($this->divide2($tpDis, $taDis), 2) . ' %',
                    ),
                );
            }
        }
        return $usersData;
    }

    public function load_clinician_names() {
        $location_id = $this->input->post('location');
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date')));
        $user_id = $this->session->userdata('user_id');
        $practice_id = $this->userpractices->get_practice($user_id);
        $roles = $this->session->userdata('roles');
        if ($location_id == '0') {
            if ($this->in_array_r('Practice Owner', $roles)) {
                $locations = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);
            } else {
                $locations = $this->session->userdata('locs');
            }

            $strClinicians = '';
            foreach ($locations as $location) {
                $clinician_names = $this->locations->get_clinician_list($location['location_id'], $start_date, $end_date);

                if ($clinician_names) {
                    $strClinicians .= '<span>' . $location['location_name'] . '</span>';
                    foreach ($clinician_names as $clini_names) {


                        $strClinicians .= '<label class="checkbox">';
                        $strClinicians .= '<input type="checkbox" value="' . $clini_names['id'] . '" name="userid[' . $clini_names['id'] . '][' . $location['location_id'] . ']" checked="checked">';
                        $strClinicians .= strtoupper($clini_names['last_name']) . ', ' . $clini_names['first_name'] . ' (' . $clini_names['employee_id'] . ')';
                        $strClinicians .= '</label>';
                    }
                }
            }
        } else {

            $clinician_names = $this->locations->get_clinician_list($location_id, $start_date, $end_date);


            $strClinicians = '';
            foreach ($clinician_names as $clini_names) {


                $strClinicians .= '<label class="checkbox">';
                $strClinicians .= '<input type="checkbox" value="' . $clini_names['id'] . '" name="userid[]" checked="checked">';
                $strClinicians .= strtoupper($clini_names['last_name']) . ', ' . $clini_names['first_name'] . ' (' . $clini_names['employee_id'] . ')';
                $strClinicians .= '</label>';
            }
        }

        $data = array(
            $strClinicians
        );

        echo json_encode($data);
    }

}
