<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('cliniciandatas');
        $this->load->model('cliniciandata');
        $this->load->model('userlocations');
        $this->load->model('userpractices');
        $this->load->model('users');
        $this->load->model('collectionrates');
        $this->load->model('locations');
        $this->load->model('practices');
    }

    public function check_if_authorized() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {

        $this->check_if_authorized();

        $roles = $this->session->userdata('roles');
        $practiceId = $this->session->userdata('practice_id');
        //check roles
        if ($this->in_array_r('Super Administrator', $roles)) { //if Super ADmin
            //get all locations
            $locations = $this->userlocations->get_all();
        } elseif ($this->in_array_r('Practice Owner', $roles)) {//if Practice Owner
            //get practice locations
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
            //get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        $data1 = array(
            'location_data' => $locations,
        );

//$this->session->set_userdata('all-locations', $locations);
        $this->load->vars($data1);
        $this->template->set_layout('default');
        $this->template->title('Performance Tracker | Admin Dashboard');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/admin-dashboard.js") . '"></script>');
        $this->template->build('pages/admin_dashboard');
    }

    public function get_data() {
        if ($this->input->post('location') == 0) {
            $usersData = $this->get_all_admin_data();
            $newData = $this->set_data_array($usersData);

            $clinician_list = $this->load_names();
            $data = array(
                'vol' => $newData['vol'],
                'bill' => $newData['bill'],
                'unit' => $newData['unit'],
                'visit' => $newData['visit'],
                'case' => $newData['case'],
                'finance' => $newData['finance'],
                'staff' => $newData['staff'],
                'clinician_list' => $clinician_list
            );
        } else {
            $usersData = $this->get_admin_data();
            $newData = $this->set_data_array($usersData);
            $location_id = $this->input->post('location');
            $start_date = $this->input->post('start-date');
            $end_date = $this->input->post('end-date');

            $this->session->set_userdata('startdate', date('m/d/Y', strtotime($start_date)));
            $this->session->set_userdata('enddate', date('m/d/Y', strtotime($end_date)));

            $clinician_list = $this->load_clinician_names();

            $data = array(
                'vol' => $newData['vol'],
                'bill' => $newData['bill'],
                'unit' => $newData['unit'],
                'visit' => $newData['visit'],
                'case' => $newData['case'],
                'finance' => $newData['finance'],
                'staff' => $newData['staff'],
                'clinician_list' => $clinician_list
            );

//        
        }

        echo json_encode($data);
    }

    public function set_data_array($data) {
        $vol = '';
        $bill = '';
        $unit = '';
        $visit = '';
        $case = '';
        $finance = '';
        $staff = '';

        foreach ($data as $dat) {
            if ($dat) {
                $vol[] = $dat['volume'];
                $bill[] = $dat['billing'];
                $unit[] = $dat['unitratios'];
                $visit[] = $dat['visitratios'];
                $case[] = $dat['casemanage'];
                $finance[] = $dat['finance'];
                $staff[] = $dat['staff'];
            }
        }

        $newData = array(
            'vol' => $vol,
            'bill' => $bill,
            'unit' => $unit,
            'visit' => $visit,
            'case' => $case,
            'finance' => $finance,
            'staff' => $staff,
        );

        return $newData;
    }

    public function get_all_admin_data() {
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date')));
        $roles = $this->session->userdata('roles');
        $usersData = array();
        if ($this->in_array_r('Practice Owner', $roles)) {
            $practiceId = $this->session->userdata('practice_id');
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
            //get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        foreach ($locations as $location) {
            $userIds = $this->userlocations->get_users_underloc($location['location_id']);

            foreach ($userIds as $id) { //set data values
                $tnewCases = 0;
                $taDis = 0;
                $tpDis = 0;
                $addCases = 0;
                $remCases = 0;
                $activeCases = 0;
                $vAttended = 0;
                $vScheduled = 0;
                $vCanceled = 0;
                $uCharged = 0;
                $dWorked = 0;
                $hWorked = 0;
                $hScheduled = 0;
                $collected = 0;
                $charges = 0;
                $bdays = 0;
                $emp_id = '';
                $fname = '';
                $lname = '';
                $include = false;
                $strClinicians = '';
                $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $location['location_id'], $sDate, $eDate);

                foreach ($userData as $data) {
//get collection rate per data where date
                    $collectionrate = $this->collectionrates->get_collection_rate_by_date($location['location_id'], $data['end_date']);
                    if ($data) { //sum all data per user within the dates
                        $emp_id = $data['employee_id'];
                        $fname = $data['first_name'];
                        $lname = $data['last_name'];
                        $locname = $location['location_name'];
                        $tnewCases += $data['new_cases'];
                        $taDis += $data['active_discharges'];
                        $tpDis += $data['passive_discharges'];
                        $addCases += $data['added_cases'];
                        $remCases += $data['removed_cases'];
                        $activeCases = $data['active_cases'];
                        $vAttended += $data['visits_attended'];
                        $vScheduled += $data['visits_scheduled'];
                        $vCanceled += $data['no_show'];
                        $uCharged += $data['units_charged'];
                        $dWorked +=$data['days_worked'];
                        $hWorked += $data['hours_worked'];
                        $hScheduled +=$data['hours_scheduled'];
                        $collected += $data['charges'] * ( $collectionrate / 100);
                        $charges +=$data['charges'];
                        $bdays +=$data['billing_days'];
                        $include = true;
                    }
                }

                if ($include) {//set the array to the computed sums per user
                    //Unit Ratios
                    $unitsvisit = $this->divide($uCharged, $vAttended);
                    $unitsactive = $this->divide($uCharged, $activeCases);
                    $unitshour = $this->divide($uCharged, $hWorked);
                    $units8hour = $this->divide($uCharged, ($hWorked / 8));
                    $chargeunit = $this->divide($charges, $uCharged);
                    $collectedunit = $this->divide($collected, $uCharged);

                    //Visit Ratios
                    $visitscase = $this->divide($vAttended, $activeCases);
                    $visitshour = $this->divide($vAttended, $hWorked);
                    $visits8hour = $this->divide($vAttended, ($hWorked / 8));
                    $chargesvisit = $this->divide($charges, $vAttended);
                    $collectedvisit = $this->divide($collected, $vAttended);
                    $noshowvisit = $this->divide2($vCanceled, $vAttended);


                    //Case Management
                    $casesday = $this->divide($activeCases, $dWorked);
                    $passiverate = $this->divide2($tpDis, ($taDis + $tpDis));
                    $attendrate = $this->divide2($vAttended, $vScheduled);
                    $pdisadis = $this->divide2($tpDis, $taDis);

                    //Financial Ratios
                    $chargeshour = $this->divide($charges, $hWorked);
                    $collectedhour = $this->divide($collected, $hWorked);

//Staffing Ratios
                    $unitsbilling = $this->divide($uCharged, $bdays);
                    $visitsbilling = $this->divide($vAttended, $bdays);
                    $thourdays = $this->divide($hWorked, $dWorked);
                    $scheddensity = $this->divide($hScheduled, $hWorked);

                    $usersData[] = array(
//Volume  
                        'volume' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'newcases' => $tnewCases,
                            'adis' => $taDis,
                            'pdis' => $tpDis,
                            'addcases' => $addCases,
                            'remcases' => $remCases,
                            'activecases' => $activeCases,
                            'vattended' => $vAttended,
                            'vscheduled' => $vScheduled,
                            'vcanceled' => $vCanceled,
                            'ucharged' => $uCharged,
                            'dworked' => $dWorked,
                            'hworked' => number_format($hWorked, 2),
                            'hscheduled' => number_format($hScheduled, 2),
                        ),
                        'billing' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'collected' => '$ ' . number_format($collected, 2),
                            'charges' => '$ ' . number_format($charges, 2),
                            'bdays' => $bdays,
                        ),
                        //Unit Ratios
                        'unitratios' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'unitsvisit' => $unitsvisit,
                            'unitsactive' => $unitsactive,
                            'unitshour' => $unitshour,
                            'units8hour' => $units8hour,
                            'chargeunit' => '$ ' . $chargeunit,
                            'collectedunit' => '$ ' . $collectedunit,
                        ),
                        //Visit Ratios
                        'visitratios' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'visitscase' => $visitscase,
                            'visitshour' => $visitshour,
                            'visits8hour' => $visits8hour,
                            'chargesvisit' => '$ ' . $chargesvisit,
                            'collectedvisit' => '$ ' . $collectedvisit,
                            'noshowvisit' => number_format($noshowvisit, 2) . ' %',
                        ),
                        //Case Management
                        'casemanage' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'casesday' => $casesday,
                            'passiverate' => number_format($passiverate, 2) . ' %',
                            'attendrate' => number_format($attendrate, 2) . ' %',
                            'pdisadis' => number_format($pdisadis, 2) . ' %',
                        ),
                        //Financial Ratios
                        'finance' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'chargeshour' => '$ ' . $chargeshour,
                            'collectedhour' => '$ ' . $collectedhour,
                        ),
                        //Staffing Ratios
                        'staff' => array(
                            'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')<br/>' . $locname,
                            'unitsbilling' => $unitsbilling,
                            'visitsbilling' => $visitsbilling,
                            'thourdays' => $thourdays,
                            'scheddensity' => $scheddensity,
                        ),
                        'clinician_list' => $strClinicians,
                    );
                }
            }
        }


        return $usersData;
    }

    public function get_admin_data() {
//get posted dates and location id
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date')));
        $locationId = $this->input->post('location');
        $usersData = array();
//get users under location selected
//foreach($locationids as $locid){
        $userIds = $this->userlocations->get_users_underloc($locationId);

        foreach ($userIds as $id) { //set data values
            $tnewCases = 0;
            $taDis = 0;
            $tpDis = 0;
            $addCases = 0;
            $remCases = 0;
            $activeCases = 0;
            $vAttended = 0;
            $vScheduled = 0;
            $vCanceled = 0;
            $uCharged = 0;
            $dWorked = 0;
            $hWorked = 0;
            $hScheduled = 0;
            $collected = 0;
            $charges = 0;
            $bdays = 0;
            $emp_id = '';
            $fname = '';
            $lname = '';
            $include = false;
//get data per user id passed
            $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $locationId, $sDate, $eDate);

            foreach ($userData as $data) {
//get collection rate per data where date
                $collectionrate = $this->collectionrates->get_collection_rate_by_date($locationId, $data['end_date']);
                if ($data) { //sum all data per user within the dates
                    $emp_id = $data['employee_id'];
                    $fname = $data['first_name'];
                    $lname = $data['last_name'];
                    $tnewCases += $data['new_cases'];
                    $taDis += $data['active_discharges'];
                    $tpDis += $data['passive_discharges'];
                    $addCases += $data['added_cases'];
                    $remCases += $data['removed_cases'];
                    $activeCases = $data['active_cases'];
                    $vAttended += $data['visits_attended'];
                    $vScheduled += $data['visits_scheduled'];
                    $vCanceled += $data['no_show'];
                    $uCharged += $data['units_charged'];
                    $dWorked +=$data['days_worked'];
                    $hWorked += $data['hours_worked'];
                    $hScheduled +=$data['hours_scheduled'];
                    $collected += $data['charges'] * ( $collectionrate / 100);
                    $charges +=$data['charges'];
                    $bdays +=$data['billing_days'];
                    $include = true;
                }
            }
            if ($include) {//set the array to the computed sums per user
//Unit Ratios
                $unitsvisit = $this->divide($uCharged, $vAttended);
                $unitsactive = $this->divide($uCharged, $activeCases);
                $unitshour = $this->divide($uCharged, $hWorked);
                $units8hour = $this->divide($uCharged, ($hWorked / 8));
                $chargeunit = $this->divide($charges, $uCharged);
                $collectedunit = $this->divide($collected, $uCharged);

//Visit Ratios
                $visitscase = $this->divide($vAttended, $activeCases);
                $visitshour = $this->divide($vAttended, $hWorked);
                $visits8hour = $this->divide($vAttended, ($hWorked / 8));
                $chargesvisit = $this->divide($charges, $vAttended);
                $collectedvisit = $this->divide($collected, $vAttended);
                $noshowvisit = $this->divide2($vCanceled, $vAttended);


//Case Management
                $casesday = $this->divide($activeCases, $dWorked);
                $passiverate = $this->divide2($tpDis, ($taDis + $tpDis));
                $attendrate = $this->divide2($vAttended, $vScheduled);
                $pdisadis = $this->divide2($tpDis, $taDis);

//Financial Ratios
                $chargeshour = $this->divide($charges, $hWorked);
                $collectedhour = $this->divide($collected, $hWorked);

//Staffing Ratios
                $unitsbilling = $this->divide($uCharged, $bdays);
                $visitsbilling = $this->divide($vAttended, $bdays);
                $thourdays = $this->divide($hWorked, $dWorked);
                $scheddensity = $this->divide($hScheduled, $hWorked);


                $usersData[] = array(
//Volume  
                    'volume' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'newcases' => $tnewCases,
                        'adis' => $taDis,
                        'pdis' => $tpDis,
                        'addcases' => $addCases,
                        'remcases' => $remCases,
                        'activecases' => $activeCases,
                        'vattended' => $vAttended,
                        'vscheduled' => $vScheduled,
                        'vcanceled' => $vCanceled,
                        'ucharged' => $uCharged,
                        'dworked' => $dWorked,
                        'hworked' => number_format($hWorked, 2),
                        'hscheduled' => number_format($hScheduled, 2),
                    ),
                    'billing' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'collected' => '$ ' . number_format($collected, 2),
                        'charges' => '$ ' . number_format($charges, 2),
                        'bdays' => $bdays,
                    ),
                    //Unit Ratios
                    'unitratios' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'unitsvisit' => $unitsvisit,
                        'unitsactive' => $unitsactive,
                        'unitshour' => $unitshour,
                        'units8hour' => $units8hour,
                        'chargeunit' => '$ ' . $chargeunit,
                        'collectedunit' => '$ ' . $collectedunit,
                    ),
                    //Visit Ratios
                    'visitratios' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'visitscase' => $visitscase,
                        'visitshour' => $visitshour,
                        'visits8hour' => $visits8hour,
                        'chargesvisit' => '$ ' . $chargesvisit,
                        'collectedvisit' => '$ ' . $collectedvisit,
                        'noshowvisit' => number_format($noshowvisit, 2) . ' %',
                    ),
                    //Case Management
                    'casemanage' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'casesday' => $casesday,
                        'passiverate' => number_format($passiverate, 2) . ' %',
                        'attendrate' => number_format($attendrate, 2) . ' %',
                        'pdisadis' => number_format($pdisadis, 2) . ' %',
                    ),
                    //Financial Ratios
                    'finance' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'chargeshour' => '$ ' . $chargeshour,
                        'collectedhour' => '$ ' . $collectedhour,
                    ),
                    //Staffing Ratios
                    'staff' => array(
                        'emp' => strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')',
                        'unitsbilling' => $unitsbilling,
                        'visitsbilling' => $visitsbilling,
                        'thourdays' => $thourdays,
                        'scheddensity' => $scheddensity,
                    ),
                );
            }
//$perloc []= array(
//'location_name'=>location name
//'loc_data'=>$usersData
//);
//} per location 
//foreach perloc = locdata
//foreach locdata['volume'] = volumedata
//
//
//
        }
        return $usersData;
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    function divide($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format($dividend / $divisor, 2);
            return $quotient;
        }
    }

    function divide2($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor) * 100, 2);
            return $quotient;
        }
    }

    public function load_names() {
        $user_id = $this->session->userdata('user_id');
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date')));

        if ($this->in_array_r('Practice Owner', $this->session->userdata('roles'))) {
            $practice_id = $this->userpractices->get_practice($user_id);
            $locations = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);
        } else {
            $locations = $this->session->userdata('locs');
        }

        $strClinicians = '<input type="hidden" name="location-id" value="0"/>';
        foreach ($locations as $location) {

            $clinician_names = $this->locations->get_clinician_list($location['location_id'], $start_date, $end_date);

            if ($clinician_names) {
                $strClinicians .= '<span>' . $location['location_name'] . '</span>';

                foreach ($clinician_names as $clini_names) {
                    $strClinicians .= '<label class="checkbox">';
                    $strClinicians .= '<input type="checkbox" value="' . $clini_names['id'] . '" name="userid[' . $clini_names['id'] . '][' . $location['location_id'] . ']" checked="checked">';
                    $strClinicians .= strtoupper($clini_names['last_name']) . ', ' . $clini_names['first_name'] . ' (' . $clini_names['employee_id'] . ')';
                    $strClinicians .= '</label>';
                }
                $strClinicians .= '<br/>';
            }
        }
        return $strClinicians;
    }

    public function load_clinician_names() {
        $location_id = $this->input->post('location');
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date')));
        $user_id = $this->session->userdata('user_id');
        $practice_id = $this->userpractices->get_practice($user_id);
        $location = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);


        $clinician_names = $this->locations->get_clinician_list($location_id, $start_date, $end_date);
        $strClinicians = '';
        $strClinicians = '<input type="hidden" name="location-id" value="1"/>';
        foreach ($clinician_names as $clini_names) {


            $strClinicians .= '<label class="checkbox">';
            $strClinicians .= '<input type="checkbox" value="' . $clini_names['id'] . '" name="userid[]" checked="checked">';
            $strClinicians .= strtoupper($clini_names['last_name']) . ', ' . $clini_names['first_name'] . ' (' . $clini_names['employee_id'] . ')';
            $strClinicians .= '</label>';
        }

        return $strClinicians;
    }

    public function set_clinician_list($usersData) {
        $strClinicians = '';
        foreach ($usersData as $id) {
            $strClinicians .= '<label class="checkbox">';
            $strClinicians .= '<input type="checkbox" value="' . $id['id'] . '" name="userid[]" checked="checked">';
            $strClinicians .= $id['name'];
            $strClinicians .= '</label>';
        }

        return $strClinicians;
    }

    public function generate_individual() {
        if (!$this->input->post('location-id')) {
            $this->generate_individual_all();
        } else {
            include_once APPPATH . '/third_party/mpdf/mpdf.php';
            $user_id = $this->input->post('userid');
            $location_id = $this->input->post('location');
            $start_date = $this->input->post('start-date-admin');
            $end_date = $this->input->post('end-date-admin');
            $location_data = $this->locations->get_location_by_id($location_id);
            $practice_name = $this->practices->get_practice_by_id($location_data[0]['practice_id']);

            $user_arr = array();
            foreach ($user_id as $key => $users) {
                $user_arr[$key]['user_id'] = $users;
            }
//        $res = $this->get_all_clinician_agg_data($user_arr, $start_date, $end_date, $location_id, true);
            $location_name = $this->locations->get_location_by_id($location_id);
            ini_set('memory_limit', '64M');

            $mpdf = new mPDF('utf-8', //mode
                    'A4', //format
                    9, //fontsize
                    '', //fontfamily
                    15, //margin_left
                    15, //margin_right
                    16, //margin_top
                    16, //margin_bottom
                    9, //margin_header
                    9, //margin_footer
                    'L'//orientation
            );
            $mpdf->SetHeader($practice_name[0]['practice_name'] . " : " . "Clinicians Daily Report" . "||" . "Inclusive Dates: " . date('m/d/Y', strtotime($start_date)) . " to " . date('m/d/Y', strtotime($end_date)));
            $mpdf->SetFooter("Report Generated on " . date(DATE_RFC822) . '||Page {PAGENO}');
            $page_ctr = 0;
            $style = '<style>
                    table,td,th{
                        border: 1px solid;
                        border-collapse: collapse;
                    }
                    
                    td{
                        text-align: right;
                        
                    }
                    .location{
                        text-align:right;
                    }
                    .name{
                        text-align:left;
                        float:left;
                        width:300px;
                       
                    }
                  </style>';
            $mpdf->WriteHTML($style);

//        $mpdf->WriteHTML('<h6>Location: ' . $location_data[0]['location_name'] . '</h6>', 2);

            $res = $this->get_data_individual($user_arr, $start_date, $end_date, $location_id);
            $user_data = array();
            $ctr = 0;
            foreach ($user_id as $key => $users) {
                $user_data = $this->users->get_user_by_id($users);
                $user_id = $user_data[0]['id'];
                $str = '<h6 class="name">' . $user_data[0]['last_name'] . ', ' . $user_data[0]['first_name'] . ' (' . $user_data[0]['employee_id'] . ')</h6>';
                $str .= '<h6 class="location">Location: ' . $location_name[0]['location_name'] . '</h6>';
                $str .= '<table width="100%">';
                $str .= '<tr>'
                        . '<th colspan="14">Volume</th>'
                        . '</tr>';
                $str .= '<tr>'
                        . '<th>Date</th>'
                        . '<th>New<br>Cases</th>'
                        . '<th>Active<br>Discharges</th>'
                        . '<th>Passive<br>Discharges</th>'
                        . '<th>Active<br>Cases</th>'
                        . '<th>Visits<br>Attended</th>'
                        . '<th>Visit<br>Cancels<br>& No-<br>Shows</th>'
                        . '<th>Visits<br>Scheduled</th>'
                        . '<th>Units<br>Charged</th>'
                        . '<th>Hours<br>Worked</th>'
                        . '<th>Clinical<br>Hours<br>Scheduled</th>'
                        . '<th>Charges</th>'
                        . '<th>Estimated<br>Collections</th>'
                        . '<th>Collection<br>Rate</th>'
                        . '</tr>';
                $new_cases_total = 0;
                $active_discharges_total = 0;
                $passive_discharges_total = 0;
                $active_cases_total = 0;
                $visits_attended_total = 0;
                $no_show_total = 0;
                $visits_scheduled_total = 0;
                $units_charged_total = 0;
                $hours_worked_total = 0;
                $hours_scheduled_total = 0;
                $charges_total = 0;
                $collected_total = 0;
                $t_billingdays = 0;
                $noshowvisit = 0;

                foreach ($res[$user_id] as $data) {
                    $str .= '<tr>'
                            . '<td  style="text-align: center;" width="11.5%">' . date('m/d/Y', strtotime($data['start_date'])) . '</td>'
                            . '<td width="6.9%">' . $data['new_cases'] . '</td>'
                            . '<td width="6.9%">' . $data['active_discharges'] . '</td>'
                            . '<td width="6.9%">' . $data['passive_discharges'] . '</td>'
                            . '<td width="6.9%">' . $data['active_cases'] . '</td>'
                            . '<td width="6.9%">' . $data['visits_attended'] . '</td>'
                            . '<td width="6.9%">' . $data['no_show'] . '</td>'
                            . '<td width="6.9%">' . $data['visits_scheduled'] . '</td>'
                            . '<td width="6.9%">' . $data['units_charged'] . '</td>'
                            . '<td width="6.9%">' . number_format($data['hours_worked'], 2) . '</td>'
                            . '<td width="6.9%">' . number_format($data['hours_scheduled'], 2) . '</td>'
                            . '<td width="6.9%">$ ' . number_format($data['charges'], 2) . '</td>'
                            . '<td width="6.9%">$ ' . number_format($data['collected'], 2) . '</td>'
                            . '<td width="6.9%">' . $data['collectionrate'] . ' %</td>'
                            . '</tr>';

                    $new_cases_total += $data['new_cases'];
                    $active_discharges_total += $data['active_discharges'];
                    $passive_discharges_total += $data['passive_discharges'];
                    $active_cases_total = $data['active_cases'];
                    $visits_attended_total += $data['visits_attended'];
                    $no_show_total += $data['no_show'];
                    $visits_scheduled_total += $data['visits_scheduled'];
                    $units_charged_total += $data['units_charged'];
                    $hours_worked_total += $data['hours_worked'];
                    $hours_scheduled_total += $data['hours_scheduled'];
                    $charges_total += $data['charges'];
                    $collected_total += $data['collected'];
                    $t_billingdays += intval($data['billing_days']);
                    $noshowvisit = $this->divide2($data['no_show'], $data['visits_attended']);
                    $pdcadc = $this->divide2($data['passive_discharges'], $data['active_discharges']);
                }
                $str .= '<tr>'
                        . '<td  style="text-align: center;" width="11.5%"><strong>Total</strong></td>'
                        . '<td width="6.9%">' . $new_cases_total . '</td>'
                        . '<td width="6.9%">' . $active_discharges_total . '</td>'
                        . '<td width="6.9%">' . $passive_discharges_total . '</td>'
                        . '<td width="6.9%">' . $active_cases_total . '</td>'
                        . '<td width="6.9%">' . $visits_attended_total . '</td>'
                        . '<td width="6.9%">' . $no_show_total . '</td>'
                        . '<td width="6.9%">' . $visits_scheduled_total . '</td>'
                        . '<td width="6.9%">' . $units_charged_total . '</td>'
                        . '<td width="6.9%">' . number_format($hours_worked_total, 2) . '</td>'
                        . '<td width="6.9%">' . number_format($hours_scheduled_total, 2) . '</td>'
                        . '<td width="6.9%">$ ' . number_format($charges_total, 2) . '</td>'
                        . '<td width="6.9%">$ ' . number_format($collected_total, 2) . '</td>'
                        . '</tr>';

                $str .= '</table>';

                $str .= '<h6>Total Billing Days: ' . $t_billingdays . '</h6> <br/>';

                $str .= '<table width="100%" style="font-size: 11px;">';
                $str .= '<tr>'
                        . '<th></th>'
                        . '<th colspan="6">Units Ratios</th>'
                        . '<th colspan="6">Visits Ratios</th>'
                        . '</tr>';
                $str .= '<tr>'
                        . '<th>Date</th>'
                        . '<th>Units/<br>Visit Attended</th>'
                        . '<th>Units/<br>Active<br>Case</th>'
                        . '<th>Units/<br>Hours<br>Worked</th>'
                        . '<th>Units/8<br>Hours<br>Worked</th>'
                        . '<th>Charge/<br>Unit</th>'
                        . '<th>Collections<br>/Unit</th>'
                        . '<th>Visits/<br>Case</th>'
                        . '<th>Visits/<br>Hour<br>Worked</th>'
                        . '<th>Visits/8<br>Hour<br>Worked</th>'
                        . '<th>Charges<br>/Visit Attended</th>'
                        . '<th>Collections<br>/Visit Attended</th>'
                        . '<th>Visits Canceled<br>/Visit Attended</th>'
                        . '</tr>';

                foreach ($res[$user_id] as $data) {
                    $str .= '<tr>'
                            . '<td  style="text-align: center;" width="10%">' . date('m/d/Y', strtotime($data['start_date'])) . '</td>'
                            . '<td width="8.2%">' . $data['unitsvisit'] . '</td>'
                            . '<td width="8.2%">' . $data['unitsactive'] . '</td>'
                            . '<td width="8.2%">' . $data['unitshour'] . '</td>'
                            . '<td width="8.2%">' . $data['units8hour'] . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['chargeunit'], 2) . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['collectedunit'], 2) . '</td>'
                            . '<td width="8.2%">' . $data['visitscase'] . '</td>'
                            . '<td width="8.2%">' . number_format($data['visitshour'], 2) . '</td>'
                            . '<td width="8.2%">' . number_format($data['visits8hour'], 2) . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['chargesvisit'], 2) . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['collectedvisit'], 2) . '</td>'
                            . '<td width="8.2%">' . number_format($noshowvisit, 2) . ' %</td>'
                            . '</tr>';

                    $unitsvisit_total = $this->divide($units_charged_total, $visits_attended_total);
                    $unitsactive_total = $this->divide($units_charged_total, $active_cases_total);
                    $unitshour_total = $this->divide($units_charged_total, $hours_worked_total);
                    $units8hour_total = $this->divide($units_charged_total, ($hours_worked_total / 8));
                    $chargeunit_total = $this->divide($charges_total, $units_charged_total);
                    $collectedunit_total = $this->divide($collected_total, $units_charged_total);
                    $visitscase_total = $this->divide($visits_attended_total, $active_cases_total);
                    $visitshour_total = $this->divide($visits_attended_total, $hours_worked_total);
                    $uvisits8hour_total = $this->divide($visits_attended_total, ($hours_worked_total / 8));
                    $chargesvisit_total = $this->divide($charges_total, $visits_attended_total);
                    $collectedvisit_total = $this->divide($collected_total, $visits_attended_total);
                    $noshowvisit_total = $this->divide2($no_show_total, $visits_attended_total);
                    $pdcadc_total = $this->divide2($passive_discharges_total, $active_discharges_total);
                }
                $str .= '<tr>'
                        . '<td  style="text-align: center;" width="10%"><strong>Average</strong></td>'
                        . '<td width="8.2%">' . $unitsvisit_total . '</td>'
                        . '<td width="8.2%">' . $unitsactive_total . '</td>'
                        . '<td width="8.2%">' . $unitshour_total . '</td>'
                        . '<td width="8.2%">' . $units8hour_total . '</td>'
                        . '<td width="8.2%">$ ' . number_format($chargeunit_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($collectedunit_total, 2) . '</td>'
                        . '<td width="8.2%">' . $visitscase_total . '</td>'
                        . '<td width="8.2%">' . number_format($visitshour_total, 2) . '</td>'
                        . '<td width="8.2%">' . number_format($uvisits8hour_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($chargesvisit_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($collectedvisit_total, 2) . '</td>'
                        . '<td width="8.2%">' . number_format($noshowvisit_total, 2) . '%</td>'
                        . '</tr>';

                $str .= '</table><br/><br/>';



                $str .= '<table width="100%" style="font-size: 11px;">';
                $str .= '<tr>'
                        . '<th></th>'
                        . '<th colspan="4">Case Management Ratios</th>'
                        . '<th colspan="2">Financial Ratios</th>'
                        . '<th colspan="4">Staffing Ratios</th>'
                        . '</tr>';
                $str .= '<tr>'
                        . '<th>Date</th>'
                        . '<th>Active Cases/<br>Day</th>'
                        . '<th>Passive<br>Discharge Rate</th>'
                        . '<th>Attendance Rate</th>'
                        . '<th>Passive D/C / Active D/C</th>'
                        . '<th>Charge /<br>Hour<br>Worked</th>'
                        . '<th>Collections  <br>/Hour<br>Worked</th>'
                        . '<th>Units /<br>Billing<br>Day</th>'
                        . '<th>Visits /<br>Billing <br>Day</th>'
                        . '<th>Total Hours Worked / Days worked</th>'
                        . '<th>Schedule<br>Density</th>'
                        . '</tr>';
                $ctr1 = 0;

                $days_worked_total = 0;
                $caseday_total = 0;
                $passiverate_total = 0;
                $attendrate_total = 0;
                $chargeshour_total = 0;
                $collectedhour_total = 0;
                $unitsbilling_total = 0;
                $visitsbilling_total = 0;
                $thourdays_total = 0;
                $scheddensity_total = 0;
                foreach ($res[$user_id] as $data) {
                    $str .= '<tr>'
                            . '<td style="text-align: center;" width="10%">' . date('m/d/Y', strtotime($data['start_date'])) . '</td>'
                            . '<td width="10%">' . $data['casesday'] . '</td>'
                            . '<td width="10%">' . $data['passiverate'] * 100 . ' %</td>'
                            . '<td width="10%">' . $data['attendrate'] * 100 . ' %</td>'
                            . '<td width="10%">' . $pdcadc . ' %</td>'
                            . '<td width="10%">$ ' . number_format($data['chargeshour'], 2) . '</td>'
                            . '<td width="10%">$ ' . number_format($data['collectedhour'], 2) . '</td>'
                            . '<td width="10%">' . $data['unitsbilling'] . '</td>'
                            . '<td width="10%">' . $data['visitsbilling'] . '</td>'
                            . '<td width="10%">' . $data['thourdays'] . '</td>'
                            . '<td width="10%">' . $data['scheddensity'] . '</td>'
                            . '</tr width="10%">';

                    $days_worked_total += $data['days_worked'];
                    $caseday_total = $this->divide($active_cases_total, $days_worked_total);
                    $passiverate_total = $this->divide2($passive_discharges_total, ($active_discharges_total + $passive_discharges_total));
                    $attendrate_total = $this->divide2($visits_attended_total, $visits_scheduled_total);
                    $chargeshour_total = $this->divide($charges_total, $hours_worked_total);
                    $collectedhour_total = $this->divide($collected_total, $hours_worked_total);
                    $unitsbilling_total = $this->divide($units_charged_total, $t_billingdays);
                    $visitsbilling_total = $this->divide($visits_attended_total, $t_billingdays);
                    $thourdays_total = $this->divide($hours_worked_total, $days_worked_total);
                    $scheddensity_total = $this->divide($hours_scheduled_total, $hours_worked_total);


                    $ctr1++;
                }
                $str .= '<tr>'
                        . '<td  style="text-align: center;" width="10%"><strong>Average</strong></td>'
                        . '<td width="8.2%">' . $caseday_total . '</td>'
                        . '<td width="8.2%">' . number_format($passiverate_total, 2) . ' %</td>'
                        . '<td width="8.2%">' . number_format($attendrate_total, 2) . ' %</td>'
                        . '<td width="8.2%">' . $pdcadc_total . ' %</td>'
                        . '<td width="8.2%">$ ' . number_format($chargeshour_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($collectedhour_total, 2) . '</td>'
                        . '<td width="8.2%">' . $unitsbilling_total . '</td>'
                        . '<td width="8.2%">' . $visitsbilling_total . '</td>'
                        . '<td width="8.2%">' . number_format($thourdays_total, 2) . '</td>'
                        . '<td width="8.2%">' . $scheddensity_total . '</td>'
                        . '</tr>';

                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);
                $mpdf->AddPage();
            }

            $count = count($mpdf->pages);
            $mpdf->DeletePages($count, $count);
            $mpdf->Output('Clinicians Daily Report.pdf', 'I');
        }
    }

    public function generate_individual_all() {
        include_once APPPATH . '/third_party/mpdf/mpdf.php';
        $user_id = $this->input->post('userid');
        $location_id = $this->input->post('location');
        $start_date = $this->input->post('start-date-admin');
        $end_date = $this->input->post('end-date-admin');
        $usersData = array();
        $daily = array();
        $users = $this->input->post('userid');
        $res = $this->get_data_all_individual($users, $start_date, $end_date, $location_id);

        ini_set('memory_limit', '64M');

        $mpdf = new mPDF('utf-8', //mode
                'A4', //format
                9, //fontsize
                '', //fontfamily
                15, //margin_left
                15, //margin_right
                16, //margin_top
                16, //margin_bottom
                9, //margin_header
                9, //margin_footer
                'L'//orientation
        );
        $mpdf->SetHeader("Clinicians Daily Report" . "||" . "Inclusive Dates: " . date('m/d/Y', strtotime($start_date)) . " to " . date('m/d/Y', strtotime($end_date)));
        $mpdf->SetFooter("Report Generated on " . date(DATE_RFC822) . '||Page {PAGENO}');

        $style = '<style>
                    table,td,th{
                        border: 1px solid;
                        border-collapse: collapse;
                    }
                    
                    td{
                        text-align: right;
                        
                    }
                    .location{
                        text-align:right;
                    }
                    .name{
                        text-align:left;
                        float:left;
                        width:300px;
                       
                    }
                  </style>';
        $mpdf->WriteHTML($style);



        foreach ($users as $userID => $individual) {
            $username = $this->users->get_user_by_id($userID);


            $str = '<h6> Name: ' . $username[0]['first_name'] . ' ' . $username[0]['last_name'] . '</h6>';
            foreach ($individual as $locID => $uid) {

                $location_name = $this->locations->get_location_by_id($locID);
                $str .= '<h6>Location: ' . $location_name[0]['location_name'] . '</h6>';

                $str .= '<table width="100%">';
                $str .= '<tr>'
                        . '<th colspan="14">Volume</th>'
                        . '</tr>';
                $str .= '<tr>'
                        . '<th>Date</th>'
                        . '<th>New<br>Cases</th>'
                        . '<th>Active<br>Discharges</th>'
                        . '<th>Passive<br>Discharges</th>'
                        . '<th>Active<br>Cases</th>'
                        . '<th>Visits<br>Attended</th>'
                        . '<th>Visit<br>Cancels<br>& No-<br>Shows</th>'
                        . '<th>Visits<br>Scheduled</th>'
                        . '<th>Units<br>Charged</th>'
                        . '<th>Hours<br>Worked</th>'
                        . '<th>Clinical<br>Hours<br>Scheduled</th>'
                        . '<th>Charges</th>'
                        . '<th>Estimated<br>Collections</th>'
                        . '<th>Collection<br>Rate</th>'
                        . '</tr>';
                $new_cases_total = 0;
                $active_discharges_total = 0;
                $passive_discharges_total = 0;
                $active_cases_total = 0;
                $visits_attended_total = 0;
                $no_show_total = 0;
                $visits_scheduled_total = 0;
                $units_charged_total = 0;
                $hours_worked_total = 0;
                $hours_scheduled_total = 0;
                $charges_total = 0;
                $collected_total = 0;
                $t_billingdays = 0;
                $noshowvisit = 0;

                foreach ($res[$userID][$locID] as $key => $data) {
                    $str .= '<tr>'
                            . '<td  style="text-align: center;" width="11.5%">' . date('m/d/Y', strtotime($data['start_date'])) . '</td>'
                            . '<td width="6.9%">' . $data['new_cases'] . '</td>'
                            . '<td width="6.9%">' . $data['active_discharges'] . '</td>'
                            . '<td width="6.9%">' . $data['passive_discharges'] . '</td>'
                            . '<td width="6.9%">' . $data['active_cases'] . '</td>'
                            . '<td width="6.9%">' . $data['visits_attended'] . '</td>'
                            . '<td width="6.9%">' . $data['no_show'] . '</td>'
                            . '<td width="6.9%">' . $data['visits_scheduled'] . '</td>'
                            . '<td width="6.9%">' . $data['units_charged'] . '</td>'
                            . '<td width="6.9%">' . number_format($data['hours_worked'], 2) . '</td>'
                            . '<td width="6.9%">' . number_format($data['hours_scheduled'], 2) . '</td>'
                            . '<td width="6.9%">$ ' . number_format($data['charges'], 2) . '</td>'
                            . '<td width="6.9%">$ ' . number_format($data['collected'], 2) . '</td>'
                            . '<td width="6.9%">' . $data['collectionrate'] . ' %</td>'
                            . '</tr>';
                    $new_cases_total += $data['new_cases'];
                    $active_discharges_total += $data['active_discharges'];
                    $passive_discharges_total += $data['passive_discharges'];
                    $active_cases_total = $data['active_cases'];
                    $visits_attended_total += $data['visits_attended'];
                    $no_show_total += $data['no_show'];
                    $visits_scheduled_total += $data['visits_scheduled'];
                    $units_charged_total += $data['units_charged'];
                    $hours_worked_total += $data['hours_worked'];
                    $hours_scheduled_total += $data['hours_scheduled'];
                    $charges_total += $data['charges'];
                    $collected_total += $data['collected'];
                    $t_billingdays += intval($data['billing_days']);
                    $noshowvisit = $this->divide2($data['no_show'], $data['visits_attended']);
                    $pdcadc = $this->divide2($data['passive_discharges'], $data['active_discharges']);
                }
                $str .= '<tr>'
                        . '<td  style="text-align: center;" width="11.5%"><strong>Total</strong></td>'
                        . '<td width="6.9%">' . $new_cases_total . '</td>'
                        . '<td width="6.9%">' . $active_discharges_total . '</td>'
                        . '<td width="6.9%">' . $passive_discharges_total . '</td>'
                        . '<td width="6.9%">' . $active_cases_total . '</td>'
                        . '<td width="6.9%">' . $visits_attended_total . '</td>'
                        . '<td width="6.9%">' . $no_show_total . '</td>'
                        . '<td width="6.9%">' . $visits_scheduled_total . '</td>'
                        . '<td width="6.9%">' . $units_charged_total . '</td>'
                        . '<td width="6.9%">' . number_format($hours_worked_total, 2) . '</td>'
                        . '<td width="6.9%">' . number_format($hours_scheduled_total, 2) . '</td>'
                        . '<td width="6.9%">$ ' . number_format($charges_total, 2) . '</td>'
                        . '<td width="6.9%">$ ' . number_format($collected_total, 2) . '</td>'
                        . '</tr>';

                $str .= '</table>';

                $str .= '<h6>Total Billing Days: ' . $t_billingdays . '</h6> <br/>';

                $str .= '<table width="100%" style="font-size: 11px;">';
                $str .= '<tr>'
                        . '<th></th>'
                        . '<th colspan="6">Units Ratios</th>'
                        . '<th colspan="6">Visits Ratios</th>'
                        . '</tr>';
                $str .= '<tr>'
                        . '<th>Date</th>'
                        . '<th>Units/<br>Visit Attended</th>'
                        . '<th>Units/<br>Active<br>Case</th>'
                        . '<th>Units/<br>Hours<br>Worked</th>'
                        . '<th>Units/8<br>Hours<br>Worked</th>'
                        . '<th>Charge/<br>Unit</th>'
                        . '<th>Collections<br>/Unit</th>'
                        . '<th>Visits/<br>Case</th>'
                        . '<th>Visits/<br>Hour<br>Worked</th>'
                        . '<th>Visits/8<br>Hour<br>Worked</th>'
                        . '<th>Charges<br>/Visit Attended</th>'
                        . '<th>Collections<br>/Visit Attended</th>'
                        . '<th>Visits Canceled<br>/Visit Attended</th>'
                        . '</tr>';

                foreach ($res[$userID][$locID] as $key => $data) {
                    $str .= '<tr>'
                            . '<td  style="text-align: center;" width="10%">' . date('m/d/Y', strtotime($data['start_date'])) . '</td>'
                            . '<td width="8.2%">' . $data['unitsvisit'] . '</td>'
                            . '<td width="8.2%">' . $data['unitsactive'] . '</td>'
                            . '<td width="8.2%">' . $data['unitshour'] . '</td>'
                            . '<td width="8.2%">' . $data['units8hour'] . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['chargeunit'], 2) . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['collectedunit'], 2) . '</td>'
                            . '<td width="8.2%">' . $data['visitscase'] . '</td>'
                            . '<td width="8.2%">' . number_format($data['visitshour'], 2) . '</td>'
                            . '<td width="8.2%">' . number_format($data['visits8hour'], 2) . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['chargesvisit'], 2) . '</td>'
                            . '<td width="8.2%">$ ' . number_format($data['collectedvisit'], 2) . '</td>'
                            . '<td width="8.2%">' . number_format($noshowvisit, 2) . ' %</td>'
                            . '</tr>';

                    $unitsvisit_total = $this->divide($units_charged_total, $visits_attended_total);
                    $unitsactive_total = $this->divide($units_charged_total, $active_cases_total);
                    $unitshour_total = $this->divide($units_charged_total, $hours_worked_total);
                    $units8hour_total = $this->divide($units_charged_total, ($hours_worked_total / 8));
                    $chargeunit_total = $this->divide($charges_total, $units_charged_total);
                    $collectedunit_total = $this->divide($collected_total, $units_charged_total);
                    $visitscase_total = $this->divide($visits_attended_total, $active_cases_total);
                    $visitshour_total = $this->divide($visits_attended_total, $hours_worked_total);
                    $uvisits8hour_total = $this->divide($visits_attended_total, ($hours_worked_total / 8));
                    $chargesvisit_total = $this->divide($charges_total, $visits_attended_total);
                    $collectedvisit_total = $this->divide($collected_total, $visits_attended_total);
                    $noshowvisit_total = $this->divide2($no_show_total, $visits_attended_total);
                    $pdcadc_total = $this->divide2($passive_discharges_total, $active_discharges_total);
                }
                $str .= '<tr>'
                        . '<td  style="text-align: center;" width="10%"><strong>Average</strong></td>'
                        . '<td width="8.2%">' . $unitsvisit_total . '</td>'
                        . '<td width="8.2%">' . $unitsactive_total . '</td>'
                        . '<td width="8.2%">' . $unitshour_total . '</td>'
                        . '<td width="8.2%">' . $units8hour_total . '</td>'
                        . '<td width="8.2%">$ ' . number_format($chargeunit_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($collectedunit_total, 2) . '</td>'
                        . '<td width="8.2%">' . $visitscase_total . '</td>'
                        . '<td width="8.2%">' . number_format($visitshour_total, 2) . '</td>'
                        . '<td width="8.2%">' . number_format($uvisits8hour_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($chargesvisit_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($collectedvisit_total, 2) . '</td>'
                        . '<td width="8.2%">' . number_format($noshowvisit_total, 2) . '%</td>'
                        . '</tr>';

                $str .= '</table><br/><br/>';


                $str .= '<table width="100%" style="font-size: 11px;">';
                $str .= '<tr>'
                        . '<th></th>'
                        . '<th colspan="4">Case Management Ratios</th>'
                        . '<th colspan="2">Financial Ratios</th>'
                        . '<th colspan="4">Staffing Ratios</th>'
                        . '</tr>';
                $str .= '<tr>'
                        . '<th>Date</th>'
                        . '<th>Active Cases/<br>Day</th>'
                        . '<th>Passive<br>Discharge Rate</th>'
                        . '<th>Attendance Rate</th>'
                        . '<th>Passive D/C / Active D/C</th>'
                        . '<th>Charge /<br>Hour<br>Worked</th>'
                        . '<th>Collections  <br>/Hour<br>Worked</th>'
                        . '<th>Units /<br>Billing<br>Day</th>'
                        . '<th>Visits /<br>Billing <br>Day</th>'
                        . '<th>Total Hours Worked / Days worked</th>'
                        . '<th>Schedule<br>Density</th>'
                        . '</tr>';
                $ctr1 = 0;

                $days_worked_total = 0;
                $caseday_total = 0;
                $passiverate_total = 0;
                $attendrate_total = 0;
                $chargeshour_total = 0;
                $collectedhour_total = 0;
                $unitsbilling_total = 0;
                $visitsbilling_total = 0;
                $thourdays_total = 0;
                $scheddensity_total = 0;
                foreach ($res[$userID][$locID] as $key => $data) {
                    $str .= '<tr>'
                            . '<td style="text-align: center;" width="10%">' . date('m/d/Y', strtotime($data['start_date'])) . '</td>'
                            . '<td width="10%">' . $data['casesday'] . '</td>'
                            . '<td width="10%">' . $data['passiverate'] * 100 . ' %</td>'
                            . '<td width="10%">' . $data['attendrate'] * 100 . ' %</td>'
                            . '<td width="10%">' . $pdcadc . ' %</td>'
                            . '<td width="10%">$ ' . number_format($data['chargeshour'], 2) . '</td>'
                            . '<td width="10%">$ ' . number_format($data['collectedhour'], 2) . '</td>'
                            . '<td width="10%">' . $data['unitsbilling'] . '</td>'
                            . '<td width="10%">' . $data['visitsbilling'] . '</td>'
                            . '<td width="10%">' . $data['thourdays'] . '</td>'
                            . '<td width="10%">' . $data['scheddensity'] . '</td>'
                            . '</tr width="10%">';

                    $days_worked_total += $data['days_worked'];
                    $caseday_total = $this->divide($active_cases_total, $days_worked_total);
                    $passiverate_total = $this->divide2($passive_discharges_total, ($active_discharges_total + $passive_discharges_total));
                    $attendrate_total = $this->divide2($visits_attended_total, $visits_scheduled_total);
                    $chargeshour_total = $this->divide($charges_total, $hours_worked_total);
                    $collectedhour_total = $this->divide($collected_total, $hours_worked_total);
                    $unitsbilling_total = $this->divide($units_charged_total, $t_billingdays);
                    $visitsbilling_total = $this->divide($visits_attended_total, $t_billingdays);
                    $thourdays_total = $this->divide($hours_worked_total, $days_worked_total);
                    $scheddensity_total = $this->divide($hours_scheduled_total, $hours_worked_total);


                    $ctr1++;
                }
                $str .= '<tr>'
                        . '<td  style="text-align: center;" width="10%"><strong>Average</strong></td>'
                        . '<td width="8.2%">' . $caseday_total . '</td>'
                        . '<td width="8.2%">' . number_format($passiverate_total, 2) . ' %</td>'
                        . '<td width="8.2%">' . number_format($attendrate_total, 2) . ' %</td>'
                        . '<td width="8.2%">' . $pdcadc_total . ' %</td>'
                        . '<td width="8.2%">$ ' . number_format($chargeshour_total, 2) . '</td>'
                        . '<td width="8.2%">$ ' . number_format($collectedhour_total, 2) . '</td>'
                        . '<td width="8.2%">' . $unitsbilling_total . '</td>'
                        . '<td width="8.2%">' . $visitsbilling_total . '</td>'
                        . '<td width="8.2%">' . number_format($thourdays_total, 2) . '</td>'
                        . '<td width="8.2%">' . $scheddensity_total . '</td>'
                        . '</tr>';

                $str .= '</table><br/><br/>';
            }
            $mpdf->WriteHTML($str, 2);
            $mpdf->AddPage();
        }

        $count = count($mpdf->pages);
        $mpdf->DeletePages($count, $count);
        $mpdf->Output('Clinicians Daily Report.pdf', 'I');
    }

    public function get_data_all_individual($user_array, $start_date, $end_date, $location_id) {
        $response = array();
        $res_data = array();
        foreach ($user_array as $userID => $individual) {
//            $response[$userID] = $this->cliniciandata->get_clinician_data_by_date($uid, $locID, date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));

            foreach ($individual as $locID => $uid) {
                $response[$uid][$locID] = $this->cliniciandata->get_clinician_data_by_date($uid, $locID, date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));
                foreach ($response[$uid][$locID] as $key => $new_data) {
                    $collectionrate = $this->collectionrates->get_collection_rate_by_date($locID, $new_data['end_date']);
                    $res_data[$uid][$locID][$key]['start_date'] = $new_data['start_date'];
                    $res_data[$uid][$locID][$key]['new_cases'] = $new_data['new_cases'];
                    $res_data[$uid][$locID][$key]['active_discharges'] = $new_data['active_discharges'];
                    $res_data[$uid][$locID][$key]['passive_discharges'] = $new_data['passive_discharges'];
                    $res_data[$uid][$locID][$key]['active_cases'] = $new_data['active_cases'];
                    $res_data[$uid][$locID][$key]['visits_attended'] = $new_data['visits_attended'];
                    $res_data[$uid][$locID][$key]['visits_scheduled'] = $new_data['visits_scheduled'];
                    $res_data[$uid][$locID][$key]['no_show'] = $new_data['no_show'];
                    $res_data[$uid][$locID][$key]['units_charged'] = $new_data['units_charged'];
                    $res_data[$uid][$locID][$key]['days_worked'] = $new_data['days_worked'];
                    $res_data[$uid][$locID][$key]['hours_worked'] = $new_data['hours_worked'];
                    $res_data[$uid][$locID][$key]['hours_scheduled'] = $new_data['hours_scheduled'];
                    $res_data[$uid][$locID][$key]['collected'] = $new_data['charges'] * ( $collectionrate / 100);
                    $res_data[$uid][$locID][$key]['charges'] = $new_data['charges'];
                    $res_data[$uid][$locID][$key]['billing_days'] = $new_data['billing_days'];
                    $res_data[$uid][$locID][$key]['collectionrate'] = $collectionrate;
//                    $res_data[$uid][$locID][$key]['total_billing_days'] += $new_data['billing_days'];


                    $unitsvisit = $this->divide($new_data['units_charged'], $new_data['visits_attended']);
                    $unitsactive = $this->divide($new_data['units_charged'], $new_data['active_cases']);
                    $unitshour = $this->divide($new_data['units_charged'], $new_data['hours_worked']);
                    $units8hour = $this->divide($new_data['units_charged'], ($new_data['hours_worked'] / 8));
                    $chargeunit = $this->divide($new_data['charges'], $new_data['units_charged']);
                    $collectedunit = $this->divide($new_data['charges'] * ( $collectionrate / 100), $new_data['units_charged']);

//Visit Ratios
                    $visitscase = $this->divide($new_data['visits_attended'], $new_data['active_cases']);
                    $visitshour = $this->divide($new_data['visits_attended'], $new_data['hours_worked']);
                    $visits8hour = $this->divide($new_data['visits_attended'], ($new_data['hours_worked'] / 8));
                    $chargesvisit = $this->divide($new_data['charges'], $new_data['visits_attended']);
                    $collectedvisit = $this->divide($new_data['charges'] * ( $collectionrate / 100), $new_data['visits_attended']);
                    $noshowvisit = $this->divide($new_data['no_show'], $new_data['visits_attended']);

//Case Management
                    $casesday = $this->divide($new_data['active_cases'], $new_data['days_worked']);
                    $passiverate = $this->divide($new_data['passive_discharges'], ($new_data['active_discharges'] + $new_data['passive_discharges']));
                    $attendrate = $this->divide($new_data['visits_attended'], $new_data['visits_scheduled']);
                    $pdisadis = $this->divide($new_data['passive_discharges'], $new_data['active_discharges']);

//Financial Ratios
                    $chargeshour = $this->divide($new_data['charges'], $new_data['hours_worked']);
                    $collectedhour = $this->divide($new_data['charges'] * ( $collectionrate / 100), $new_data['hours_worked']);

//Staffing Ratios
                    $unitsbilling = $this->divide($new_data['units_charged'], $new_data['billing_days']);
                    $visitsbilling = $this->divide($new_data['visits_attended'], $new_data['billing_days']);
                    $thourdays = $this->divide($new_data['hours_worked'], $new_data['days_worked']);
                    $scheddensity = $this->divide($new_data['hours_scheduled'], $new_data['hours_worked']);

                    $res_data[$uid][$locID][$key]['unitsvisit'] = $unitsvisit;
                    $res_data[$uid][$locID][$key]['unitsactive'] = $unitsactive;
                    $res_data[$uid][$locID][$key]['unitshour'] = $unitshour;
                    $res_data[$uid][$locID][$key]['units8hour'] = $units8hour;
                    $res_data[$uid][$locID][$key]['chargeunit'] = $chargeunit;
                    $res_data[$uid][$locID][$key]['collectedunit'] = $collectedunit;

                    $res_data[$uid][$locID][$key]['visitscase'] = $visitscase;
                    $res_data[$uid][$locID][$key]['visitshour'] = $visitshour;
                    $res_data[$uid][$locID][$key]['visits8hour'] = $visits8hour;
                    $res_data[$uid][$locID][$key]['chargesvisit'] = $chargesvisit;
                    $res_data[$uid][$locID][$key]['collectedvisit'] = $collectedvisit;
                    $res_data[$uid][$locID][$key]['noshowvisit'] = $noshowvisit;

                    $res_data[$uid][$locID][$key]['casesday'] = $casesday;
                    $res_data[$uid][$locID][$key]['passiverate'] = $passiverate;
                    $res_data[$uid][$locID][$key]['attendrate'] = $attendrate;

                    $res_data[$uid][$locID][$key]['chargeshour'] = $chargeshour;
                    $res_data[$uid][$locID][$key]['collectedhour'] = $collectedhour;

                    $res_data[$uid][$locID][$key]['unitsbilling'] = $unitsbilling;
                    $res_data[$uid][$locID][$key]['visitsbilling'] = $visitsbilling;
                    $res_data[$uid][$locID][$key]['thourdays'] = $thourdays;
                    $res_data[$uid][$locID][$key]['scheddensity'] = $scheddensity;
                }
            }
        }
//        print_r(json_encode($res_data));

        return $res_data;
    }

    public function get_data_individual($user_id, $start_date, $end_date, $location_id) {
        $practice_id = $this->userpractices->get_practice($this->session->userdata('user_id'));
        $data_per_user = array();
        $res_data = array();
        foreach ($user_id as $user_key => $userid) {
            $data_per_user[$userid['user_id']] = $this->cliniciandata->get_clinician_data_by_date($userid['user_id'], $location_id, date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));

            foreach ($data_per_user[$userid['user_id']] as $key => $data) {
                $collectionrate = $this->collectionrates->get_collection_rate_by_date($location_id, $data['end_date']);
                $res_data[$userid['user_id']][$key]['start_date'] = $data['start_date'];
                $res_data[$userid['user_id']][$key]['new_cases'] = $data['new_cases'];
                $res_data[$userid['user_id']][$key]['active_discharges'] = $data['active_discharges'];
                $res_data[$userid['user_id']][$key]['passive_discharges'] = $data['passive_discharges'];
                $res_data[$userid['user_id']][$key]['active_cases'] = $data['active_cases'];
                $res_data[$userid['user_id']][$key]['removed_cases'] = $data['removed_cases'];
                $res_data[$userid['user_id']][$key]['active_cases'] = $data['active_cases'];
                $res_data[$userid['user_id']][$key]['visits_attended'] = $data['visits_attended'];
                $res_data[$userid['user_id']][$key]['visits_scheduled'] = $data['visits_scheduled'];
                $res_data[$userid['user_id']][$key]['no_show'] = $data['no_show'];
                $res_data[$userid['user_id']][$key]['units_charged'] = $data['units_charged'];
                $res_data[$userid['user_id']][$key]['days_worked'] = $data['days_worked'];
                $res_data[$userid['user_id']][$key]['hours_worked'] = $data['hours_worked'];
                $res_data[$userid['user_id']][$key]['hours_scheduled'] = $data['hours_scheduled'];
                $res_data[$userid['user_id']][$key]['collected'] = $data['charges'] * ( $collectionrate / 100);
                $res_data[$userid['user_id']][$key]['charges'] = $data['charges'];
                $res_data[$userid['user_id']][$key]['billing_days'] = $data['billing_days'];
                $res_data[$userid['user_id']][$key]['collectionrate'] = $collectionrate;
                $res_data[$userid['user_id']][$key]['total_billing_days'] += $data['billing_days'];


                $unitsvisit = $this->divide($data['units_charged'], $data['visits_attended']);
                $unitsactive = $this->divide($data['units_charged'], $data['active_cases']);
                $unitshour = $this->divide($data['units_charged'], $data['hours_worked']);
                $units8hour = $this->divide($data['units_charged'], ($data['hours_worked'] / 8));
                $chargeunit = $this->divide($data['charges'], $data['units_charged']);
                $collectedunit = $this->divide($data['charges'] * ( $collectionrate / 100), $data['units_charged']);

//Visit Ratios
                $visitscase = $this->divide($data['visits_attended'], $data['active_cases']);
                $visitshour = $this->divide($data['visits_attended'], $data['hours_worked']);
                $visits8hour = $this->divide($data['visits_attended'], ($data['hours_worked'] / 8));
                $chargesvisit = $this->divide($data['charges'], $data['visits_attended']);
                $collectedvisit = $this->divide($data['charges'] * ( $collectionrate / 100), $data['visits_attended']);
                $noshowvisit = $this->divide($data['no_show'], $data['visits_attended']);

//Case Management
                $casesday = $this->divide($data['active_cases'], $data['days_worked']);
                $passiverate = $this->divide($data['passive_discharges'], ($data['active_discharges'] + $data['passive_discharges']));
                $attendrate = $this->divide($data['visits_attended'], $data['visits_scheduled']);
                $pdisadis = $this->divide($data['passive_discharges'], $data['active_discharges']);

//Financial Ratios
                $chargeshour = $this->divide($data['charges'], $data['hours_worked']);
                $collectedhour = $this->divide($data['charges'] * ( $collectionrate / 100), $data['hours_worked']);

//Staffing Ratios
                $unitsbilling = $this->divide($data['units_charged'], $data['billing_days']);
                $visitsbilling = $this->divide($data['visits_attended'], $data['billing_days']);
                $thourdays = $this->divide($data['hours_worked'], $data['days_worked']);
                $scheddensity = $this->divide($data['hours_scheduled'], $data['hours_worked']);

                $res_data[$userid['user_id']][$key]['unitsvisit'] = $unitsvisit;
                $res_data[$userid['user_id']][$key]['unitsactive'] = $unitsactive;
                $res_data[$userid['user_id']][$key]['unitshour'] = $unitshour;
                $res_data[$userid['user_id']][$key]['units8hour'] = $units8hour;
                $res_data[$userid['user_id']][$key]['chargeunit'] = $chargeunit;
                $res_data[$userid['user_id']][$key]['collectedunit'] = $collectedunit;

                $res_data[$userid['user_id']][$key]['visitscase'] = $visitscase;
                $res_data[$userid['user_id']][$key]['visitshour'] = $visitshour;
                $res_data[$userid['user_id']][$key]['visits8hour'] = $visits8hour;
                $res_data[$userid['user_id']][$key]['chargesvisit'] = $chargesvisit;
                $res_data[$userid['user_id']][$key]['collectedvisit'] = $collectedvisit;
                $res_data[$userid['user_id']][$key]['noshowvisit'] = $noshowvisit;

                $res_data[$userid['user_id']][$key]['casesday'] = $casesday;
                $res_data[$userid['user_id']][$key]['passiverate'] = $passiverate;
                $res_data[$userid['user_id']][$key]['attendrate'] = $attendrate;

                $res_data[$userid['user_id']][$key]['chargeshour'] = $chargeshour;
                $res_data[$userid['user_id']][$key]['collectedhour'] = $collectedhour;

                $res_data[$userid['user_id']][$key]['unitsbilling'] = $unitsbilling;
                $res_data[$userid['user_id']][$key]['visitsbilling'] = $visitsbilling;
                $res_data[$userid['user_id']][$key]['thourdays'] = $thourdays;
                $res_data[$userid['user_id']][$key]['scheddensity'] = $scheddensity;
            }
        }
        return $res_data;
    }

    public function display_clinic_view($data_per_loc) {
        $str = '<h6>Location: </h6>'
                . '<table class="table  dashboard-table-pdf">
                        <tr>
                            <th colspan="12">Volume</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>New<br>Cases</th>
                            <th>Active<br>D/C</th>
                            <th>Passive<br>D/C</th>
                            <th>Active<br>Cases</th>
                            <th>Vists<br>Att.</th>
                            <th>Visits<br>Cancels</th>
                            <th>Visits<br>Sched.</th>
                            <th>Units<br>Charged</th>
                            <th>Days<br>Worked</th>
                            <th>Hours<br>Worked</th>
                            <th>Clinical<br>Hours<br>Sched</th>
                        </tr>';
        foreach ($data_per_loc as $datal) {
            $str .= '<tr>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['new_cases'] . '</td>
                            <td>' . $datal['active_discharges'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                            <td>' . $datal['user_id'] . '</td>
                        </tr>
                   </table>';
        }


        return $str;
    }

}
