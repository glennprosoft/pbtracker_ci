<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Line_graph extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('cliniciandatas');
        $this->load->model('collectionrates');
    }

    public function check_if_authorized() {
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//              || $this->in_array_r("Biller", $roles)
//              || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $location_id = $this->input->post('location_id');
        
        $this->check_if_authorized();

        $data['viewMenu'] = array(
            array('item' => 'New Cases', 'desc' => 'Total number of new evaulations'),
            array('item' => 'Active Discharges', 'desc' => 'Patients discharged by therapist'),
            array('item' => 'Passive Discharges', 'desc' => 'Self Discharges'),
            array('item' => 'Active Cases', 'desc' => 'Total cases not yet discharged'),
            array('item' => 'Visits Attended', 'desc' => 'Appointments that are kept'),
            array('item' => 'Visits Cancels & No-Shows', 'desc' => 'Appointments not kept and not rescheduled with at least 24 hours notice'),
            array('item' => 'Visits Scheduled', 'desc' => 'Visits Scheduled = Visits Attended + Visits Cancels & No-Shows '),
            array('item' => 'Units Charged', 'desc' => ''),
            array('item' => 'Days Worked', 'desc' => ''),
            array('item' => 'Hours Worked', 'desc' => 'Total number of hours worked including patient care meetings, documentation, and case management'),
            array('item' => 'Clinical Hours Scheduled', 'desc' => 'Total aggregate hours scheduled (e.g 2 patients scheduled for one hour each = 2 hours scheduled, even if patients are scheduled at the same time) ')
        );


        $data['billingMenu'] = array(
            array('item' => 'Charges', 'desc' => 'Total $ amount charges for services'),
            array('item' => 'Estimated Collections', 'desc' => 'Estimated Collections = Charge * Collection Rate'),
            array('item' => 'Billing Days', 'desc' => 'Number of dates for which services were delivered'),
        );


        $data['unitRatiosMenu'] = array(
            array('item' => 'Units / Visits', 'desc' => 'Units/Visit = Units Charged/Visits Attended'),
            array('item' => 'Units / Cases', 'desc' => 'Units/Active Case = Units Charged/Active Cases'),
            array('item' => 'Units / Hours Worked', 'desc' => 'Units/Hour Worked = Units Charged/Hours Worked'),
            array('item' => 'Units / 8 Hours Worked', 'desc' => 'Units/8 Hours Worked = Units Charged/(Hours Worked/8)'),
            array('item' => 'Charge / Units', 'desc' => 'Charge/Unit = Charges/Units charged'),
            array('item' => 'Collected / Units', 'desc' => 'Collected/Unit = Estimated Collections/Units Charged')
        );

        $data['visitRatiosMenu'] = array(
            array('item' => 'Visits / Cases', 'desc' => 'Visits/Case = Total Visits/Active Cases'),
            array('item' => 'Visits / Hours Worked', 'desc' => 'Visits/Hour Worked = Total Visits/Hours Worked'),
            array('item' => 'Visits / 8 Hours Worked', 'desc' => 'Visits/8 Hours Worked = Total Visits(Hours Worked/8)'),
            array('item' => 'Charges / Visits', 'desc' => 'Charge/Visit Attended = Charges/Total Visits'),
            array('item' => 'Collected / Visits', 'desc' => 'Collected Visit = Estimated Collections/Total Visits')
        );

        $data['managementMenu'] = array(
            array('item' => 'Cases / Days', 'desc' => 'Active Cases/Day = Active Cases/Days Worked'),
            array('item' => 'Passive Discharge Rate', 'desc' => 'Passive Discharge Rate = Passive Discharges/(Active Discharges + Passive Discharge)'),
            array('item' => 'Attendance Rate', 'desc' => 'Attendance Rate = Visits Attended/Visits Scheduled')
        );

        $data['financialRatiosMenu'] = array(
            array('item' => 'Charges / Hours Worked', 'desc' => 'Charges/Hour Worked = Charges/Hours Worked'),
            array('item' => 'Collected / Hours Worked', 'desc' => 'Collected/Hour Worked = Estimated Collections/Hours Worked')
        );

        $data['staffingRatiosMenu'] = array(
            array('item' => 'Units / Billing Days', 'desc' => 'Units/Billing Day = Units Charged/Billing Days'),
            array('item' => 'Visits / Billing Days', 'desc' => 'Visits/Billing Day = Visits Attended/Billing Days'),
            array('item' => 'Total Hours Worked / Days Worked', 'desc' => 'Total Hours Worked/Days Worked = Hours Worked/Days Worked'),
            array('item' => 'Schedule Density', 'desc' => 'Scheduled Density = Clinical Hours Scheduled/Hours Worked')
        );
        $roles = $this->session->userdata('roles');
        $practiceId = $this->session->userdata('practice_id');
        //check roles
        if ($this->in_array_r('Super Administrator', $roles)) { //if Super ADmin
            //get all locations
            $locations = $this->userlocations->get_all();
        } elseif ($this->in_array_r('Practice Owner', $roles)) {//if Practice Owner
            //get practice locations
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
            //get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        $data1 = array(
            'location_data' => $locations,
        );

        if (isset($start_date)) {
            $sessArr = array(
                'graphrequesteddata' => array(
                    'sdate' => $start_date,
                    'edate' => $end_date,
                    'locationid' => $location_id,
                )
            );
            $data1['graphdata'] = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'location_id' => $location_id,
            );
        } else {
            $sessArr = array(
                'graphrequesteddata' => array(
                    'sdate' => '',
                    'edate' => '',
                    'locationid' => '',
                )
            );
        }
        $this->session->set_userdata($sessArr);

        $this->load->vars($data1);
        $this->template->set_layout('default');
        $this->template->title('Line Graph');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/line_graph.js") . '"></script>');
        $this->load->view('pages/line_graph', $data);
        $this->template->build('pages/line_graph');
    }

    public function get_data() {
        $sDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $eDate = date('Y-m-d', strtotime($this->input->post('end-date')));
        $locationId = $this->input->post('location');

        $this->session->set_userdata('startdate', date('m/d/Y', strtotime($sDate)));
        $this->session->set_userdata('enddate', date('m/d/Y', strtotime($eDate)));

        $arr = array(
            'sdate' => $sDate,
            'edate' => $eDate,
            'locationid' => $locationId,
        );

        $this->session->set_userdata('graphrequesteddata', $arr);

        $usersData = $this->get_linegraph_data($sDate, $eDate, $locationId);

        $names = array();
        $dates = array();
        $values = array();
        $datadates = array();

//        format for names
        foreach ($usersData as $data) {
            if ($this->input->post('show') == 1) {
                $names[] = '<strong>' . strtoupper($data['user']['lname']) . '</strong>,<br/>' . $data['user']['fname'] . ' (' . $data['user']['empid'] . ')';
            } else if ($this->input->post('show') == 2) {
                $names[] = '<strong>' . strtoupper($data['user']['lname']) . '</strong>,<br/>' . $data['user']['fname'];
            } else
            if ($this->input->post('show') == 3) {
                $names[] = '<strong>' . $data['user']['empid'] . '</strong>';
            }
            $values[] = $data['volume']['newcases'];
            $datadates[] = $data['user']['sdate'];
        }

        $strClinicians = $this->set_clinician_list($usersData);

        $data = $data = array(
            'status' => 'success',
            'names' => $names,
            'datas' => $values,
            'clinicians' => $strClinicians,
            'dates' => $datadates,
        );

        echo json_encode($data);
    }

    public function switch_graph() {
        $postarr = $this->session->userdata('graphrequesteddata');

        $sDate = $postarr['sdate'];
        $eDate = $postarr['edate'];
        $locationId = $postarr['locationid'];

        $usersData = $this->get_linegraph_data($sDate, $eDate, $locationId);

        $userids = array();
        $selclin = array();
        $nothingselected = false;

        foreach ($usersData as $id) {
            $userids[] = $id['user']['uid'];
        }

        if ($this->input->post('userid')) {
            $selclin = $this->input->post('userid');
        } else {
            $nothingselected = true;
        }

        $indx = 0;
        foreach ($userids as $uid) {
            if (!in_array($uid, $selclin) || $nothingselected) {
                unset($usersData[$indx]);
            }
            $indx++;
        }

        $names = array();
        $dates = array();
        $values = array();
        $datadates = array();

        foreach ($usersData as $data) {
            if ($this->input->post('show') == 1) {
                $names[] = '<strong>' . strtoupper($data['user']['lname']) . '</strong>,<br/>' . $data['user']['fname'] . ' (' . $data['user']['empid'] . ')';
            } else if ($this->input->post('show') == 2) {
                $names[] = '<strong>' . strtoupper($data['user']['lname']) . '</strong>,<br/>' . $data['user']['fname'];
            } else
            if ($this->input->post('show') == 3) {
                $names[] = '<strong>' . $data['user']['empid'] . '</strong>';
            }
            $datadates[] = $data['user']['sdate'];
        }


        switch ($this->input->post('selected')) {

//VOLUME------------------------------------------------------------
            case 'New Cases':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['newcases'];
                }
                break;
            case 'Active Discharges':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['adis'];
                }
                break;
            case 'Passive Discharges':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['pdis'];
                }
                break;
            case 'Active Cases':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['activecases'];
                }
                break;
            case 'Visits Attended':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['vattended'];
                }
                break;
            case 'Visits Cancels ':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['vcanceled'];
                }
                break;
            case 'Visits Scheduled':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['vscheduled'];
                }
                break;
            case 'Units Charged':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['ucharged'];
                }
                break;
            case 'Days Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['dworked'];
                }
                break;
            case 'Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['hworked'];
                }
                break;

            case 'Clinical Hours Scheduled':
                foreach ($usersData as $data) {
                    $values[] = $data['volume']['hscheduled'];
                }
                break;


//BILLING-----------------------------------------------------------
            case 'Charges':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['billing']['charges']);
                }

                break;
            case 'Estimated Collections':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['billing']['collected']);
                }
                break;
            case 'Billing Days':
                foreach ($usersData as $data) {
                    $values[] = $data['billing']['bdays'];
                }
                break;


//UNIT RATIOS-------------------------------------------------------
            case 'Units / Visits':
                foreach ($usersData as $data) {
                    $values[] = $data['unitratios']['unitsvisit'];
                }
                break;
            case 'Units / Cases':
                foreach ($usersData as $data) {
                    $values[] = $data['unitratios']['unitsactive'];
                }
                break;
            case 'Units / Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['unitratios']['unitshour'];
                }
                break;
            case 'Units / 8 Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['unitratios']['units8hour'];
                }
                break;
            case 'Charge / Units':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['unitratios']['chargeunit']);
                }
                break;
            case 'Collected / Units':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['unitratios']['collectedunit']);
                }
                break;

//VISITS RATIOS-----------------------------------------------------
            case 'Visits / Cases':
                foreach ($usersData as $data) {
                    $values[] = $data['visitratios']['visitscase'];
                }
                break;
            case 'Visits / Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['visitratios']['visitshour'];
                }
                break;
            case 'Visits / 8 Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['visitratios']['visits8hour'];
                }
                break;
            case 'Charges / Visits':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['visitratios']['chargesvisit']);
                }
                break;
            case 'Collected / Visits':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['visitratios']['collectedvisit']);
                }
                break;

//CASE MANAGEMENT RATIOS--------------------------------------------
            case 'Cases / Days':
                foreach ($usersData as $data) {
                    $values[] = $data['casemanage']['casesday'];
                }
                break;
            case 'Passive Discharge Rate':
                foreach ($usersData as $data) {
                    $values[] = $data['casemanage']['passiverate'];
                }
                break;
            case 'Attendance Rate':
                foreach ($usersData as $data) {
                    $values[] = $data['casemanage']['attendrate'];
                }
                break;


//FINANCIAL RATIOS--------------------------------------------------
            case 'Charges / Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['finance']['chargeshour']);
                }
                break;
            case 'Collected / Hours Worked':
                foreach ($usersData as $data) {
                    $values[] = str_replace(',', '', $data['finance']['collectedhour']);
                }
                break;


//STAFFING RATIOS---------------------------------------------------
            case 'Units / Billing Days':
                foreach ($usersData as $data) {
                    $values[] = $data['staff']['unitsbilling'];
                }
                break;
            case 'Visits / Billing Days':
                foreach ($usersData as $data) {
                    $values[] = $data['staff']['visitsbilling'];
                }
                break;
            case 'Total Hours Worked / Days Worked':
                foreach ($usersData as $data) {
                    $values[] = $data['staff']['thourdays'];
                }
                break;
            case 'Schedule Density':
                foreach ($usersData as $data) {
                    $values[] = $data['staff']['scheddensity'];
                }
                break;
        }

        $data = $data = array(
            'status' => 'success',
            'names' => $names,
            'datas' => $values,
            'dates' => $datadates,
        );

        echo json_encode($data);
    }

    public function set_clinician_list($usersData) {
        $userid = array();
        $name = array();
        $strClinicians = '';
        $ind = 0;
        foreach ($usersData as $data) {
            $userid[] = $data['user']['uid'];
            $name[] = '<strong>' . strtoupper($data['user']['lname']) . '</strong>, ' . $data['user']['fname'] . ' (' . $data['user']['empid'] . ')';
        }
        foreach ($userid as $id) {
            $strClinicians .= '<label class="checkbox">';
            $strClinicians .= '<input type="checkbox" value="' . $userid[$ind] . '" name="userid[]" checked="checked">';
            $strClinicians .= $name[$ind];
            $strClinicians .= '</label>';
            $ind++;
        }

        return $strClinicians;
    }

    public function get_linegraph_data($sDate, $eDate, $locationId) {
        $usersData = array();

        if ($sDate == '') {
            return $usersData;
        }

        $userIds = $this->userlocations->get_users_underloc($locationId);



        foreach ($userIds as $id) {
            $tnewCases = array();
            $taDis = array();
            $tpDis = array();
            $addCases = array();
            $remCases = array();
            $activeCases = array();
            $vAttended = array();
            $vScheduled = array();
            $vCanceled = array();
            $uCharged = array();
            $dWorked = array();
            $hWorked = array();
            $hScheduled = array();
            $collected = array();
            $charges = array();
            $bdays = array();
            $emp_id = '';
            $fname = '';
            $lname = '';
            $include = false;
            $sdate = array();
            $edate = array();
            //Computed Values
            //Unit Ratios
            $unitsvisit = array();
            $unitsactive = array();
            $unitshour = array();
            $units8hour = array();
            $chargeunit = array();
            $collectedunit = array();

            //Visit Ratios
            $visitscase = array();
            $visitshour = array();
            $visits8hour = array();
            $chargesvisit = array();
            $collectedvisit = array();
            $noshowvisit = array();

            //Case Management
            $casesday = array();
            $passiverate = array();
            $attendrate = array();
            $pdisadis = array();

            //Financial Ratios
            $chargeshour = array();
            $collectedhour = array();

            //Staffing Ratios
            $unitsbilling = array();
            $visitsbilling = array();
            $thourdays = array();
            $scheddensity = array();

            $userData = $this->cliniciandatas->get_graph_data($id['user_id'], $locationId, $sDate, $eDate);
            $emp_id = $id['employee_id'];
            $fname = $id['first_name'];
            $lname = $id['last_name'];
            $uid = $id['user_id'];
            foreach ($userData as $data) {
                $collectionrate = $this->collectionrates->get_collection_rate_by_date($locationId, $data['end_date']);

                if ($data) {
                    //Volume
                    $sdate[] = $data['start_date'];
                    $edate[] = $data['end_date'];

                    $tnewCases[] = $data['new_cases'];
                    $taDis[] = $data['active_discharges'];
                    $tpDis[] = $data['passive_discharges'];
                    $addCases[] = $data['added_cases'];
                    $remCases[] = $data['removed_cases'];
                    $activeCases[] = $data['active_cases'];
                    $vAttended[] = $data['visits_attended'];
                    $vScheduled[] = $data['visits_scheduled'];
                    $vCanceled[] = $data['no_show'];
                    $uCharged[] = $data['units_charged'];
                    $dWorked[] = $data['days_worked'];
                    $hWorked[] = $data['hours_worked'];
                    $hScheduled[] = $data['hours_scheduled'];


                    //Billing
                    $collected[] = $data['charges'] * ( $collectionrate / 100);
                    $charges[] = $data['charges'];
                    $bdays[] = $data['billing_days'];

                    //Unit Ratios
                    $unitsvisit[] = $this->divide($data['units_charged'], $data['visits_attended']);
                    $unitsactive[] = $this->divide($data['units_charged'], $data['active_cases']);
                    $unitshour[] = $this->divide($data['units_charged'], $data['hours_worked']);
                    $units8hour[] = $this->divide($data['units_charged'], ($data['hours_worked'] / 8));
                    $chargeunit[] = $this->divide($data['charges'], $data['units_charged']);
                    $collectedunit[] = $this->divide($data['charges'] * ( $collectionrate / 100), $data['units_charged']);

                    //Visit Ratios
                    $visitscase[] = $this->divide($data['visits_attended'], $data['active_cases']);
                    $visitshour[] = $this->divide($data['visits_attended'], $data['hours_worked']);
                    $visits8hour[] = $this->divide($data['visits_attended'], ($data['hours_worked'] / 8));
                    $chargesvisit[] = $this->divide($data['charges'], $data['visits_attended']);
                    $collectedvisit[] = $this->divide($data['charges'] * ( $collectionrate / 100), $data['visits_attended']);
//                    $noshowvisit[] = $this->divide2($data['no_show'], $data['visits_attended']);
                    //Case Management
                    $casesday[] = $this->divide($data['active_cases'], $data['days_worked']);
                    $passiverate[] = $this->divide2($data['passive_discharges'], ($data['active_discharges'] + $data['passive_discharges']));
                    $attendrate[] = $this->divide2($data['visits_attended'], $data['visits_scheduled']);
//                    $pdisadis[] = $this->divide2($data['passive_discharges'], $data['active_discharges']);
                    //Financial Ratios
                    $chargeshour[] = $this->divide($data['charges'], $data['hours_worked']);
                    $collectedhour[] = $this->divide($data['charges'] * ( $collectionrate / 100), $data['hours_worked']);

                    //Staffing Ratios
                    $unitsbilling[] = $this->divide($data['units_charged'], $data['billing_days']);
                    $visitsbilling[] = $this->divide($data['visits_attended'], $data['billing_days']);
                    $thourdays[] = $this->divide($data['hours_worked'], $data['days_worked']);
                    $scheddensity[] = $this->divide($data['hours_scheduled'], $data['hours_worked']);

                    $include = true;
                }
            }

            if ($include) {
                $usersData[] = array(
                    //userdata
                    'user' => array(
                        'uid' => $uid,
                        'empid' => $emp_id,
                        'fname' => $fname,
                        'lname' => $lname,
                        'sdate' => $sdate,
                        'edate' => $edate,
                    ),
                    //volume
                    'volume' => array(
                        'newcases' => $tnewCases,
                        'adis' => $taDis,
                        'pdis' => $tpDis,
                        'addcases' => $addCases,
                        'remcases' => $remCases,
                        'activecases' => $activeCases,
                        'vattended' => $vAttended,
                        'vscheduled' => $vScheduled,
                        'vcanceled' => $vCanceled,
                        'ucharged' => $uCharged,
                        'dworked' => $dWorked,
                        'hworked' => $hWorked,
                        'hscheduled' => $hScheduled,
                        'collected' => $collected,
                        'charges' => $charges,
                        'bdays' => $bdays
                    ),
                    'billing' => array(
                        'collected' => $collected,
                        'charges' => $charges,
                        'bdays' => $bdays,
                    ),
                    //Unit Ratios
                    'unitratios' => array(
                        'unitsvisit' => $unitsvisit,
                        'unitsactive' => $unitsactive,
                        'unitshour' => $unitshour,
                        'units8hour' => $units8hour,
                        'chargeunit' => $chargeunit,
                        'collectedunit' => $collectedunit,
                    ),
                    //Visit Ratios
                    'visitratios' => array(
                        'visitscase' => $visitscase,
                        'visitshour' => $visitshour,
                        'visits8hour' => $visits8hour,
                        'chargesvisit' => $chargesvisit,
                        'collectedvisit' => $collectedvisit,
//                        'noshowvisit' => number_format($noshowvisit * 100, 2) . ' %',
                    ),
                    //Case Management
                    'casemanage' => array(
                        'casesday' => $casesday,
                        'passiverate' => $passiverate,
                        'attendrate' => $attendrate,
//                        'pdisadis' => number_format($pdisadis * 100, 2) . ' %',
                    ),
                    //Financial Ratios
                    'finance' => array(
                        'chargeshour' => $chargeshour,
                        'collectedhour' => $collectedhour,
                    ),
                    //Staffing Ratios
                    'staff' => array(
                        'unitsbilling' => $unitsbilling,
                        'visitsbilling' => $visitsbilling,
                        'thourdays' => $thourdays,
                        'scheddensity' => $scheddensity,
                    )
                );
            }
        }
//        print_r($usersData);
        return $usersData;
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    function divide($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor), 2);
            return $quotient;
        }
    }

    function divide2($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor) * 100, 2);
            return $quotient;
        }
    }

}
