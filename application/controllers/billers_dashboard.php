<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Billers_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('positions');
        $this->load->model('mailer');
        $this->load->model('cliniciandatas');
    }

    public function check_if_authorized() {

        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles) || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        
        $this->check_if_authorized();

        $roles = $this->session->userdata("roles");
        $practiceId = $this->session->userdata('practice_id');
        //check roles
        if ($this->in_array_r('Super Administrator', $roles)) { //if Super ADmin
            //get all locations
            $locations = $this->userlocations->get_all();
        } elseif ($this->in_array_r('Practice Owner', $roles)) {//if Practice Owner
            //get practice locations
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
            //get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        $data1 = array(
            'location_data' => $locations,
        );

        $this->load->vars($data1);

        $this->template->set_layout('default');
        $this->template->title('Billers Dashboard');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src = "' . base_url("js/billers_dashboard.js") . '"></script>');

        $this->template->build('pages/billers_dashboard');
    }

    public function get_users() {
        $weekDates = $this->get_week_dates($this->input->post('date_select'));
        $billings = array();
        $users = array();
        $returndata = array();
        $markups = $this->set_markup($weekDates);

        foreach ($markups as $markup) {

            $returndata[] = array(
                'user' => $markup['user'],
                'mon' => $markup['markup'][0],
                'tue' => $markup['markup'][1],
                'wed' => $markup['markup'][2],
                'thu' => $markup['markup'][3],
                'fri' => $markup['markup'][4],
                'sat' => $markup['markup'][5],
                'sun' => $markup['markup'][6],
            );
        }


        $data = array(
            'dates' => $this->convert_date_format('F d', $weekDates),
            'daterange' => $this->convert_date_format('F d, Y', $weekDates),
            'markups' => $returndata,
        );

//        print_r($returndata);
        echo json_encode($data);
    }

    public function get_week_dates($date) {
        $week_dates = array();
        $ts = strtotime($date);
        $week_number = date('W', $ts);
        $year = date('Y', $ts);

        for ($day = 1; $day <= 7; $day++) {
            $week_dates[$day] = date('m/d/Y', strtotime($year . "W" . $week_number . $day));
        }
        return $week_dates;
    }

    public function set_markup($weekDates) {
        $locId = $this->input->post('location_id');
        $users = $this->userlocations->get_users_underloc($locId);
        $userscharges = array();
        $markup = array();
        $data = array();

        if ($users) {
            foreach ($users as $id) {
                $roles = $this->userroles->get_role_by_id($id['user_id']);
                if ($this->in_array_r('Clinician', $roles)) { //if user is clinician
                    $userscharges = array();
                    $markup = array();
                    $userscharges[] = strtoupper($id['last_name']) . ',<br/> ' . $id['first_name'] . ' (' . $id['employee_id'] . ')';
                    $int = 0;
                    foreach ($weekDates as $day){
                        $weeklydata = $this->cliniciandatas->get_clinician_data($id['user_id'], $locId, date('Y-m-d', strtotime($day)), date('Y-m-d', strtotime($day)));
                        if ($weeklydata) {
                            if ($this->in_array_r('Biller', $this->session->userdata('roles'))) {
                                $markup[] = '<input style="height:30px; width:90px;" ng-model="price" currency onkeypress="validate(event)" type="numeric" uid="' . $id["user_id"] . '" name="charges[' . $id["user_id"] . '][' . date('Y-m-d', strtotime($day)) . ']" date="' . date('Y-m-d', strtotime($day)) . '" class="form-control biller-input decimal" value = "' . str_replace(',', '', number_format($weeklydata[0]['charges'], 2)) . '">';
                            } else {
                                $markup[] = '<input disabled style="height:30px; width:90px;" ng-model="price" currency onkeypress="validate(event)" type="numeric" uid="' . $id["user_id"] . '" name="charges[' . $id["user_id"] . '][' . date('Y-m-d', strtotime($day)) . ']" date="' . date('Y-m-d', strtotime($day)) . '" class="form-control biller-input decimal" value = "' . str_replace(',', '', number_format($weeklydata[0]['charges'], 2)) . '">';
                            }
                        } else {
                            $markup[] = '<input style="height:30px; width:90px;" onkeypress="validate(event)"  type="numeric" disabled uid="' . $id["user_id"] . '" name="charges[' . $id["user_id"] . '][' . date('Y-m-d', strtotime($day)) . ']" date="' . date('Y-m-d', strtotime($day)) . '" class="form-control biller-input decimal" value = "">';
                        }
                        $int++;
                    }
                    $data[] = array(
                        'user' => $userscharges,
                        'markup' => $markup,
                    );
                }
            }
        }

//        print_r($data);
        return $data;
    }
    
    

    public function save_billing() {
        $charges = $this->input->post('charges');
        $loc = $this->input->post('location_id');
        $date = array();
        $id = array();
        $charge = array();

        foreach ($charges as $key => $id) {
            foreach ($id as $key2 => $date) {
//                echo $key . ' - '. $key2 .' - ' . $charges[$key][$key2] .'              ';
                $this->update_userdata(str_replace(',', '', $charges[$key][$key2]), $key, $key2, $loc);
            }
        }
    }

    public function check_entry() {

        $charges = $this->input->post('charges');
        if ($charges) {
            $loc = $this->input->post('location_id');
            $date = array();
            $id = array();
            $charge = array();
            $names = array();
            $status = 'success';
            foreach ($charges as $key => $id) {
                foreach ($id as $key2 => $date) {
                    if ($charges[$key][$key2] == '') {
                        $status = 'error';
                        $names[] = 'charges[' . $key . '][' . $key2 . ']';
                    }
                }
            }
            $data = array(
                'status' => $status,
                'names' => $names,
            );

            if ($status == 'success') {
                $this->save_billing();
                echo json_encode($data);
            } else {
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 'nodata',
            );
            echo json_encode($data);
        }
    }

    function update_userdata($charge, $uid, $date, $loc) {
        $this->cliniciandatas->update_charges($charge, $uid, $date, $loc);
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function convert_date_format($format, $date_array) {
        $date = array();
        for ($i = 1, $h = 1; $i <= count($date_array); $i++, $h++) {
            $date[$i] = strtotime($date_array[$h]);
            $date[$i] = date($format, $date[$i]);
        }
        return $date;
    }

}
