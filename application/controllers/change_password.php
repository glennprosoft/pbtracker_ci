<?php
//asdfasdfasd
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Change_password extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('users');
    }
    
    public function validate_fields(){
        $rules = array(
            array(
                'field' => 'old-password',
                'label' => 'Old Password',
                'rules' => 'trim|required|callback_check_old_pass'
            ),
            array(
                'field' => 'new-password',
                'label' => 'New Password',
                'rules' => 'trim|required|matches[confirm-password]'  
            ),
            array(
                'field' => 'confirm-password',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|matches[new-password]'
            )
        );
        
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == false){
            $data = array(
                'status' => 'with-error',
                'oldpassword' => form_error('old-password'),
                'newpassword' => form_error('new-password'),
                'confirmpassword' => form_error('confirm-password') 
            );
            
            echo json_encode($data);
        }else{
            $this->change_pass();
            $this->session->set_userdata('changepass', '0');
            $data = array(
                'status' => 'success'
            );
            
            echo json_encode($data);
        }
    }
    
    public function change_pass(){
        $session_id = $this->session->userdata('user_id');
        $data = array(
            'password' => "'" .md5($this->input->post('new-password')). "'",
            'password_reset_date' => "'".date('Y-m-d', strtotime(date('Y-m-d').'+90 days'))."'"
        );
        $this->users->update_user($data, $session_id);    
    }
    
    public function check_old_pass(){
        $pass_from_post = md5($this->input->post('old-password'));
        $data =  $this->users->get_user_by_id($this->session->userdata('user_id'));
        
        if($pass_from_post == $data[0]['password']){
            return true;
        }else{
            $this->form_validation->set_message('check_old_pass','Wrong password');
            return false;
        }
    }
}
