<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_registration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('collectionrates');
        $this->load->model('userlocations');
        $this->load->model('locations');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('mailer');
    }
    
    public function index() {
        
        if(!$_GET['hash']){
            redirect('login');
        }
        
        $strHash = $_GET['hash'];
        $data = $this->get_page_data($strHash);
        
        if(!$data){
            redirect('login');
        }
        
        $userData = array(
            'userId' => $data['userData'][0]['id'],
            'empId' => $data['userData'][0]['employee_id'],
            'fName' => $data['userData'][0]['first_name'],
            'lName' => $data['userData'][0]['last_name'],
            'email' => $data['userData'][0]['email'],
            'valCode' => $data['userData'][0]['validation_code'],
        );

        $this->load->vars($userData);
        $this->template->set_layout('owner_registration');
        $this->template->title('User Registration');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->append_metadata('<script src="' . base_url("js/password.js") . '"></script>');
        $this->template->append_metadata('<script src="' . base_url("js/user-registration.js") . '"></script>');
        $this->template->build('pages/user_registration');
    }

    public function get_page_data($hash) {
        $userData = $this->users->get_user_by_valcode($hash);

        $data = array(
            'userData' => $userData,
        );

        if ($userData) {
            return $data;
        } else {
            return false;
        }
    }

    public function page1_val() {

        $rules = array(
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'confirmpassword',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|matches[password]'
            )
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
                'password' => form_error('password'),
                'confirmpassword' => form_error('confirmpassword')
            );

            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'ok'
            );
            echo json_encode($data);
        }
    }

    public function page2_val() {
        $rules = array(
            array(
                'field' => 'emp-id',
                'label' => 'Employee ID',
                'rules' => 'trim|required|callback_check_empid'
            ),
            array(
                'field' => 'reg-fname',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'reg-lname',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'reg-email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|callback_check_email'
            ),
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
                'empid' => form_error('emp-id'),
                'regfname' => form_error('reg-fname'),
                'reglname' => form_error('reg-lname'),
                'regemail' => form_error('reg-email')
            );

            echo json_encode($data);
        } else {


            $this->update_user();


            $data = array(
                'status' => 'ok'
            );
            echo json_encode($data);
        }
    }

    public function update_user() {
        $passMD5 = md5($this->input->post('password'));
    $passResetDate = date('Y-m-d', strtotime(date('Y-m-d').'+90 days'));

        $data = array(
            'employee_id' => "'" . $this->input->post('emp-id') . "'",
            'first_name' => "'" . $this->input->post('reg-fname') . "'",
            'last_name' => "'" . $this->input->post('reg-lname') . "'",
            'email' => "'" . $this->input->post('reg-email') . "'",
            'password' => "'" . $passMD5 . "'",
            'password_reset_date' => "'" . $passResetDate . "'",
            'employment_status' => 1, //emp status 1 =active / 0 =inactive
            'activation_status' => 1, //activation status  1 = activated / 0 =not activated
        );
        
        $this->users->update_user($data, $this->input->post('user-id'));
    }

    public function check_email() {
        $email = $this->input->post('reg-email');
        $userId = $this->input->post('user-id');

        $isUnique = $this->users->get_user_by_email($email);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $userId) {
                return true;
            } else {
                $this->form_validation->set_message('check_email', 'The %s is already registered to a different account.');
                return false;
            }
        }
    }

    public function check_empid() {
        $empId = $this->input->post('emp-id');
        $userId = $this->input->post('user-id');

        $isUnique = $this->users->get_user_by_empid($empId);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $userId) {
                return true;
            } else {
                $this->form_validation->set_message('check_empid', 'The %s has already been used.');
                return false;
            }
        }
    }

}
