<?php

//asdfasdfasd
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('locations');
        $this->load->model('positions');
        $this->load->model('mailer');
        $this->load->model('cliniciandata');
        $this->load->model('collectionrates');
        $this->load->model('clinicianbaseline');
    }

    public function index() {
        $this->template->set_layout('default');
    }

    function divide($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format($dividend / $divisor, 2);
            return $quotient;
        }
    }

    function divide2($dividend, $divisor) {
        if ($divisor == 0) {
            return 0.00;
        } else {
            $quotient = number_format(($dividend / $divisor) * 100, 2);
            return $quotient;
        }
    }

    public function generate_clinician_report() {
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date')));
        $location_id = $this->input->post('location-list-header');
        $location_practice = $this->locations->get_location_by_id($location_id);
        $practice = $this->practices->get_practice_by_id($location_practice[0]['practice_id']);
        $user_id = $this->session->userdata('user_id');
        $user_res = $this->users->get_user_by_id($user_id);
//        $user_name = 
        $res = $this->cliniciandata->get_clinician_data_by_date($user_id, $location_id, $start_date, $end_date);
        $clinician_data = array();
        $index = 0;
        foreach ($res as $user) {
            $collectionrate = $this->collectionrates->get_collection_rate_by_date($user['location_id'], $user['start_date']);
            $clinician_data[] = array(
                'start_date' => $user['start_date'],
                'new_cases' => $user['new_cases'],
                'active_discharges' => $user['active_discharges'],
                'passive_discharges' => $user['passive_discharges'],
                'active_cases' => $user['active_cases'],
                'visits_attended' => $user['visits_attended'],
                'no_show' => $user['no_show'],
                'visits_scheduled' => $user['visits_scheduled'],
                'units_charged' => $user['units_charged'],
                'hours_worked' => $user['hours_worked'],
                'hours_scheduled' => $user['hours_scheduled'],
                'charges' => $user['charges'],
                'billing_days' => $user['billing_days'],
                'estimated_collections' => $user['charges'] * ($collectionrate / 100),
                'collection_rate' => $collectionrate
            );
            $index++;
        }
//        print_r($res);
        include_once APPPATH . '/third_party/mpdf/mpdf.php';
        $data['location_practice'] = $location_practice;
        $data['user_id'] = $user_id;
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['location_id'] = $location_id;
        $data['clinician_data'] = $clinician_data;
        $data['practice'] = $practice;
        $data['user_name'] = $user_res[0]['first_name'] . ' ' . $user_res[0]['last_name'] . ' (' . $user_res[0]['employee_id'] . ')';
        ini_set('memory_limit', '32M');
        $file = $this->load->view('templates/clinician_dashboard_report', $data, true);
        //format
        $mpdf = new mPDF('utf-8', //mode
                'A4', //format
                11, //fontsize
                '', //fontfamily
                15, //margin_left
                15, //margin_right
                16, //margin_top
                16, //margin_bottom
                9, //margin_header
                9, //margin_footer
                'L'//orientation
        );
        $mpdf->SetHeader($practice[0]['practice_name'] . " : Clinician Report" . "||" . "Inclusive Dates: " . date('m/d/Y', strtotime($start_date)) . " to " . date('m/d/Y', strtotime($end_date)));
        $mpdf->SetFooter("Report Generated on " . date(DATE_RFC822) . '||Page {PAGENO}');
        $mpdf->WriteHTML($file);
        $mpdf->Output('stats.pdf', 'I');
    }

    

    public function generate_billers_report() {
        $start_date = date('Y-m-d', strtotime($this->input->post('start-date-biller')));
        $end_date = date('Y-m-d', strtotime($this->input->post('end-date-biller')));
        $location_id = $this->input->post('location-list-biller-header');
        
            $user_id = $this->session->userdata('user_id');
            $location_practice = $this->locations->get_location_by_id($location_id);
            $practice = $this->practices->get_practice_by_id($location_practice[0]['practice_id']);
            $users = $this->userlocations->get_users_underloc($location_id);
            $clinic_per_loc = array();
            $user_name = array();
            $res_clinic = array();
            foreach ($users as $userid) {
                $res = $this->cliniciandata->get_clinician_data_by_date($userid['user_id'], $location_id, $start_date, $end_date);
                $user_names = $this->users->get_user_by_id($userid['user_id']);
                for ($ct = 0; $ct < count($res); $ct++) {
                    $coll_rate_res['collection_rate'] = $this->collectionrates->get_collection_rate_by_date($location_id, $res[$ct]['start_date']);
                    $res[$ct]['estimated_collection'] = $res[$ct]['charges'] * ($coll_rate_res['collection_rate'] / 100);
                    $res[$ct]['collection_rate'] = $coll_rate_res['collection_rate'];
                }
                $clinic_per_loc[$userid['user_id']] = $res;

                $user_name[$userid['user_id']] = $userid['first_name'] . ' ' . $userid['last_name'] . ' (' . $userid['employee_id'] . ')';
            }
            include_once APPPATH . '/third_party/mpdf/mpdf.php';

            //format    
            $mpdf = new mPDF('utf-8', //mode
                    'A4', //format
                    10, //fontsize
                    '', //fontfamily
                    15, //margin_left
                    15, //margin_right
                    16, //margin_top
                    16, //margin_bottom
                    9, //margin_header
                    9, //margin_footer
                    'L'//orientation
            );
            $mpdf->SetHeader($practice[0]['practice_name'] . " : Biller Report" . "||" . "Inclusive Dates: " . date('m/d/Y', strtotime($start_date)) . " to " . date('m/d/Y', strtotime($end_date)));
            $mpdf->SetFooter("Report Generated on " . date(DATE_RFC822) . '||Page {PAGENO}');


            $data['table'] = $str;
            $data['page_title'] = 'testing';
            $data['location_id'] = $location_id;
            $data['user_id'] = $user_id;
            $data['clinician'] = $clinic_per_loc;
            $data['username'] = $user_name;
            $data['re_test'] = $res_clinic;
            ini_set('memory_limit', '32M');
//        $file = $this->load->view('templates/billers_dashboard_report', $data, true);
            $ctr2 = 0;
            foreach ($clinic_per_loc as $key => $datum) {

                $table_heading = '<h6 style="float: left; text-align: left; line-height:0;width:50%;">' . $user_name[$key] . '</h6>'
                        . ' <h6 style="float: right; text-align: right;width:50%;">' . $location_practice[0]['location_name'] . '</h6>';
                $str = '
                
            <style>
                table{
                    width: 100%;
                    font-size: 11px;
                }
                
                table,th,td{
                    border: 1px solid;
                    border-collapse: collapse;
                }
                
                tr td{
                    text-align: right;
                }
                
            
            </style>
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="3">Billing</th>
                        </tr>
                        <tr>
                            <th width="20%">Date</th>
                            <th width="20%">Charges</th>
                            <th width="30%">Estimated Collections</th>
                            <th width="30%">Collection Rate</th>
                        </tr>
                    </thead>
                    <tbody>';
                for ($ctr = 0; $ctr < count($datum); $ctr++) {
                    $str .='<tr>
                            <td style="text-align:center;">' . date('m/d/Y', strtotime($datum[$ctr]['start_date'])) . '</td>
                            <td style="text-align: center;">$ ' . number_format($datum[$ctr]['charges'], 2) . '</td>
                            <td style="text-align: center;">$ ' . number_format($datum[$ctr]['estimated_collection'], 2) . '</td>
                            <td>' . $datum[$ctr]['collection_rate'] . ' %</td>
                        </tr>
                        ';
                    $charges_tot += number_format($datum[$ctr]['charges'], 2);
                    $estimated_tot += number_format($datum[$ctr]['estimated_collection']);
                }


                $str .= '
                    <tr>
                        <td style="text-align:center;"><strong>Total</strong></td>
                        <td style="text-align:center;">$ ' . number_format($charges_tot, 2) . '</td>
                        <td style="text-align:center;">$ ' . number_format($estimated_tot, 2) . '</td>
                    </tr>
                    </tbody>
                </table>

                    ';

                $mpdf->WriteHTML($table_heading);
                $mpdf->WriteHTML($str);
//            if ($ctr2 != count($datum)) {
                $mpdf->AddPage();
//            }
                $ctr2++;
            }
            $mpdf->DeletePages($ctr2 + 1, $ctr2 + 1);

//        $mpdf->AddPage();
            $mpdf->Output('stats.pdf', 'I');
        
    }

    public function get_location_data() {
        $report_type = $this->input->post('report_type');
        $roles = $this->session->userdata("roles");
        $practiceId = $this->session->userdata('practice_id');
        //check roles
        if ($this->in_array_r('Super Administrator', $roles)) { //if Super ADmin
            //get all locations
            $locations = $this->userlocations->get_all();
        } elseif ($this->in_array_r('Practice Owner', $roles)) {//if Practice Owner
            //get practice locations
            $locations = $this->userlocations->get_all_location($practiceId);
        } else { // Office Administrator
            //get locations of admin only
            $locations = $this->session->userdata('locs');
        }

        $data = array(
            'location_data' => $locations,
        );
//        print_r($report_type);
        echo json_encode($data);
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function generate_aggregatesPDF() {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        include_once APPPATH . '/third_party/mpdf/mpdf.php';
        $user_id = $this->session->userdata('user_id');
        $practice_id = $this->userpractices->get_practice($user_id);
        $location = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);
        $location_id = $this->input->post('location_id');

        $practice_name = $this->practices->get_practice_by_id($practice_id[0]['practice_id']);
        $roles = $this->session->userdata('roles');
        if ($this->in_array_r("Super Administrator", $roles)) {
            
        } else if ($this->in_array_r("Practice Owner", $roles)) {

            if ($location_id == '0') {
                $user_id = $this->session->userdata('user_id');
                $practice_id = $this->userpractices->get_practice($user_id);
                $location = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);
                $practice_name = $this->practices->get_practice_by_id($practice_id[0]['practice_id']);
                $res = $this->get_all_clinician_agg_data($user_id, $start_date, $end_date, '0', false);
            } else {
                $office_admin_locations = $this->session->userdata('locs');
                $location_arr_id[0]['id'] = $location_id;
                $location_arr_id = $this->locations->get_location_by_id($location_id);
                $res = $this->get_all_clinician_agg_data($user_id, $start_date, $end_date, $location_id, false);
                $location = $location_arr_id;
            }
        } else if ($this->in_array_r("Office Administrator", $roles)) {

            if ($location_id == '0') {
                $office_admin_locations = $this->session->userdata('locs');
                $location_arr_id[0]['id'] = $office_admin_locations[0]['location_id'];
                $location_arr_id = $this->locations->get_location_by_id($office_admin_locations[0]['location_id']);
                $res = $this->get_all_clinician_agg_data($user_id, $start_date, $end_date, $location_arr_id, true);
                $location = $location_arr_id;
            } else if ($location_id != '0') {

                $office_admin_locations = $this->session->userdata('locs');
                $location_arr_id[0]['id'] = $location_id;
                $location_arr_id = $this->locations->get_location_by_id($location_id);
                $res = $this->get_all_clinician_agg_data($user_id, $start_date, $end_date, $location_arr_id, true);
                $location = $location_arr_id;
            }
        }



        if ($res) {
            ini_set('memory_limit', '64M');

            $mpdf = new mPDF('utf-8', //mode
                    'A4', //format
                    8, //fontsize
                    '', //fontfamily
                    15, //margin_left
                    15, //margin_right
                    16, //margin_top
                    16, //margin_bottom
                    9, //margin_header
                    9, //margin_footer
                    'L'//orientation
            );
            $mpdf->SetHeader($practice_name[0]['practice_name'] . " : Clinician Aggregates" . "||" . "Inclusive Dates: " . date('m/d/Y', strtotime($start_date)) . " to " . date('m/d/Y', strtotime($end_date)));
            $mpdf->SetFooter("Report Generated on " . date(DATE_RFC822) . '||Page {PAGENO}');

            $page_ctr = 0;
            $style = '<style>
                    table,th,td{
                        border:1px solid black;
                        border-collapse:collapse;
                    }

                    td{
                        text-align: right;
                    }
                    
                    
                  </style>';
            $mpdf->WriteHTML($style);
            foreach ($location as $loc_key => $loc) {
                $str = '<h4>Location: ' . $loc['location_name'] . '</h4>'
                        . '<table class="volume table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="12">Volume</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>New<br>Cases</th>
                            <th>Active<br>D/C</th>
                            <th>Passive<br>D/C</th>
                            <th>Active<br>Cases</th>
                            <th>Vists<br>Att.</th>
                            <th>Visits<br>Cancels</th>
                            <th>Visits<br>Sched.</th>
                            <th>Units<br>Charged</th>
                            <th>Days<br>Worked</th>
                            <th>Hours<br>Worked</th>
                            <th>Clinical<br>Hours<br>Sched</th>
                        </tr>';
                foreach ($res[$loc_key] as $datal) {

                    $str .= '<tr>
                            <td width="25%" style="text-align:left;">' . $datal['name'] . '</td>
                            <td width="6.8%">' . $datal['new_cases'] . '</td>
                            <td width="6.8%">' . $datal['active_discharges'] . '</td>
                            <td width="6.8%">' . $datal['passive_discharges'] . '</td>
                            <td width="6.8%">' . $datal['active_cases'] . '</td>
                            <td width="6.8%">' . $datal['visits_attended'] . '</td>
                            <td width="6.8%">' . $datal['no_show'] . '</td>
                            <td width="6.8%">' . $datal['visits_scheduled'] . '</td>
                            <td width="6.8%">' . number_format($datal['units_charged'], 2) . '</td>
                            <td width="6.8%">' . $datal['days_worked'] . '</td>
                            <td width="6.8%">' . number_format($datal['hours_worked'], 2) . '</td>
                            <td width="6.8%">' . number_format($datal['hours_scheduled'], 2) . '</td>
                        </tr>
                   ';
                }
                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);

                $str = '<table class="billing table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="4">Billing</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Charges</th>
                            <th>Estimated Collections</th>
                            <th>Billings Days</th>
                        </tr>';


                foreach ($res[$loc_key] as $datax) {

                    //$names = split(',', $datax['name']);
                    $str .= '<tr>   
                            <td style="text-align:left;" width="25%">' . $datax['name'] . '</td>
                            <td width="25%"> $ ' . number_format($datax['charges'], 2) . '</td>
                            <td width="25%"> $ ' . number_format($datax['estimated_collection'], 2) . '</td>
                            <td width="25%">' . $datax['billing_days'] . '</td>
                        </tr>
                   ';
                }
                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);


                $str = '<table class="unit-ratios table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="7">Unit Ratios</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Units / Visit</th>
                            <th>Units / Active Case</th>
                            <th>Units / Hour Worked</th>
                            <th>Units / 8 Hours Worked</th>
                            <th>Charge / Unit</th>
                            <th>Collected / Unit</th>
                        </tr>';
                foreach ($res[$loc_key] as $datax) {
                    $str .= '<tr>
                            <td style="text-align:left;" width="25%">' . $datax['name'] . '</td>
                            <td width="12.5%">' . $datax['unitsvisit'] . '</td>
                            <td width="12.5%">' . $datax['unitsactive'] . '</td>
                            <td width="12.5%">' . number_format($datax['unitshour'], 2) . '</td>
                            <td width="12.5%">' . number_format($datax['units8hour'], 2) . '</td>
                            <td width="12.5%">$ ' . number_format($datax['chargeunit'], 2) . '</td>
                            <td width="12.5%">$ ' . number_format($datax['collectedunit'], 2) . '</td>
                        </tr>
                   ';
                }
                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);

                $str = '<table class="visit-ratios table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="7">Visit Ratios</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Visits / Case</th>
                            <th>Visits / Hours Worked</th>
                            <th>Visits / 8 Hour Worked</th>
                            <th>Charges / Visit</th>
                            <th>Collected / Visit</th>
                        </tr>';
                foreach ($res[$loc_key] as $datax) {
                    $str .= '<tr>
                            <td style="text-align:left;" width="25%">' . $datax['name'] . '</td>
                            <td width="15%">' . $datax['visitscase'] . '</td>
                            <td width="15%">' . number_format($datax['visitshour'], 2) . '</td>
                            <td width="15%">' . number_format($datax['visits8hour'], 2) . '</td>
                            <td width="15%">$ ' . number_format($datax['chargesvisit'], 2) . '</td>
                            <td width="15%">$ ' . number_format($datax['collectedvisit'], 2) . '</td>
                        </tr>
                   ';
                }
                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);

                $str = '<table class="case-management-ratios table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="4">Case Management Ratios</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Active Cases / Day</th>
                            <th>Passive Discharge Rate</th>
                            <th>Attendance Rate</th>
                        </tr>';
                foreach ($res[$loc_key] as $datax) {
                    $str .= '<tr>
                            <td style="text-align:left;" width="25%">' . $datax['name'] . '</td>
                            <td width="25%">' . $datax['casesday'] . '</td>
                            <td width="25%">' . $datax['passiverate'] * 100 . '%</td>
                            <td width="25%">' . $datax['attendrate'] * 100 . '%</td>
                        </tr>
                   ';
                }
                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);


                $str = '<table class="financial-ratios table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="3">Financial Ratios</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Charge / Hour Worked</th>
                            <th>Collected / Hour Worked</th>
                        </tr>';
                foreach ($res[$loc_key] as $datax) {
                    $str .= '<tr>
                            <td style="text-align:left;" width="25%">' . $datax['name'] . '</td>
                            <td width="37.5%">$ ' . number_format($datax['chargeshour'], 2) . '</td>
                            <td width="37.5%">$ ' . number_format($datax['collectedhour'], 2) . '</td>
                        </tr>
                   ';
                }
                $str .= '</table><br/><br/>';
                $mpdf->WriteHTML($str, 2);

                $str = '<table class="staffing-ratios table table-bordered dashboard-table-pdf" width="100%">
                        <tr>
                            <th colspan="5">Staffing Ratios</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Units / Billing Day</th>
                            <th>Visits / Billing Day</th>
                            <th>Total Hrs Worked/ Days Worked</th>
                            <th>Schedule Density</th>
                        </tr>';
                foreach ($res[$loc_key] as $datax) {
                    $str .= '<tr>
                            <td style="text-align:left;" width="25%">' . $datax['name'] . '</td>
                            <td width="18.75%">' . $datax['unitsbilling'] . '</td>
                            <td width="18.75%">' . $datax['visitsbilling'] . '</td>
                            <td width="18.75%">' . $datax['thourdays'] . '</td>
                            <td width="18.75%">' . $datax['scheddensity'] . '</td>
                        </tr>
                   ';
                }
                $str .= '</table>';
                $mpdf->WriteHTML($str, 2);
                $mpdf->AddPage();
                $page_ctr++;
            }

//            $mpdf->DeletePages($page_ctr + 1, $page_ctr + 1);
            $count = count($mpdf->pages);
            $mpdf->DeletePages($count, $count);

//            $username = $this->users->get_user_by_id($user_id);
            $mpdf->Output('aggregates.pdf', 'I');
        } else {
            echo 'No data to generate';
        }
    }

    public function get_all_clinician_agg_data($user_id, $start_date, $end_date, $location_id, $is_office_admin) {
        if ($is_office_admin) {
            $location = $location_id;
        } else {
            if ($location_id == '0') {
                $practice_id = $this->userpractices->get_practice($user_id);
                $location = $this->locations->get_locs_under_practice($practice_id[0]['practice_id']);
            } else if ($location_id != '0') {
                $location[0]['id'] = $location_id;
            }
        }


        $data_per_loc = array();
        $data_array = array();
        foreach ($location as $location_key => $loc) {
            $include = false;
            $userIds = $this->userlocations->get_users_underlocation($loc['id']);


            foreach ($userIds as $user_key => $userid) {

                $data_per_loc[$location_key][$user_key] = $this->cliniciandata->get_all_clinician_data_by_date($loc['id'], $userid['user_id'], date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));

                $emp_id = 0;
                $fname = 0;
                $lname = 0;
                $tnewCases = 0;
                $taDis = 0;
                $tpDis = 0;
                $addCases = 0;
                $remCases = 0;
                $activeCases = 0;
                $vAttended = 0;
                $vScheduled = 0;
                $vCanceled = 0;
                $uCharged = 0;
                $dWorked = 0;
                $hWorked = 0;
                $hScheduled = 0;
                $collected = 0;
                $charges = 0;
                $bdays = 0;
                foreach ($data_per_loc[$location_key][$user_key] as $data) {
//get collection rate per data where date
                    if ($data || $data['hours_worked'] !== '' || $data['hours_worked'] !== null) {
                        $collectionrate = $this->collectionrates->get_collection_rate_by_date($loc['id'], $data['end_date']);
                        $emp_id = $data['employee_id'];
                        $fname = $data['first_name'];
                        $lname = $data['last_name'];
                        $tnewCases += $data['new_cases'];
                        $taDis += $data['active_discharges'];
                        $tpDis += $data['passive_discharges'];
                        $addCases += $data['added_cases'];
                        $remCases += $data['removed_cases'];
                        $activeCases = $data['active_cases'];
                        $vAttended += $data['visits_attended'];
                        $vScheduled += $data['visits_scheduled'];
                        $vCanceled += $data['no_show'];
                        $uCharged += $data['units_charged'];
                        $dWorked +=$data['days_worked'];
                        $hWorked += $data['hours_worked'];
                        $hScheduled +=$data['hours_scheduled'];
                        $collected += $data['charges'] * ( $collectionrate / 100);
                        $charges +=$data['charges'];
                        $bdays +=$data['billing_days'];
                        $include = true;
                    } else {
                        $include = false;
                    }
                }

                if ($include) {
//Unit Ratios
                    $unitsvisit = $this->divide($uCharged, $vAttended);
                    $unitsactive = $this->divide($uCharged, $activeCases);
                    $unitshour = $this->divide($uCharged, $hWorked);
                    $units8hour = $this->divide($uCharged, ($hWorked / 8));
                    $chargeunit = $this->divide($charges, $uCharged);
                    $collectedunit = $this->divide($collected, $uCharged);

//Visit Ratios
                    $visitscase = $this->divide($vAttended, $activeCases);
                    $visitshour = $this->divide($vAttended, $hWorked);
                    $visits8hour = $this->divide($vAttended, ($hWorked / 8));
                    $chargesvisit = $this->divide($charges, $vAttended);
                    $collectedvisit = $this->divide($collected, $vAttended);
                    $noshowvisit = $this->divide($vCanceled, $vAttended);

//Case Management
                    $casesday = $this->divide($activeCases, $dWorked);
                    $passiverate = $this->divide($tpDis, ($taDis + $tpDis));
                    $attendrate = $this->divide($vAttended, $vScheduled);
                    $pdisadis = $this->divide($tpDis, $taDis);

//Financial Ratios
                    $chargeshour = $this->divide($charges, $hWorked);
                    $collectedhour = $this->divide($collected, $hWorked);

//Staffing Ratios
                    $unitsbilling = $this->divide($uCharged, $bdays);
                    $visitsbilling = $this->divide($vAttended, $bdays);
                    $thourdays = $this->divide($hWorked, $dWorked);
                    $scheddensity = $this->divide($hScheduled, $hWorked);

                    $data_array[$location_key][$user_key]['name'] = strtoupper($lname) . ',' . '<br/>' . $fname . ' (' . $emp_id . ')';
                    $data_array[$location_key][$user_key]['new_cases'] = $tnewCases;
                    $data_array[$location_key][$user_key]['active_discharges'] = $taDis;
                    $data_array[$location_key][$user_key]['passive_discharges'] = $tpDis;
                    $data_array[$location_key][$user_key]['active_cases'] = $activeCases;
                    $data_array[$location_key][$user_key]['visits_attended'] = $vAttended;
                    $data_array[$location_key][$user_key]['no_show'] = $vCanceled;
                    $data_array[$location_key][$user_key]['visits_scheduled'] = $vScheduled;
                    $data_array[$location_key][$user_key]['units_charged'] = $uCharged;
                    $data_array[$location_key][$user_key]['days_worked'] = $dWorked;
                    $data_array[$location_key][$user_key]['hours_worked'] = $hWorked;
                    $data_array[$location_key][$user_key]['hours_scheduled'] = $hScheduled;
                    $data_array[$location_key][$user_key]['charges'] = $charges;
                    $data_array[$location_key][$user_key]['estimated_collection'] = $collected;
                    $data_array[$location_key][$user_key]['billing_days'] = $bdays;

                    $data_array[$location_key][$user_key]['unitsvisit'] = $unitsvisit;
                    $data_array[$location_key][$user_key]['unitsactive'] = $unitsactive;
                    $data_array[$location_key][$user_key]['unitshour'] = $unitshour;
                    $data_array[$location_key][$user_key]['units8hour'] = $units8hour;
                    $data_array[$location_key][$user_key]['chargeunit'] = $chargeunit;
                    $data_array[$location_key][$user_key]['collectedunit'] = $collectedunit;

                    $data_array[$location_key][$user_key]['visitscase'] = $visitscase;
                    $data_array[$location_key][$user_key]['visitshour'] = $visitshour;
                    $data_array[$location_key][$user_key]['visits8hour'] = $visits8hour;
                    $data_array[$location_key][$user_key]['chargesvisit'] = $chargesvisit;
                    $data_array[$location_key][$user_key]['collectedvisit'] = $collectedvisit;

                    $data_array[$location_key][$user_key]['noshowvisit'] = $noshowvisit;

                    $data_array[$location_key][$user_key]['casesday'] = $casesday;
                    $data_array[$location_key][$user_key]['passiverate'] = $passiverate;
                    $data_array[$location_key][$user_key]['attendrate'] = $attendrate;
                    $data_array[$location_key][$user_key]['pdisadis'] = $pdisadis;

                    $data_array[$location_key][$user_key]['chargeshour'] = $chargeshour;
                    $data_array[$location_key][$user_key]['collectedhour'] = $collectedhour;

                    $data_array[$location_key][$user_key]['unitsbilling'] = $unitsbilling;
                    $data_array[$location_key][$user_key]['visitsbilling'] = $visitsbilling;
                    $data_array[$location_key][$user_key]['thourdays'] = $thourdays;
                    $data_array[$location_key][$user_key]['scheddensity'] = $scheddensity;
                }
            }
//dito
        }
        return $data_array;
    }

}
