<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Edit_user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('collectionrates');
        $this->load->model('userlocations');
        $this->load->model('userpractices');
        $this->load->model('locations');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userroles');
        $this->load->model('roles');
        $this->load->model('positions');
        $this->load->model('userpositions');
        $this->load->model('mailer');
		$this->load->model('cliniciandatas');
    }

    public function pass_userid() {
        $this->session->set_userdata('edit_user_id', $this->input->post('user-id'));
        $data = array(
            'status' => 'success'
        );
        echo json_encode($data);
    }

    public function check_if_authorized() {

        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }

        $this->check_if_authorized();

        if (!$this->session->userdata('edit_user_id')) {
            redirect('user_maintenance');
        }
        
        

        $this->session->set_userdata('location_options', array()); //array for locations displayed
        $this->session->set_userdata('selected_locations', array());
        $practice = $this->userpractices->get_practice($this->session->userdata('edit_user_id'));
        $locations = $this->get_location_data();
        $locMarkup = $this->set_up_markup($locations);
        $userData = $this->get_user_data();
        $userRoles = $this->get_user_roles();
        $userPosition = $this->get_user_position();
        $locOptions = $this->get_location_options();
        $roleOptions = $this->roles->get_all_roles();
        $positionOptions = $this->positions->get_positions_by_practice($practice[0]['practice_id']);
        $countlocs = $this->count_locs();



        $data = array(
            'locMarkup' => $locMarkup,
            'userData' => $userData,
            'userRoles' => $userRoles,
            'userPosition' => $userPosition,
            'roleOptions' => $roleOptions,
            'locOptions' => $locOptions,
            'positionOptions' => $positionOptions,
            'countlocs' => $countlocs,
        );

        $this->load->vars($data);
        $this->template->set_layout('default');
        $this->template->title('Edit User');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
//        $this->template->append_metadata('<script src="' . base_url("js/admin-dashboard.js") . '"></script>');
        $this->template->build('pages/edit_user');
    }

    public function get_user_position() {
        $userId = $this->session->userdata('edit_user_id');
        $position = $this->userpositions->get_posdata($userId);
        return $position;
    }

    public function get_location_options() {
        $roles = $this->session->userdata('roles');
        $this->session->set_userdata('location_options', array());
		if ($this->in_array_r('Super Administrator', $roles)) {	
            $pid = $this->userpractices->get_practice($this->session->userdata('edit_user_id'));
            $locations = $this->userlocations->get_all_location_data($pid[0]['practice_id']);
        }  else if ($this->in_array_r('Practice Owner', $roles)) {
            $pid = $this->session->userdata('practice_id');
            $locations = $this->userlocations->get_all_location_data($pid);
        } else if ($this->in_array_r('Office Administrator', $roles)) {
            $locations = $this->userlocations->get_admin_locs($this->session->userdata('user_id'));
        } 

        $selectedLocations = $this->session->userdata('selected_locations');

        $options = '';
        foreach ($locations as $loc) {

            $locOptions = $this->session->userdata('location_options');
            array_push($locOptions, $loc);
            $this->session->set_userdata('location_options', $locOptions);

            if (!in_array($loc['id'], $selectedLocations)) {
                $options .= '<option value="' . $loc['id'] . '">' . $loc['location_name'] . '</option>';
            }
        }

        return $options;
    }

    public function count_locs() {
        $plocs = $this->session->userdata('location_options');
        $slocs = $this->session->userdata('selected_locations');

        if (count($plocs) == count($slocs)) {
            return 1;
        }

        return 0;
    }

    public function get_location_data() {
        $userId = $this->session->userdata('edit_user_id');
        $locationdata = $this->userlocations->get_locdata($userId);

        return $locationdata;
    }

    public function get_user_roles() {
        $userId = $this->session->userdata('edit_user_id');
        $userRoles = $this->userroles->get_role_by_id($userId);
        return $userRoles;
    }

    public function get_user_data() {
        $userId = $this->session->userdata('edit_user_id');
        $userData = $this->users->get_user_by_id($userId);
        return $userData;
    }

    public function set_up_markup($locations) {
        $markup = '';
        $rolenames = $this->session->userdata('roles');
        $adminlocs = $this->userlocations->get_admin_locs($this->session->userdata('user_id'));

        foreach ($locations as $loc) {
            if ($this->in_array_r('Office Administrator', $rolenames)  && !$this->in_array_r('Practice Owner', $rolenames)) {
                if ($this->in_array_r($loc['location_name'], $adminlocs)) {
                    $markup .= $this->load->view('templates/edit_user_location', $loc, true);

                    //insert the id of location displayed
                    $selLocations = $this->session->userdata('selected_locations');
                    array_push($selLocations, $loc['location_id']);
                    $this->session->set_userdata('selected_locations', $selLocations);
                }
            } else {
                //insert the id of location displayed
                $markup .= $this->load->view('templates/edit_user_location', $loc, true);

                //insert the id of location displayed
                $selLocations = $this->session->userdata('selected_locations');
                array_push($selLocations, $loc['location_id']);
                $this->session->set_userdata('selected_locations', $selLocations);
            }
        }
        return $markup;
    }

    public function add_markup($location) {
        $markup = $this->load->view('templates/edit_user_location_add', $location, true);
        return $markup;
    }

    public function add_location() {
        if ($this->validate_loc()) {

            //ADD the id of location selected
            $selLocations = $this->session->userdata('selected_locations');
            array_push($selLocations, $this->input->post('location-id'));
            $this->session->set_userdata('selected_locations', $selLocations);

            $location = array(
                'location_id' => $this->input->post('location-id'),
                'location_name' => $this->input->post('location-name'),
                'start_date' => $this->input->post('date-first-use'),
                'initial_cases' => $this->input->post('initial-cases'),
                'work_status' => 1,
                'termination_date' => '0000-00-00',
            );

            $markup = $this->add_markup($location);
            $locOptions = $this->get_location_options();

            $countlocs = $this->count_locs();

            echo json_encode(
                    array(
                        'status' => "success",
                        'id' => $this->input->post('location-id'),
                        'markup' => $markup,
                        'countlocs' => $countlocs,
                        'locOptions' => $locOptions
                    )
            );
        }
    }

    public function remove_location() {

        $locId = $this->input->post('location-id');
        $locIds = $this->session->userdata('selected_locations');

        if (($key = array_search($locId, $locIds)) !== false) {
            unset($locIds[$key]);
        }

        $this->session->set_userdata('selected_locations', $locIds);


        $locOptions = $this->get_location_options();
        $countlocs = $this->count_locs();
        echo json_encode(
                array(
                    'status' => "success",
                    'locOptions' => $locOptions,
                    'countlocs' => $countlocs,
                )
        );
    }

    public function validate_loc() {
        $rules = array(
            array(
                'field' => 'date-first-use',
                'label' => 'Date of First Use',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'initial-cases',
                'label' => 'Initial Cases',
                'rules' => 'trim|required'
            ),
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error',
                'date' => form_error('date-first-use'),
                'initial' => form_error('initial-cases'),
            );
            echo json_encode($data);
        } else {

            return true;
        }
    }

    public function validate_entry($from = '') {

        $rules = array(
            array(
                'field' => 'emp-id',
                'label' => 'Employee ID',
                'rules' => 'trim|required|callback_check_empid'
            ),
            array(
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ), array(
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ), array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|callback_check_email'
            ),
            array(
                'field' => 'roles',
                'label' => 'Role',
                'rules' => 'callback_role_check'
            ),
        );
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has already been used.');

        $noloc = '';
        $valDate = '';
        $valCase = '';
        $termdate = '';
        $error = true;

        if (!$this->input->post('location-id')) {
            $noloc = 'noloc';
            $error = false;
        } else {
            $vDate = $this->check_dates();
            if ($vDate) {
                $valDate = $vDate;
                $error = false;
            }
            if ($this->input->post('roles')) {
                $vCase = $this->check_cases();
                if ($vCase) {
                    $valCase = $vCase;
                    $error = false;
                }
            }
            $tdate = $this->check_term_date();

            if ($tdate) {
                $termdate = $tdate;
                $error = false;
            }
        }

        if ($this->form_validation->run() == false || $error == false) {
            $data = array(
                'status' => 'error',
                'empid' => form_error('emp-id'),
                'fname' => form_error('first-name'),
                'lname' => form_error('last-name'),
                'email' => form_error('email'),
                'role' => form_error('roles'),
                'noloc' => $noloc,
                'valdate' => $valDate,
                'valcase' => $valCase,
                'termdate' => $termdate,
            );

            if ($from == '') {
                echo json_encode($data);
            } else {
                return $data;
            }
        } else {//validation success weehee
            $this->update_user_info();
            $this->update_user_locs();
            $this->update_user_roles();
            $this->update_user_pos();

            $data = array(
                'status' => 'success',
            );
            if ($from == '') {
                echo json_encode($data);
            } else {
                return $data;
            }
        }
    }

    public function update_user_info() {
        $userId = $this->input->post('user-id');
        $data = array(
            'employee_id' => "'" . $this->input->post('emp-id') . "'",
            'first_name' => "'" . $this->input->post('first-name') . "'",
            'last_name' => "'" . $this->input->post('last-name') . "'",
            'email' => "'" . $this->input->post('email') . "'",
        );

        $this->users->update_user($data, $userId);
    }

    public function update_user_locs() {
        $userId = $this->input->post('user-id');
        $locIds = $this->input->post('location-id');
        $startDate = $this->input->post('date-first-use');
        $initialCases = $this->input->post('initial-cases');
        $workStatus = $this->input->post('work-status');
        $termDate = $this->input->post('termination-date');
        $index = 0;
        $existinglocations = array();


        if ($locIds) {
            foreach ($locIds as $id) {
                //get userloc where userid and locationid

                if (in_array('4', $this->input->post('roles'))) {
                    $cases = $initialCases[$index];
                } else {
                    $cases = 0;
                }

                if ($workStatus[$id] == 0) {
                    $tdate = "'" . date('Y-m-d', strtotime($termDate[$index])) . "'";
                    $wstatus = 0;
                } else {
                    $tdate = '0000-00-00';
                    $wstatus = 1;
                }

                $startdate = "'" . date('Y-m-d', strtotime($startDate[$index])) . "'";
                $exists = $this->userlocations->get_locs_by_userid_locid($userId, $id);

                if ($exists) {
                    //update userloc where userid and locationid
					//update clinician data of user
                    $oldcases = $this->userlocations->get_initial_cases($id,$userId);
                    $newcases = $cases;
                    $casesdif=$newcases - $oldcases;
                    $this->cliniciandatas->update_active_cases($id,$userId,$casesdif);
					
					
                    $data = array(
                        'work_status' => $wstatus, //1 = active, 0 = terminated
                        'initial_cases' => $cases,
                        'start_date' => $startdate,
                        'termination_date' => $tdate,
                    );
                    $existinglocations[] = $id;
                    $this->userlocations->update_userloc($data, $userId, $id);
                } else {
                    //insert
                    $data = array(
                        'user_id' => $userId,
                        'location_id' => $id,
                        'work_status' => $wstatus, //1 = active, 0 = terminated
                        'initial_cases' => $cases,
                        'start_date' => $startdate,
                        'termination_date' => $tdate,
                    );

                    $existinglocations[] = $id;
                    $this->userlocations->save_userlocation($data);
                }
                $index++;
            }

            //check if locations exists else delete them
            $previouslocs = $this->userlocations->get_locids($userId);
            $adminlocs = $this->userlocations->get_adminlocids($this->session->userdata('user_id'));
            $adminlocids = array();
            foreach ($adminlocs as $adloc) {
                $adminlocids[] = $adloc['location_id'];
            }

            if ($this->in_array_r('Office Administrator', $this->session->userdata('roles'))  && !$this->in_array_r('Practice Owner', $this->session->userdata('roles'))) {
                foreach ($previouslocs as $eid) {
                    if (!in_array($eid[0], $existinglocations) && in_array($eid[0], $adminlocids)) {
                        $this->userlocations->delete_userloc($userId, $eid[0]);
                    }
                }
            } else {
                foreach ($previouslocs as $eid) {
                    if (!in_array($eid[0], $existinglocations)) {
                        $this->userlocations->delete_userloc($userId, $eid[0]);
                    }
                }
            }

            //update employment status
            $existsActiveLoc = $this->userlocations->get_loc_active($userId);
            if (!$existsActiveLoc) {
                $data = array(
                    'activation_status'=>3,
                    'employment_status' => 0
                );

                $this->users->update_user($data, $userId);
            } else {
                $data = array(
                    'activation_status'=>1,
                    'employment_status' => 1
                );
                $this->users->update_user($data, $userId);
            }
        } else {
            $this->userlocations->delete_all_userloc($userId);
        }
    }

    public function update_user_roles() {
        $userId = $this->input->post('user-id');

        $newroles = $this->input->post('roles');

        if ($newroles) {
            foreach ($newroles as $nrid) {
                $exists = $this->userroles->get_user_role_by_userid_roleid($userId, $nrid);
                if (!$exists) {
                    //insert
                    $data = array(
                        'user_id' => $userId,
                        'role_id' => $nrid,
                    );
                    $this->userroles->save_userrole($data);
                }
            }

            $previousroles = $this->userroles->get_user_roles($userId);
            foreach ($previousroles as $pried) {
                if (!in_array($pried[0], $newroles)) {
                    $this->userroles->delete_userrole($userId, $pried[0]);
                }
            }
        } else {
            $this->userroles->delete_all_roles($userId);
        }
    }

    public function update_user_pos() {
        $userId = $this->input->post('user-id');
        $pos = $this->input->post('position');


        $data = array(
            'position_id' => $pos,
        );
        $this->userpositions->update_userpos($data, $userId);
    }

    public function check_dates() {
        $dates = $this->input->post('date-first-use');
        $locations = $this->input->post('location-id');
        $index = 0;
        $val = array();
        foreach ($locations as $loc) {
            if ($dates[$index] == '') {
                $val[] = 'date_' . $loc;
            }

            $index++;
        }
        $array = $this->input->post('array');

        return $val;
    }

    public function check_cases() {
        $cases = $this->input->post('initial-cases');
        $locations = $this->input->post('location-id');
        $roles = $this->input->post('roles');
        $index = 0;
        $val = array();
        foreach ($locations as $loc) {
            if ($this->in_array_r(4, $roles)) {
                if ($cases[$index] == '') {
                    $val[] = 'case_' . $loc;
                }
            }

            $index++;
        }

        return $val;
    }

    public function role_check() {
        if ($this->input->post('roles')) {
            return true;
        } else {
            $this->form_validation->set_message('role_check', 'Please select at least 1 %s.');
            return false;
        }
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function check_email() {
        $email = $this->input->post('reg-email');
        $userId = $this->input->post('user-id');

        $isUnique = $this->users->get_user_by_email($email);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $userId) {
                return true;
            } else {
                $this->form_validation->set_message('check_email', 'The %s is already registered to a different account.');
                return false;
            }
        }
    }

    public function check_term_date() {
        $termdates = $this->input->post('termination-date');
        $locations = $this->input->post('location-id');
        $workstat = $this->input->post('work-status');
        $index = 0;
        $vdate = array();

        foreach ($locations as $loc) {
            if ($workstat[$loc] == 0) {//if not active in the location
                if ($termdates[$index] == '') {
                    $vdate[] = 'tdate_' . $loc;
                }
            }

            $index++;
        }

        return $vdate;
    }

    public function check_empid() {
        $empId = $this->input->post('emp-id');
        $userId = $this->input->post('user-id');

        $isUnique = $this->users->get_user_by_empid($empId);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $userId) {
                return true;
            } else {
                $this->form_validation->set_message('check_empid', 'The %s has already been used.');
                return false;
            }
        }
    }

    //for admin pending users ----------------------------------------------------------------------------------
    public function save_user_admin() {
        $rules = array(
            array(
                'field' => 'emp-id',
                'label' => 'Employee ID',
                'rules' => 'trim|callback_check_empid'
            ),
            array(
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ), array(
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ), array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|callback_check_email'
            ),
        );
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has already been used.');

        $valDate = '';
        $valCase = '';
        $termdate = '';
        $error = true;

        if ($this->input->post('location-id')) {
            $vDate = $this->check_dates();
            if ($vDate) {
                $valDate = $vDate;
                $error = false;
            }

            if ($this->input->post('roles')) {
                $vCase = $this->check_cases();
                if ($vCase) {
                    $valCase = $vCase;
                    $error = false;
                }
            }

            $tdate = $this->check_term_date();

            if ($tdate) {
                $termdate = $tdate;
                $error = false;
            }
        }
        if ($this->form_validation->run() == false || $error == false) {
            $data = array(
                'status' => 'error',
                'empid' => form_error('emp-id'),
                'fname' => form_error('first-name'),
                'lname' => form_error('last-name'),
                'email' => form_error('email'),
                'valdate' => $valDate,
                'valcase' => $valCase,
                'termdate' => $termdate,
            );
            echo json_encode($data);
        } else {//validation success
            //update or insert certain information
            $this->update_user_info();
            $this->update_user_pos();
            $this->update_user_roles();
            $this->update_user_locs();
            $data = array(
                'status' => 'success',
            );
            echo json_encode($data);
        }
    }

    //save user data plus send invititation
    public function save_user_invite() {

        $data = $this->validate_entry('admin-invite'); //save data
        if ($data['status'] == 'success') {

            $this->send_invite(); //invite user
            echo json_encode($data);
        } else {
            echo json_encode($data);
        }
    }

    public function update_activation_status($hash) {
        $userId = $this->input->post('user-id');
        $data = array(
            'activation_status' => 0,
            'validation_code' => "'" . $hash . "'"
        );

        $this->users->update_user($data, $userId);
    }

    //send invitation
    public function send_invite() {
        $status = 'senderror';
        $hash = $this->create_validationcode($this->input->post('email'));
        $pid = $this->userpractices->get_practice($this->input->post('user-id'));
        $practicename = $this->practices->get_practice_by_id($pid[0]['practice_id']);
        $owner = $this->users->get_user_by_id($practicename[0]['user_id']);
        $data = array(
            'headerBgLink' => base_url() . 'img/nav-bg.png',
            'firstName' => $this->input->post('first-name'),
            'lastName' => $this->input->post('last-name'),
            'hash' => $hash,
            'pName' => $practicename[0]['practice_name'],
            'siteurl' => site_url(),
            'fromname' => $owner[0]['first_name'] . ' ' . $owner[0]['last_name'],
        );

        $mailBody = $this->load->view('templates/user_invitation', $data, true);
        $send = $this->mailer->send_mail(//send mail function in mailer.php
                $this->input->post('email'), //destination email
                'Performance Tracker Invitation', //subject
                $mailBody//email body
        );

        if ($send) {
			//CC to prosoft-phils
			$this->mailer->send_mail(//send mail function in mailer.php
                'support@prosoft-phils.com', //destination email
                'Performance Tracker - Invitation', //subject
                $mailBody//email body
			);
			
				/*
			//CC to test
				$this->mailer->send_mail(//send mail function in mailer.php
                ' prosoft.ph@gmail.com', //destination email
                'Performance Tracker - Invitation', //subject
                $mailBody//email body
			);
			*/
		
            $this->update_activation_status($hash);
            return true;
        } else {
            return false;
        }
    }

    public function create_validationcode($salt) {
        // Read the user agent, IP address, current time, and a random number:
        $data = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] .
                time() . rand() . $salt;
        // Return this value hashed via sha1
        return sha1($data);
    }

    public function resend_invite() {
        if ($this->send_invite()) {
            $data = array(
                'status' => 'success',
            );
            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'error',
            );
            echo json_encode($data);
        }
    }

    //positions
    public function create_pos() {
        $rules = array(
            array(
                'field' => 'position-code',
                'label' => 'Position Code',
                'rules' => 'trim|required|is_unique[tblposition.position_code]'
            ),
            array(
                'field' => 'position-name',
                'label' => 'Position Name',
                'rules' => 'trim|required|is_unique[tblposition.position_name]'
            )
        );

        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has already been used.');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error',
                'pcode' => form_error('position-code'),
                'pname' => form_error('position-name'),
            );
            echo json_encode($data);
        } else {

            $this->save_pos();
            $markup = $this->pos_markup();
            $data = array(
                'status' => 'success',
                'posmarkup' => $markup,
            );

            echo json_encode($data);
        }
    }

    public function save_pos() {
        $data = array(
            'position_code' => "'" . strtoupper($this->input->post('position-code')) . "'",
            'position_name' => "'" . $this->input->post('position-name') . "'",
            'practice_id' => $this->session->userdata('practice_id'),
            'is_clinician' => 0,
        );

        $this->positions->save_pos($data);
    }

    public function pos_markup() {
        $pos = $this->positions->get_positions_by_practice($this->session->userdata('practice_id'));
        $markup = '';
        foreach ($pos as $p) {
            $markup .= '<option value="' . $p['id'] . '">' . $p['position_name'] . ' (' . $p['position_code'] . ')</option>';
        }
        return $markup;
    }

}
