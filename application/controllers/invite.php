	<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invite extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');

        $this->load->model('positions');
        $this->load->model('mailer');
    }

    public function check_if_authorized() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles)
//                || $this->in_array_r("Practice Owner", $roles) 
//                || $this->in_array_r("Office Administrator", $roles) 
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        $this->check_if_authorized();

        $data = $this->positions->get_all_positions();
        $roles = $this->session->userdata("roles");
        $this->load->vars('positions', $data);
        $this->template->set_layout('default');
        $this->template->title('Invite');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/invite.js") . '"></script>');
        $this->template->build('pages/invite');
    }

    public function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function entry_validation() {
        $rules = array(
            array(
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|is_unique[tblusers.email]'
            ),
            array(
                'field' => 'confirmemail',
                'label' => 'Confirm Email Address',
                'rules' => 'trim|required|valid_email|matches[email]'
            ),
            array(
                'field' => 'practice-name',
                'label' => 'Practice Name',
                'rules' => 'trim|required|is_unique[tblpractice.practice_name]'
            ),
            array(
                'field' => 'practice-code',
                'label' => 'Practice Code',
                'rules' => 'trim|required|is_unique[tblpractice.practice_code]'
            ),
        );

        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'The %s has already been used.');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
                'firstname' => form_error('first-name'),
                'lastname' => form_error('last-name'),
                'email' => form_error('email'),
                'confirmemail' => form_error('confirmemail'),
                'practicename' => form_error('practice-name'),
                'practicecode' => form_error('practice-code')
            );

            echo json_encode($data);
        } else {
            //1st level validation success
            //2nd level validation

            $this->send_invitation();
        }
    }

    public function send_invitation() {
        $status = 'senderror';
        $hash = $this->create_validationcode($this->input->post('email'));

        $data = array(
            'pbLogoLink' => base_url() . 'img/pb-logo-2.jpg',
            'headerBgLink' => base_url() . 'img/nav-bg.png',
            'firstName' => $this->input->post('first-name'),
            'lastName' => $this->input->post('last-name'),
            'hash' => $hash,
            'siteurl' => site_url()
        );

        $mailBody = $this->load->view('templates/owner_invitation', $data, true);
        $send = $this->mailer->send_mail(//send mail function in mailer.php
                $this->input->post('email'), //destination email
                'Performance Tracker Invitation', //subject
                $mailBody//email body
        );

        if ($send) {
            //sending email success
            //save user data
			
			//CC to prosoft-phils
			$this->mailer->send_mail(//send mail function in mailer.php
                'support@prosoft-phils.com', //destination email
                'Performance Tracker - Invitation', //subject
                $mailBody//email body
			);
		
			/*
			//CC to test
				$this->mailer->send_mail(//send mail function in mailer.php
                ' prosoft.ph@gmail.com', //destination email
                'Performance Tracker - Invitation', //subject
                $mailBody//email body
			);	*/
			
			
            $status = 'success';

            $this->save_user($hash); //save user
            $userId = mysql_insert_id(); //get last inserted user 
            $this->save_userrole($userId); //save user role
            $this->save_userposition($userId); //save user position
            $this->save_practice($userId); //save practice
            $practiceId = mysql_insert_id(); //get last inserted practice id
            $this->save_userpractice($userId, $practiceId);
        }

        echo json_encode(
                array(
                    "status" => $status
                )
        );
    }

    function create_validationcode($salt) {
        // Read the user agent, IP address, current time, and a random number:
        $data = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] .
                time() . rand() . $salt;
        // Return this value hashed via sha1
        return sha1($data);
    }

    function save_user($hash) {
        $data = array(
            'first_name' => '"' . $this->input->post('first-name') . '"',
            'last_name' => '"' . $this->input->post('last-name') . '"',
            'email' => '"' . $this->input->post('email') . '"',
            'validation_code' => '"' . $hash . '"'
        );

        $this->users->save_userdata($data);
    }

    function save_userrole($userId) {
        $data = array(
            'user_id' => $userId,
            'role_id' => 2
        );

        $this->userroles->save_userrole($data);
        
    }

    function save_userposition($userId) {
        $data = array(
            'user_id' => $userId,
            'position_id' => $this->input->post('position')
        );

        $this->userpositions->save_userposition($data);
    }

    function save_practice($userId) {
        $data = array(
            'user_id' => $userId,
            'practice_name' => '"' . $this->input->post('practice-name') . '"',
            'practice_code' => '"' . $this->input->post('practice-code') . '"'
        );

        $this->practices->save_practice($data);
    }

    function save_userpractice($userId, $practiceId) {
        $data = array(
            'user_id' => $userId,
            'practice_id' => $practiceId
        );

        $this->userpractices->save_userpractice($data);
    }

}
