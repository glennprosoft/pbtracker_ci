<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_create_user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('practices');
        $this->load->model('locations');
        $this->load->model('roles');
        $this->load->model('positions');
        $this->load->model('users');
        $this->load->model('userpractices');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userlocations');
    }

    public function check_if_authorized() {

        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles)
//                || $this->in_array_r("Office Administrator", $roles) 
//                || $this->in_array_r("Practice Owner", $roles) 
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }

        $this->check_if_authorized();

        $practices = $this->get_practices();
        $pracId = $practices[0]['id']; //get the first ID in the practice list as default
        $locations = $this->get_locations($pracId);
        $roles = $this->get_roles($pracId);
        $positions = $this->get_positions($pracId);


        $data = array(
            'practices' => $practices,
            'locations' => $locations,
            'roles' => $roles,
            'positions' => $positions,
        );

        $this->load->vars($data);
        $this->template->set_layout('default');
        $this->template->title('Create User');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->build('pages/admin_create_user');
    }

    public function get_practices() {
        $practices = $this->practices->get_all_practice();
        return $practices;
    }

    public function get_locations($practice_id) {
        $locations = $this->locations->get_locs_under_practice($practice_id);

        return $locations;
    }

    public function get_positions($practice_id) {
        $positions = $this->positions->get_positions_by_practice($practice_id);

        return $positions;
    }

    public function get_roles($practice_id) {
        $roles = $this->roles->get_practice_roles($practice_id);

        return $roles;
    }

    public function generate_locations() {
        $pid = $this->input->post('practice_id');
        $locations = $this->get_locations($pid);
        $loctable = '';
        foreach ($locations as $loc) {
            $loctable .= '<tr>';
            $loctable .= '<td style="vertical-align: middle !important">';
            $loctable .= '<input type="checkbox" name="location[]" class="location-item" type="checkbox" value="' . $loc['id'] . '">  ';
            $loctable .= '</td>';
            $loctable .= '<td style="vertical-align: middle !important">';
            $loctable .= $loc['location_name'];
            $loctable .= '</td>';
            $loctable .= '<td style="vertical-align: middle !important" class="initial-cases">';
            $loctable .='<input style="width:140px; margin:auto;" uid="' . $loc['id'] . '"disabled type="text" class="form-control initial-cases-input" name="initial-cases[' . $loc['id'] . ']" value="0" placeholder="Initial Cases">';
            $loctable .= '</td>';
            $loctable .= '<td style="vertical-align: middle !important">';
            $loctable .= '<input  style="width:140px; margin:auto;" uid="' . $loc['id'] . '" disabled type="text" class="form-control date-first date-first-input" name="date-first-use[' . $loc['id'] . ']" placeholder="Date of First Use">';
            $loctable .= '</td>';
            $loctable .= '</tr>';
        }

        $pos = $this->pos_markup();
        
        $data = array(
            'locations' => $loctable,
            'posmarkup' => $pos,
        );
        
        echo json_encode($data);
    }

    public function check_entry() {
        $rules = array(
            array(
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|is_unique[tblusers.email]'
            ),
            array(
                'field' => 'emp-id',
                'label' => 'Employee ID',
                'rules' => 'trim|is_unique[tblusers.employee_id]'
            ),
        );

        $success = true;
        $validateinitial = array();
        $validatedate = array();
        $locs = $this->input->post('location');
        $initialcases = $this->input->post('initial-cases');
        $datefirst = $this->input->post('date-first-use');

        $roles = $this->input->post('roles');
        if ($locs) {

            foreach ($locs as $loc) {
                if ($roles && in_array(4, $roles)) {
                    if ($initialcases[$loc] == '' || $initialcases[$loc] == null) {
                        $success = false;
                        $validateinitial[] = 'input[name="initial-cases[' . $loc . ']"]';
                    }
                    if ($datefirst[$loc] == '' || $datefirst[$loc] == null) {
                        $success = false;
                        $validatedate[] = 'input[name="date-first-use[' . $loc . ']"]';
                    }
                } else {
                    if ($datefirst[$loc] == '' || $datefirst[$loc] == null) {
                        $success = false;
                        $validatedate[] = 'input[name="date-first-use[' . $loc . ']"]';
                    }
                }
            }
        }


        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'The %s has already been used.');
        if ($this->form_validation->run() == false || $success == false) {
            $data = array(
                'status' => 'error',
                'firstname' => form_error('first-name'),
                'lastname' => form_error('last-name'),
                'email' => form_error('email'),
                'empid' => form_error('emp-id'),
                'initialcases' => $validateinitial,
                'datefirst' => $validatedate,
            );

            echo json_encode($data);
        } else {//validation complete
            $this->save_user();
            $uid = mysql_insert_id();
            $this->save_userprac($uid);
            $this->save_userpos($uid);
            $this->save_userrole($uid);
            $this->save_userloc($uid);

            $data = array(
                'status' => 'success',
            );
            echo json_encode($data);
        }
    }

    public function save_user() {
        $date = date('Y-m-d');
        $data = array(
            'employee_id' => "'" . $this->input->post('emp-id') . "'",
            'first_name' => "'" . $this->input->post('first-name') . "'",
            'last_name' => "'" . $this->input->post('last-name') . "'",
            'email' => "'" . $this->input->post('email') . "'",
            'activation_status' => 2,
            'password_reset_date' => "'" . $date . "'",
        );

        $this->users->save_userdata($data);
    }

    public function save_userprac($userid) {
        $data = array(
            'user_id' => $userid,
            'practice_id' => $this->input->post('practice'),
        );
        $this->userpractices->save_userpractice($data);
    }

    public function save_userpos($userid) {
        $data = array(
            'user_id' => $userid,
            'position_id' => $this->input->post('position'),
        );

        $this->userpositions->save_userposition($data);
    }

    public function save_userrole($userid) {
        $role = $this->input->post('roles');
        if ($role) {
            foreach ($role as $roleid) {
                $data = array(
                    'user_id' => $userid,
                    'role_id' => $roleid,
                );
                $this->userroles->save_userrole($data);
            }
        }
    }

    public function save_userloc($userid) {
        $locid = $this->input->post('location');
        $cases = $this->input->post('initial-cases');
        $datefirst = $this->input->post('date-first-use');
        $role = $this->input->post('roles');
        if ($locid) {
            foreach ($locid as $loc) {
                $date = date('Y-m-d', strtotime($datefirst[$loc]));
                if ($role) {
                    if (in_array(4, $role)) {
                        $data = array(
                            'user_id' => $userid,
                            'location_id' => $loc,
                            'initial_cases' => $cases[$loc],
                            'workstatus' => 1,
                            'start_date' => "'" . $date . "'",
                        );
                    } else {
                        $data = array(
                            'user_id' => $userid,
                            'location_id' => $loc,
                            'initial_cases' => 0,
                            'workstatus' => 1,
                            'start_date' => "'" . $date . "'",
                        );
                    }
                } else {
                    $data = array(
                        'user_id' => $userid,
                        'location_id' => $loc,
                        'initial_cases' => 0,
                        'workstatus' => 1,
                        'start_date' => "'" . $date . "'",
                    );
                }


                $this->userlocations->save_userlocation($data);
            }
        }
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function pos_markup() {
        $pos = $this->positions->get_positions_by_practice($this->input->post('practice'));
        $markup = '';
        foreach ($pos as $p) {
            $markup .= '<option value="' . $p['id'] . '">' . $p['position_name'] . ' (' . $p['position_code'] . ')</option>';
        }
        return $markup;
    }

}
