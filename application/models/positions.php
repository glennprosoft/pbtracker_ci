<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Positions extends Table {

       function __construct()
    {
        parent::__construct();
    }
    
    public function get_all_positions() {
        $this->select();
        $this->from('tblposition');
        $query = $this->get();
        return $query;
    }
    
    public function get_positions_by_practice($practiceId){        
        $this->select();
        $this->from('tblposition');
        $this->where('practice_id = '.$practiceId . ' OR practice_id=0');
        $query = $this->get();
        return $query;
    }
    
    public function save_pos($data){
           $result = $this->insert('tblposition', $data);
        return $result;
    }
    
   
    

}
