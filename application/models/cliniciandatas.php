<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cliniciandatas extends Table {

    public function get_graph_data($userId, $locationId, $sDate, $eDate) {

        $query = $this->db->query('SELECT t1.*, t2.first_name,t2.last_name, t2.employee_id '
                . 'FROM tblcliniciandata AS t1 '
                . 'JOIN tblusers AS t2 ON t1.user_id = t2.id '
                . 'WHERE t1.user_id=' . $userId . ' AND t1.location_id = ' . $locationId . ' AND t1.start_date >= ' . "'" . $sDate . "'" . ' AND t1.end_date <= ' . "'" . $eDate . "'" . ' ORDER BY t1.start_date ASC');
        return $query->result_array();
    }

    public function get_clinician_data($uid, $locationId, $sDate, $eDate) {
        $query = $this->db->query('SELECT t1.*, t2.first_name,t2.last_name, t2.employee_id '
                . 'FROM tblcliniciandata AS t1 '
                . 'JOIN tblusers AS t2 ON t1.user_id = t2.id '
                . 'WHERE t1.user_id=' . $uid . ' AND t1.location_id = ' . $locationId . ' AND t1.start_date >= ' . "'" . $sDate . "'" . ' AND t1.end_date <= ' . "'" . $eDate . "'");
        return $query->result_array();
    }

    public function update_charges($charge, $uid, $date, $loc) {
        $data = array(
            'charges' => round($charge, 2),
            'billing_days' => 1,
        );

        $where = array(
            'user_id' => $uid,
            'location_id' => $loc,
            'start_date' => "'" . $date . "'",
            'end_date' => "'" . $date . "'",
        );

        $result = $this->update('tblcliniciandata', $data, $where);
        return $result;
    }

    public function update_active_cases($locid, $userid, $casesdif) {
        $query = $this->db->query('UPDATE tblcliniciandata SET'
                . ' active_cases=active_cases+' . $casesdif
                . ' WHERE user_id=' . $userid
                . ' AND location_id=' . $locid);
    }

    public function save_clinician_data($data) {
        $result = $this->insert('tblcliniciandata', $data);
        return $result;
    }
    
    public function get_previous_cases($uid, $date,$locid){
        $this->select();
        $this->from('tblcliniciandata');
        $this->where('user_id='.$uid.' AND location_id='.$locid.' AND end_date <'. "'".$date."'");
        $this->orderby('end_date', 'DESC'); 
        $query = $this->get();
        return $query[0]['active_cases'];
    }
    
    public function get_data_date($uid, $date, $locid){
        $this->select();
        $this->from('tblcliniciandata');
        $this->where('user_id='.$uid.' AND location_id='.$locid.' AND end_date ='. "'".$date."'");
        $query = $this->get();
        return $query;
    }
    
    public function checkbilling($uid, $date, $locid){
        $this->select();
        $this->from('tblcliniciandata');
        $this->where('user_id='.$uid.' AND location_id='.$locid.' AND end_date ='. "'".$date."'");
        $query = $this->get();
        return $query[0]['billing_days'];
    }
    
    public function delete_cliniciandata($uid, $date, $locid){
          $query = $this->db->query('DELETE from tblcliniciandata '
                  . 'WHERE user_id='.$uid.''
                  . ' AND end_date='."'".$date."'".''
                  . ' AND location_id='.$locid);
          
    }
    
    public function get_daysworked($uid, $date, $locid){
         $this->select();
        $this->from('tblcliniciandata');
        $this->where('user_id='.$uid.' AND location_id='.$locid.' AND end_date ='. "'".$date."'");
        $query = $this->get();
        return $query[0]['days_worked'];
    }

    public function remove_billing($uid, $date, $locid){
        $query = $this->db->query('UPDATE tblcliniciandata SET'
                . ' charges=0,billing_days=0 WHERE user_id=' . $userid
                . ' AND location_id=' . $locid);
    }
}
