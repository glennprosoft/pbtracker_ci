<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userroles extends Table {

    function __construct() {
        parent::__construct();
    }

    public function get_role_by_id($userId) {
        $query = $this->db->query('SELECT * FROM tbluserrole '
                . 'INNER JOIN tblrole ON tbluserrole.role_id = tblrole.id '
                . 'WHERE tbluserrole.user_id = ' . $userId);
        return $query->result_array();
    }

    public function get_user_roles($userId) {
        $this->select('role_id');
        $this->from('tbluserrole');
        $this->where('user_id = ' . $userId);
        $query = $query = $this->get();
        return $query;
    }

    public function get_user_role_by_userid_roleid($userId, $roleId) {
        $this->select();
        $this->from('tbluserrole');
        $this->where('user_id = ' . $userId . ' AND role_id = '.$roleId);
        $query = $query = $this->get();
        return $query;
    }
    
    public function save_userrole($data) {
        $result = $this->insert('tbluserrole', $data);
        return $result;
    }

    public function delete_userrole($userId, $roleId) {
        $query = $this->db->query('DELETE FROM tbluserrole
                                   WHERE user_id = ' . $userId . ' AND role_id = ' . $roleId);
        return;
    }
    
    public function delete_all_roles($userId){
          $query = $this->db->query('DELETE FROM tbluserrole
                                   WHERE user_id = ' . $userId );
        return;
    }

}
