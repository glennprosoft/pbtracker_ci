<?php

class Roles extends Table {

    function __construct() {
        parent::__construct();
    }

    public function get_rolename_by_id($roleId) {
        $this->select('role_name');
        $this->from('tblrole');
        $this->where('id = ' . "'" . $roleId . "'");
        $query = $this->get();
        return $query;
    }

    public function get_all_roles() {
        $this->select();
        $this->from('tblrole');
        $query = $this->get();
        return $query;
    }

    public function get_practice_roles($practiceId) {
        $this->select();
        $this->from('tblrole');
        $this->where('practice_id = 0 OR practice_id ='. $practiceId);
        $query = $this->get();
        return $query;
    }
    
    
    
    public function insert_role_fields($data){
        $query = $this->insert($table, $data);
        return $query;
    }

}
