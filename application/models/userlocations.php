<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userlocations extends Table {

    public function save_userlocation($data) {
        $result = $this->insert('tbluserlocation', $data);
        return $result;
    }

    public function get_all() {
        $this->select('*, id AS location_id');
        $this->from('tbllocation');
        $query = $this->get();
        return $query;
    }

    public function get_users_underloc($locationId) {
        $query = $this->db->query('SELECT t1.user_id, t2.* FROM tbluserlocation AS t1 
                                    JOIN tblusers AS t2 ON t1.user_id = t2.id
                                    WHERE t1.location_id = ' . $locationId .
                                                    ' GROUP BY t2.id ORDER BY t2.last_name');
        return $query->result_array();
        
//        $this->select('tbluserlocation.user_id, tblusers.*');
//        $this->from('tbluserlocation');
//        $this->join('tblusers', 'tblusers.id = tbluserlocation.user_id');
//        $this->where('location_id = ' . "'" . $locationId . "'");
//        $query = $this->get();
//        return $query;
    }

    public function get_users_underlocation($locationId) {
        $query = $this->db->query('SELECT * FROM tbluserlocation AS t1 
JOIN tblcliniciandata AS t2 ON t1.user_id = t2.user_id
WHERE t1.location_id = ' . $locationId .
                ' GROUP BY t2.user_id');
        return $query->result_array();
    }

    public function get_adminlocids($userId) {
        $this->select('location_id');
        $this->from('tbluserlocation');
        $this->where('user_id = ' . "'" . $userId . "'");
        $query = $this->get();
        return $query;
    }

    public function get_all_location($practice_id) {
        $this->select('* , id AS location_id');
        $this->from('tbllocation');
        $this->where('practice_id = ' . "'" . $practice_id . "'");
        $query = $this->get();
        return $query;
    }

    public function get_all_location_data($practice_id) {
        $query = $this->db->query('SELECT * FROM tbllocation '
                . 'WHERE tbllocation.practice_id=' . $practice_id . ' GROUP BY id');
        return $query->result_array();
    }

    public function get_locdata($userId) {
        $query = $this->db->query('SELECT tbluserlocation.* , tbllocation.location_name as location_name FROM tbluserlocation '
                . 'JOIN tbllocation ON tbluserlocation.location_id = tbllocation.id '
                . 'WHERE tbluserlocation.user_id=' . $userId);
        return $query->result_array();
    }

    public function get_loc_active($userId) {
        $this->select();
        $this->from('tbluserlocation');
        $this->where('user_id = ' . $userId . ' AND work_status = 1');
        $query = $this->get();
        return $query;
    }

    public function get_locids($userId) {

        $this->select('location_id');
        $this->from('tbluserlocation');
        $this->where('user_id = ' . "'" . $userId . "'");
        $query = $this->get();
        return $query;
    }

    public function get_locs_by_userid_locid($userId, $id) {
        $query = $this->db->query('SELECT * FROM tbluserlocation '
                . 'WHERE user_id = ' . $userId . ' AND location_id = ' . $id);
        return $query->result_array();
    }

    public function update_userloc($data, $uid, $lid) {
        $result = $this->update('tbluserlocation', $data, 'user_id = ' . $uid . ' AND location_id = ' . $lid);
        return $result;
    }

    public function get_admin_locs($userId) {
        $query = $this->db->query('SELECT tbluserlocation.* , tbllocation.location_name as location_name FROM tbluserlocation '
                . 'JOIN tbllocation ON tbluserlocation.location_id = tbllocation.id '
                . 'WHERE tbluserlocation.user_id=' . $userId);
        return $query->result_array();
    }

    public function delete_userloc($userId, $locId) {
        $query = $this->db->query('DELETE FROM tbluserlocation
                                   WHERE user_id = ' . $userId . ' AND location_id = ' . $locId);
        return;
    }

    public function delete_all_userloc($userId) {
        $query = $this->db->query('DELETE FROM tbluserlocation
                                   WHERE user_id = ' . $userId);
        return;
    }

    public function get_clinician_start_end_date($user_id, $location_id) {
        $this->select();
        $this->from('tbluserlocation');
        $where = array(
            'user_id' => $user_id,
            'location_id' => $location_id
        );
        $this->where($where);
        $query = $this->get();
        return $query;
    }

    public function get_initial_case($user_id, $location_id) {
        $this->select();
        $this->from('tbluserlocation');
        $this->where('user_id = ' . "" . $user_id . " AND location_id =" . $location_id);
        $query = $this->get();
        return $query;
    }

    public function get_initial_cases($locid, $user_id) {
        $this->select();
        $this->from('tbluserlocation');
        $this->where('user_id=' . $user_id . ' AND location_id=' . $locid);
        $query = $this->get();
        return $query[0]['initial_cases'];
    }

}
