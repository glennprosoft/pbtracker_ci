<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations extends Table {

    public function save_location($data) {
        $result = $this->insert('tbllocation', $data);
        return $result;
    }

    public function get_locations_in_practice($pid) {
        $this->select();
        $this->from('tbllocation AS l');
        $this->join('tblcollectionrate AS c', 'l.id=c.location_id');
        $this->where('l.practice_id=' . "'" . $pid . "'" . " AND l.location_status = 1");
        $query = $this->get();
        return $query;
    }

    public function get_locs_under_practice($practiceId) {
        $this->select('*,id as location_id');
        $this->from('tbllocation ');
        $this->where('practice_id = ' . $practiceId );
        $query = $this->get();
        return $query;
    }
    
    public function get_clinician_list($location_id, $start_date, $end_date){
        $query = $this->db->query('SELECT * FROM tblcliniciandata as t1 
INNER JOIN tblusers as t2 ON t1.user_id = t2.id 
WHERE t1.location_id = '.$location_id.'
AND t1.start_date >= "'.$start_date.'" AND t1.start_date <= "'.$end_date.'"
GROUP BY t2.id');
        return $query->result_array();
    }

    public function get_practice_name($id) {
        $this->db->select('*');
        $this->db->from('tbllocation');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query;
    }

    public function get_location_by_id($loc_id) {
        $this->db->select('*');
        $this->db->from('tbllocation');
//        $this->where('id='."'".'4'."'");
        $this->db->where('id', $loc_id);
        $query = $this->db->get();
////        return $query;
//        $query = $this->db->get_where('tbllocation', array('id' => 10));
//        print_r($query->result_array());
//        echo ($this->db->last_query());
        return $query->result_array();
//        print_r($query);
//        return $query;
    }

    public function get_location_collection_history_by_id($loc_id) {
        $query = $this->db->query('SELECT * FROM tbllocation 
INNER JOIN tblpractice ON tbllocation.practice_id = tblpractice.id 
INNER JOIN tblcollectionrate ON tbllocation.id = tblcollectionrate.location_id 
WHERE tbllocation.id = ' . $loc_id);
        return $query->result_array();
    }

    public function get_collection_history($loc_id) {
        $query = $this->db->query('SELECT * FROM tblcollectionratehistory WHERE location_id = ' . $loc_id);
        return $query->result_array();
    }

    public function get_location_by_name($pName) {
        $this->select();
        $this->from('tbllocation');
        $this->where('location_name = ' . "'" . $pName . "'");
        $query = $this->get();
        return $query;
    }

    public function update_location($data, $locid) {
        $where = "id = " . "'" . $locid . "'";
        $this->update('tbllocation', $data, $where);
    }

    public function get_all_locations() {
        $this->select();
        $this->from('tbllocation AS l');
        $this->join('tblcollectionrate AS c', 'l.id=c.location_id');
        $this->where('location_status = 1');
        $query = $this->get();
        return $query;
    }

    public function get_locations_by_user($user_id) {
        $query = $this->db->query('SELECT * FROM tbllocation
INNER JOIN tblcollectionrate as nyur ON tbllocation.id = nyur.location_id 
WHERE tbllocation.id = ANY(SELECT location_id FROM tbluserlocation 
WHERE user_id = ' . $user_id . ')');
        return $query->result_array();
    }

//    public function get_practice_by_code($pCode) {
//        $this->select();
//        $this->from('tblpractice');
//        $this->where('practice_code = ' . "'" . $pCode . "'");
//        $query = $this->get();
//        return $query;
//    }
}
