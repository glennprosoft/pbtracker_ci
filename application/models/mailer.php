<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mailer extends CI_Model{

    public function __construct() {
        parent::__construct();
        $this->load->library('My_PHPMailer');
    }

    function get_template($filename, $data) {
        extract($data);
        if (is_file($filename)) {
            ob_start();
            include $filename;
            return ob_get_clean();
        }
        return false;
    }

    function send_mail($email, $subject, $mailbody) {
        $mail = new PHPMailer(true); 
        try {
            
			//live
			$mail->IsSMTP();  // telling the class to use SMTP
			$mail->IsHTML(true);
			$mail->Host = "relay-hosting.secureserver.net";      // sets GMAIL as the SMTP server
			$mail->Port = 25;                   // set the SMTP port for the GMAIL server
			$mail->Username = "tracker@performancebuilders.com";  // GMAIL username
			$mail->Password = "tr@ck3r";            // GMAIL password
			$mail->From = "tracker@performancebuilders.com";
			$mail->FromName = "Performance Tracker";
			
			//for localhost
			/*
            $mail->IsSMTP(); // we are going to use SMTP
            $mail->IsHTML(true);
            $mail->SMTPAuth = true; // enabled SMTP authentication
            $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
            $mail->Host = "smtp.gmail.com";      // setting GMail as our SMTP server
            $mail->Port = 465;                   // SMTP port to connect to GMail
            $mail->Username = "";  // user email address  => ex. "glenn.prosoft@gmail.com"
            $mail->Password = ""; // password in GMail => gmail password
            $mail->From = "glenncabatbat.prosoft@gmail.com";
            $mail->FromName = "Performance Tracker";
			*/
            
            
            $mail->AddAddress($email);
            $mail->Subject = $subject;
            $mail->Body = $mailbody;
            $mail->Send();
            return true;
            
        } catch (phpmailerException $e) {
            return false;
        }
    }

}
