<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Table extends CI_Model {
    
    private $connection;
    private $last_query;
    private $magic_quotes_active;
    private $real_escape_string_exists;
    private $var_select;
    private $var_where;
    private $var_from;
    private $var_join = array();
    private $var_orderby;
    private $var_limit;
    private $var_between = array();

    function __construct() {
       
    }

   public function query($sql) {
        $result = mysql_query($sql);
        $this->confirm_query($result, $sql);
        return $result;
    }

    private function confirm_query($result, $sql) {
        if (!$result) {
            die("Database query failed: " . mysql_error() . "<br/>" . $sql);
        } else {
            //echo "<br/><br/>PERFORMED QUERY: ".$sql;
        }
    }

    private function fetch_array($result) {
        return mysql_fetch_array($result);
    }

    private function num_rows($result) {
        return mysql_num_rows($result);
    }

    public function affected_rows() {
        return mysql_affected_rows();
    }

    public function escape_value($value) {

        if ($this->real_escape_string_exists) {
            if ($this->magic_quotes_active) {
                $value = stripslashes($value);
            }
            $value = mysql_real_escape_string($value);
        } else {
            if (!$this->magic_quotes_active) {
                $value = addslashes($value);
            }
        }
        return $value;
    }

    public function get() {

        $sql = "SELECT " . $this->var_select;
        $sql .= " FROM " . $this->var_from;
        if (isset($this->var_join)) {
            foreach ($this->var_join as $joinrow) {
                $sql .= $joinrow;
            }
        }
        if (isset($this->var_where)) {
            $sql .= $this->var_where;
            if (isset($this->var_between)) {
                foreach ($this->var_between as $betweenrow) {
                    $sql .= " AND" . $betweenrow;
                }
            }
        } else {
            if (isset($this->var_between)) {
                $first = true;
                foreach ($this->var_between as $betweenrow) {
                    if ($first) {
                        $sql .= $betweenrow;
                        $first = false;
                    } else {
                        $sql .= " AND" . $betweenrow;
                    }
                }
            }
        }

        if (isset($this->var_orderby)) {
            $sql .= $this->var_orderby;
        }

        if (isset($this->var_limit)) {
            $sql .= $this->var_limit;
        }
        $result = $this->query($sql);
        if ($result) {
            $num_rows = $this->num_rows($result);
            if ($num_rows > 0) {
                for ($i = 0; $i < $num_rows; $i++) {
                    $rows[] = $this->fetch_array($result);
                }
                $query = $rows;
            } else {
                $query = false;
            }
        } else {
            $query = false;
        }

        $this->reset_select();

        return $query;
    }

    public function select($select = '*') {
        $this->var_select = $select;
    }

    public function where($where) {
        $sql = " WHERE ";
        if (is_array($where)) {
            $first = true; //determine the first index
            foreach ($where as $key => $value) {
                if ($first) {
                    $sql .= $key . " = " . $value;
                    $first = false;
                } else {
                    $sql .= " AND " . $key . "= " . $value;
                }
            }
        } else {
            $sql .= $where;
        }
        $this->var_where = $sql;
    }

    public function between($field, $min, $max) {
        $this->var_between[] = " " . $field . " BETWEEN " . $min . " AND " . $max;
    }

    public function between_v2($field, $min, $max) {
        $this->var_between[] = " " . $field . " BETWEEN '" . $min . "' AND '" . $max . "'";
    }

    public function from($from) {
        $this->var_from = $from;
    }

    public function join($table, $where, $type = NULL) {
        
        if ($type != NULL) {
            if (!in_array($type, array('LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER'))) {
                $type = '';
            } else {
                $type .= ' ';
            }
            $this->var_join[] = " " . $type . "JOIN " . $table . " ON " . $where;
        } else {
            $this->var_join[] = " JOIN " . $table . " ON " . $where;
        }
        
        
    }

    public function orderby($field, $type = 'ASC') {
        if (in_array($type, array('ASC', 'DESC'))) {
            $this->var_orderby = " ORDER BY " . $field . " " . $type;
        } else {
            die('order by given type is invalid: ' . $type);
        }
    }

    public function limit($limit = 1) {
        $this->var_limit = " LIMIT " . $limit;
    }

    public function insert($table, $data) {

        $sql = "INSERT INTO " . $table;
        if (is_array($data)) {
            $fields = array_keys($data);
            $values = array_values($data);
            $sql .= " (" . implode(",", $fields) . ")";
            $sql .= " VALUES (" . implode(",", $values) . ")";
        } else {
            $sql .= " " . $data;
        }
        $result = $this->query($sql);

        return $result;
    }

    public function update($table, $data, $where = NULL) {

        $sql = "UPDATE " . $table;
        if (is_array($data)) {
            $sql .= " SET ";
            $first = true;
            foreach ($data as $key => $value) {
                if ($first) {
                    $sql .= $key . " = " . $value;
                    $first = false;
                } else {
                    $sql .= ", " . $key . " = " . $value;
                }
            }
        } else {
            $sql .= " SET " . $data;
        }
        if ($where != NULL) {
            $this->where($where);
            $sql .= $this->var_where;
            $this->reset_select();
        }
        $result = $this->query($sql);

        return $result;
    }

    public function delete($table, $where) {

        $sql = "DELETE FROM " . $table;
        $this->where($where);
        $sql .=$this->var_where;
        $result = $this->query($sql);
        $this->reset_select();

        return $result;
    }

    public function get_practice_name($id) {
        $sql = "SELECT practice_name FROM tblpractice WHERE user_id=" . $id;
        $practicename = $this->query($sql);
        return $practicename;
    }

    private function reset_select() {
        unset($this->var_select);
        unset($this->var_from);
        unset($this->var_where);
        unset($this->var_join);
        unset($this->var_orderby);
        unset($this->var_limit);
        unset($this->var_between);
    }

    
}

    
    


