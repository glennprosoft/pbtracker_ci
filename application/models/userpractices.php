<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userpractices extends Table {

    function __construct() {
        parent::__construct();
    } 
    public function save_userpractice($data) {
        $result = $this->insert('tbluserpractice', $data);
        return $result;
    }
    public function get_practice($userId){
        $this->select();
        $this->from('tbluserpractice');
        $this->where('user_id =' . "'". $userId. "'");
        $query = $this->get();
        return $query;
    }

}
