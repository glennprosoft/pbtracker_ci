<!--
<p>Hi <?php// echo $firstname." ".$lastname; ?>,</p>
<br/>
<p>You have been invited to be a clinician of practice. Please click the link below to activate your account.</p>
<p><a href="<?php// echo $url."clinician_registration.php?hash=".$hash ?>"><?php// echo $url."clinician_registration.php?hash=".$hash ?></a></p>
<p>or you can manually go to this link <a href="<?php// echo $url."clinician_registration.php" ?>"><?php// echo $url."clinician_registration.php" ?></a> <br/>
and enter this authentication key: <?php// echo $hash; ?></p>

 -->


<!--Edited by Glenn 12/12/13
<p>Hi <?php //echo $firstname." ".$lastname; ?>,</p>
<br/>
<p>You have been invited to be a practice owner. Please click the link below to activate your account.</p>
<p><a href="<?php //echo $url."owner_registration.php?hash=".$hash ?>"><?php //echo $url."owner_registration.php?hash=".$hash ?></a></p>
<br/>
<p>or you can manually go to this link <a href="<?php //echo $url."owner_registration.php" ?>"><?php //echo $url."owner_regisration.php" ?></a> </p>
<p>and enter this authentication key: <?php //echo $hash; ?></p>
-->
<div style="width:100%; background:#DDDDDD;padding:10px;">
<div style="font-family:arial; font-size:10px; color:#444444; background:#ffffff; width:95%; border-radius:5px; border:1px solid #888888; margin-left:auto; margin-right:auto;">

<div id="emailheader" style="padding-top:40px; padding-bottom:30px">
	<div style="width:inherit;text-align:center;"> <h1> Performance Tracker </h1>
	</div>

</div>

<div style="font-size:15px; padding-bottom:40px; width:80%; margin-left:auto; margin-right:auto">
<!--contents here-->
<p><?php echo date('F d, Y'); ?></p>
<br/>
<br/>
<br/>
<p>Dear <?php echo $firstname." ".$lastname; ?>,</p>
<br/>
<p>You have been invited to be a user of the <?php echo $practice_name; ?> tracker.</p>
<br/>
<p>Please click on the link below to activate your account.</p>
<p><a href="<?php echo $url."clinician_registration.php?hash=".$hash ?>"><?php echo $url."clinician_registration.php?hash=".$hash ?></a></p>
<br/>
<p>After clicking the link, you will be directed to create a password (your login will be your email address). After creating your password, you will be asked to login to the Tracker. You are now ready to use the Tracker.</p>
<br/>
<p>Thank you,</p>
<br/>
<br/>
<p><?php echo $ownername; ?></p>
<p><?php echo $practice_name; ?></p>

</div>
 
<div style="font-size:15px;padding-bottom:40px; width:80%; margin-left:auto; margin-right:auto">
	<img src="<?php echo base_url();?>/img/pb-logo-2.jpg">
	<!--logo here-->
</div>
</div>

<div style="height:20px;font-family:arial;color:#444444;font-size:11px;width:90%; margin-left:auto; margin-right:auto; margin-top:5px;">
	<div style="float:left;">
	<p>PerformanceBuilders.com </p>
</div>
	<div style="float:right; ">
	<p> &#169; 2014 Performance Builders</p>
</div>
</div>
</div>

